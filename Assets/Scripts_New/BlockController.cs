﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockController : MonoBehaviour
{
	public GameObject obj;

	public GameObject sq;

	public Material[] ma;

	public PlatformJoker pj;

	public Animator anim;

	public ParticleSystem ps;

	public ParticleSystem.EmissionModule psEmi;

	public ParticleSystem.MainModule psMain;

	public ParticleSystem.SizeOverLifetimeModule psSize;

	public LineBreak text;

	public SpriteRenderer topFire;

	public HitShadowAnim hitShadow;

	public void SetColor(Color c)
	{
		for (int i = 0; i < ma.Length; i++)
		{
			ma[i].color = c;
		}
	}

	public Color GetColor()
	{
		if (ma.Length > 0)
		{
			return ma[0].color;
		}
		return Color.black;
	}
}
