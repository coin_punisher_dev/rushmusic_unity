using System;
using UnityEngine;

public class Tapstream : MonoBehaviour
{
    public class Config
    {
        protected internal AndroidJavaObject handle;

        public Config()
        {
            //handle = new AndroidJavaObject("com.tapstream.sdk.Config");
        }

        public void Set(string key, object val)
        {
            //string methodName = "set" + char.ToUpper(key[0]) + key.Substring(1);
            //Type type = val.GetType();
            //if (type == typeof(string))
            //{
            //    handle.Call(methodName, (string)val);
            //}
            //else if (type == typeof(bool))
            //{
            //    handle.Call(methodName, (bool)val);
            //}
            //else if (type == typeof(int))
            //{
            //    handle.Call(methodName, (int)val);
            //}
            //else if (type == typeof(uint))
            //{
            //    handle.Call(methodName, (uint)val);
            //}
            //else if (type == typeof(double))
            //{
            //    handle.Call(methodName, (double)val);
            //}
            //else
            //{
            //    Console.WriteLine("Tapstream config object cannot accept this type: {0}", type);
            //}
        }
    }

    public class Event
    {
        protected internal AndroidJavaObject handle;

        public Event(string name, bool oneTimeOnly)
        {
            //handle = new AndroidJavaObject("com.tapstream.sdk.Event", name, oneTimeOnly);
        }

        public void AddPair(string key, object val)
        {
//            //@TODO TAPSTREAM
//#if !UNITY_EDITOR && UNITY_ANDROID
//            Type type = val.GetType();
//			if (type == typeof(string))
//			{
//				using (AndroidJavaObject androidJavaObject = new AndroidJavaObject("java.lang.String", (string)val))
//				{
//					handle.Call("addPair", key, androidJavaObject);
//				}
//			}
//			else if (type == typeof(bool))
//			{
//				using (AndroidJavaObject androidJavaObject2 = new AndroidJavaObject("java.lang.Boolean", (bool)val))
//				{
//					handle.Call("addPair", key, androidJavaObject2);
//				}
//			}
//			else if (type == typeof(int))
//			{
//				using (AndroidJavaObject androidJavaObject3 = new AndroidJavaObject("java.lang.Integer", (int)val))
//				{
//					handle.Call("addPair", key, androidJavaObject3);
//				}
//			}
//			else if (type == typeof(uint))
//			{
//				using (AndroidJavaObject androidJavaObject4 = new AndroidJavaObject("java.lang.Long", (uint)val))
//				{
//					handle.Call("addPair", key, androidJavaObject4);
//				}
//			}
//			else if (type == typeof(double))
//			{
//				using (AndroidJavaObject androidJavaObject5 = new AndroidJavaObject("java.lang.Double", (double)val))
//				{
//					handle.Call("addPair", key, androidJavaObject5);
//				}
//			}
//			else
//			{
//				Console.WriteLine("Tapstream event object cannot accept this type: {0}", type);
//			}
//#endif
        }
    }

    public static void Create(string accountName, string developerSecret, Config conf)
    {
//        //@TODO TAPSTREAM
//#if !UNITY_EDITOR && UNITY_ANDROID
//		using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
//		{
//			using (AndroidJavaObject androidJavaObject = androidJavaClass.GetStatic<AndroidJavaObject>("currentActivity"))
//			{
//				using (AndroidJavaClass androidJavaClass2 = new AndroidJavaClass("com.tapstream.sdk.Tapstream"))
//				{
//					androidJavaClass2.CallStatic("create", androidJavaObject.Call<AndroidJavaObject>("getApplication", new object[0]), accountName, developerSecret, conf.handle);
//				}
//			}
//		}
//#endif
    }

    public static void FireEvent(Event e)
    {
//        //@TODO TAPSTREAM
//#if !UNITY_EDITOR && UNITY_ANDROID
//        using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.tapstream.sdk.Tapstream"))
//		{
//			using (AndroidJavaObject androidJavaObject = androidJavaClass.CallStatic<AndroidJavaObject>("getInstance", new object[0]))
//			{
//				androidJavaObject.Call("fireEvent", e.handle);
//			}
//		}
//#endif
    }

    public static void GetConversionData(string callbackClass, string callbackMethod)
    {
//        //@TODO TAPSTREAM
//#if !UNITY_EDITOR && UNITY_ANDROID
//        using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.tapstream.sdk.UnityConversionListener"))
//		{
//			androidJavaClass.CallStatic("getConversionData", callbackClass, callbackMethod);
//		}
//#endif
    }
}