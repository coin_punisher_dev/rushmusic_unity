using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//using F4A = com.F4A.MobileThird;

public class AdsManager : MonoBehaviour
{
	public static AdsManager instance;

	private VIDEOREWARD rewardType;

	private VIDEOREWARD interstitialType = VIDEOREWARD.SKIPABLE_ENDGAME;

	private Transform source;

	private float adsTime;

	private bool isFirst = true;

	public bool allowBanner = true;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
			Object.DontDestroyOnLoad(base.gameObject);
			adsTime = Time.time;
		}
		else if (instance != null)
		{
			UnityEngine.Object.Destroy(base.gameObject);
		}
	}

	private void Start()
	{
		StartCoroutine(PrepareVideo(1f, isFirstLoad: true));
	}

    //private void OnEnable()
    //{
    //    F4A.AdsManager.OnRewardedAdCompleted += AdsManager_OnRewardedAdCompleted;
    //    F4A.AdsManager.OnRewardedAdSkiped += AdsManager_OnRewardedAdSkiped;
    //    F4A.AdsManager.OnRewardedAdFailed += AdsManager_OnRewardedAdFailed;
    //}

    //private void OnDisable()
    //{
    //    F4A.AdsManager.OnRewardedAdCompleted -= AdsManager_OnRewardedAdCompleted;
    //    F4A.AdsManager.OnRewardedAdSkiped -= AdsManager_OnRewardedAdSkiped;
    //    F4A.AdsManager.OnRewardedAdFailed -= AdsManager_OnRewardedAdFailed;
    //}

    //private void AdsManager_OnRewardedAdCompleted(F4A.ERewardedAdNetwork adNetwork)
    //{
    //    RewardedVideoAdRewardedEvent();
    //}

    //private void AdsManager_OnRewardedAdSkiped(F4A.ERewardedAdNetwork adNetwork)
    //{
    //    //RewardedVideoAdSkipped();
    //}
    //private void AdsManager_OnRewardedAdFailed(F4A.ERewardedAdNetwork adNetwork)
    //{
    //    RewardedVideoAdShowFailedEvent("Can't fill");
    //}

	private IEnumerator PrepareShowBanner()
	{
		yield return new WaitForSeconds(1f);
	}

	public void ShowRewardAds(VIDEOREWARD type, Transform src = null, bool wait = true)
	{
        //if (F4A.AdsManager.Instance.IsRewardAdsReady())
        //{
        //    F4A.AdsManager.Instance.ShowRewardAds();
        //    rewardType = type;
        //    source = src;
        //    //AnalyticHelper.Rad(Click_Impression.impression, rewardType);
        //    return;
        //}
        if (wait)
        {
            SceneFader.instance.ShowOverlay();
            StartCoroutine(WatingRewardedVideo());
        }
        else
        {
            Util.ShowMessage(LocalizationManager.instance.GetLocalizedValue("REWARD_VIDEO_UNAVAILABLE"));
        }
    }

    private IEnumerator WatingRewardedVideo()
	{
		yield return new WaitForSeconds(3f);
		SceneFader.instance.HideOverlay();
		ShowRewardAds(rewardType, source, wait: false);
	}

	public void InterstitialAdReadyEvent()
	{
		AnalyticHelper.FireEvent(FIRE_EVENT.VideoNoneSkipable_Loaded);
	}

	public void RequestInterstitialVideo()
	{
		if (!Configuration.instance.isNoAds && Time.time - adsTime > RemoteVariables.AdsTime)
		{
			adsTime = Time.time;
			ShowInterstitialVideo(VIDEOREWARD.SKIPABLE_ENDGAME);
		}
	}

	private IEnumerator PrepareVideo(float delay = 0f, bool isFirstLoad = false)
	{
		if (!Configuration.instance.isNoAds)
		{
			if (delay > 0f)
			{
				yield return new WaitForSeconds(delay);
			}
		}
	}

	public void LoadBanner()
	{
		if (!Configuration.instance.isNoAds && allowBanner)
		{
            //adn.loadBanner();
		}
	}

	public void ShowBanner()
	{
		//if (!Configuration.instance.isNoAds && allowBanner
  //          //&& adn.bannerStatus != AdNetwork.BANNER_STATUS.FAILED
  //          )
		//{
        //    //adn.showBanner();
        //    F4A.AdsManager.Instance.ShowBannerAds();
        //}
	}

	public void HideBanner()
	{
		//if (!Configuration.instance.isNoAds && allowBanner
  //          //&& adn.bannerStatus != AdNetwork.BANNER_STATUS.FAILED
  //          )
		//{
  //          //adn.hideBanner();
  //          F4A.AdsManager.Instance.HideBannerAds();
		//}
	}

	public AdNetwork.BANNER_STATUS GetBannerStatus()
	{
		return AdNetwork.BANNER_STATUS.FAILED;
	}

	public void ShowInterstitialVideo(VIDEOREWARD type)
	{
		interstitialType = type;
		if (isFirst)
		{
			isFirst = false;
		}

        //if (F4A.AdsManager.Instance.IsInterstitialAdsReady())
        //{
        //    F4A.AdsManager.Instance.ShowInterstitialAds();
        //    AnalyticHelper.Fad(Click_Impression.impression);
        //}
        //else
        //{
        //    //StartCoroutine(PrepareVideo());
        //}
    }

    public void InterstitialAdClickedEvent()
	{
		AnalyticHelper.Fad(Click_Impression.click);
	}

	public void RewardedVideoClickedEvent()
	{
		AnalyticHelper.Rad(Click_Impression.click, rewardType);
	}

	public void InterstitialAdLoadFailedEvent(string error)
	{
		AnalyticHelper.FireEvent(FIRE_EVENT.VideoNoneSkipable_LoadFailed);
	}

	public void ToggleGameActivities(bool isPause = true)
	{
		if (isPause)
		{
			Time.timeScale = 0f;
			if (HomeManager.instance != null)
			{
				HomeManager.instance.groundMusic.StopMusic();
			}
		}
		else
		{
			Time.timeScale = 1f;
			if (HomeManager.instance != null)
			{
				HomeManager.instance.groundMusic.ResumeMusic();
			}
		}
	}

	public void InterstitialAdClosedEvent()
	{
		adsTime = Time.time;
		StartCoroutine(PrepareVideo());
	}

    public void RewardedVideoAdRewardedEvent()
    {
        adsTime = Time.time;
        switch (rewardType)
        {
            case VIDEOREWARD.UNLOCKBALL:
                if (FreeVideo.instance != null)
                {
                    FreeVideo.instance.CompletedVideoAds();
                }
                else if (BallList.instance != null)
                {
                    BallList.instance.UnlockBall();
                }
                break;
            case VIDEOREWARD.UNLOCKTHEME:
                if (FreeVideo.instance != null)
                {
                    FreeVideo.instance.CompletedVideoAds();
                }
                else if (ThemeList.instance != null)
                {
                    ThemeList.instance.UnlockTheme();
                }
                break;
            case VIDEOREWARD.UNLOCKSONG:
                if (FreeVideo.instance != null)
                {
                    FreeVideo.instance.CompletedVideoAds();
                }
                else if (source != null)
                {
                    SongList.instance.OpenSong(source);
                }
                else if (UIController.ui != null)
                {
                    UIController.ui.gameover.GetComponent<GameCompletedUI>().UnlockedVideo();
                }
                break;
            case VIDEOREWARD.CONTINUE:
                GameController.controller.GameContinue(SONG_PLAY_COST.RewardVideo);
                UIController.ui.continueui.SetActive(value: false);
                break;
            case VIDEOREWARD.GETDIAMOND:
                TopBar.instance.UpdateDiamond(Configuration.instance.diamondsVideoBonus, Income_Type.VIDEO);
                if (FreeVideo.instance != null)
                {
                    FreeVideo.instance.IncreaseDiamondsVideoBonus();
                }
                if (Shop.instance != null)
                {
                    Shop.instance.IncreaseDiamondsVideoBonus();
                }
                break;
            case VIDEOREWARD.UPLOADSONG:
                if (SongList.instance) SongList.instance.PrepareShowFileSelector(0.3f);
                break;
            case VIDEOREWARD.X2DIAMOND:
                if (UIController.ui != null)
                {
                    UIController.ui.gameover.GetComponent<GameCompletedUI>().X2DiamondFinish();
                }
                break;
        }
    }

	public void RewardedVideoAdSkipped()
	{
		Util.ShowMessage("Rewared video was skipped!");
		if (UIController.ui != null && UIController.ui.continueui.activeSelf)
		{
			UIController.ui.continueui.GetComponent<ContinueUI>().Cancel();
		}
	}

	public void RewardedVideoAdClosedEvent()
	{
	}

	public void RewardedVideoAdShowFailedEvent(string error)
	{
		Util.ShowMessage("VideoError: " + error);
	}

	public void RewardedVideoAdEndedEvent()
	{
		AnalyticHelper.Rad(Click_Impression.finish, rewardType);
	}

	public void OnApplicationPause(bool pauseStatus)
	{
		if (!pauseStatus)
		{
			adsTime = Time.time;
		}
	}
}
