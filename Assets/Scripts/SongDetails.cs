using UnityEngine;
using UnityEngine.UI;

public class SongDetails : PopupUI
{
	public Text title;

	private Item song;

	public void SetSong(Item song)
	{
		this.song = song;
		title.text = song.name;
	}

	public void Remove()
	{
		AnalyticHelper.FireEvent(FIRE_EVENT.LocalSong_Remove);
		RemoteFilesData.instance.RemoveSongLocal(song.path);
		PlayerPrefs.DeleteKey("bestscore" + song.path);
		PlayerPrefs.DeleteKey("beststars" + song.path);
		SongList.instance.BuidListNew();
		Close();
	}
}
