using UnityEngine;

public class ExitPopUp : PopupUI
{
	public static ExitPopUp instance;

	private void Awake()
	{
		instance = this;
	}

	public void QuitApplication()
	{
		Application.Quit();
	}
}
