using UnityEngine.UI;

public class UnlockPushSong : PopupUI
{
	public static string songid;

	public Text title;

	private void Start()
	{
		title.text = title.text.Replace("{0}", "<color='#00E031'>" + RemoteFilesData.instance.songList[songid].name + "</color>");
		Configuration.instance.SetCurrentLocation(LOCATION_NAME.UNLOCK_PUSH_SONG, isPopup: true);
	}

	public void Unlock_Click()
	{
		SoundManager.instance.PlayGameButton();
		AdsManager.instance.ShowRewardAds(VIDEOREWARD.UNLOCKSONG, SongList.instance.items[songid].transform);
		AnalyticHelper.FireString("PushNewSong_Unlock");
		Close();
	}

	public void Cancel()
	{
		songid = null;
		Close();
	}
}
