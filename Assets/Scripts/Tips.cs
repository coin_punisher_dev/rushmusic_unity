using UnityEngine;

public class Tips : PopupUI
{
	public GameObject title1;

	public GameObject title2;

	public GameObject closeBtn;

	public static Tips instance;

	private void Awake()
	{
		instance = this;
		Configuration.instance.SetCurrentLocation(LOCATION_NAME.TIPS, isPopup: true);
	}

	public void SetStyle(int style)
	{
		if (style == 1)
		{
			title1.SetActive(value: true);
			title2.SetActive(value: false);
			closeBtn.SetActive(value: true);
		}
		else
		{
			title1.SetActive(value: false);
			title2.SetActive(value: true);
			closeBtn.SetActive(value: false);
		}
	}
}
