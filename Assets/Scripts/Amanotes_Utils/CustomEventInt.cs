using System.Collections.Generic;

namespace Amanotes.Utils
{
	public class CustomEventInt
	{
		public string eventName;

		public Dictionary<string, int> eventParams;

		public CustomEventInt(string eventName)
		{
			this.eventName = eventName;
			eventParams = new Dictionary<string, int>();
		}

		public CustomEventInt(string eventName, string key, int value)
		{
			this.eventName = eventName;
			eventParams = new Dictionary<string, int>();
			eventParams[key] = value;
		}

		public CustomEventInt(string eventName, Dictionary<string, int> eventParams)
		{
			this.eventName = eventName;
			this.eventParams = eventParams;
			if (this.eventParams == null)
			{
				this.eventParams = new Dictionary<string, int>();
			}
		}
	}
}
