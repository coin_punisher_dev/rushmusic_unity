using System.Collections.Generic;
using UnityEngine;

namespace Amanotes.Utils
{
	public class GlobalData : MonoBehaviour
	{
		private static Dictionary<string, GameObject> cache = new Dictionary<string, GameObject>();

		private void Awake()
		{
			Object.DontDestroyOnLoad(base.gameObject);
			if (cache.ContainsKey(base.name))
			{
				UnityEngine.Object.DestroyImmediate(base.gameObject);
			}
			else
			{
				cache[base.name] = base.gameObject;
			}
		}

		private void Start()
		{
		}
	}
}
