using System.Collections.Generic;

namespace Amanotes.Utils
{
	public class CustomEvent
	{
		public string eventName;

		public Dictionary<string, string> eventParams;

		public CustomEvent(string eventName)
		{
			this.eventName = eventName;
			eventParams = new Dictionary<string, string>();
		}

		public CustomEvent(string eventName, string key, string value)
		{
			this.eventName = eventName;
			eventParams = new Dictionary<string, string>();
			eventParams[key] = value;
		}

		public CustomEvent(string eventName, Dictionary<string, string> eventParams)
		{
			this.eventName = eventName;
			this.eventParams = eventParams;
			if (this.eventParams == null)
			{
				this.eventParams = new Dictionary<string, string>();
			}
		}
	}
}
