namespace Amanotes.Utils
{
	public abstract class Singleton<T> where T : new()
	{
		private static T singleton;

		public static T Instance
		{
			get
			{
				if (singleton == null)
				{
					singleton = new T();
				}
				return singleton;
			}
		}

		public static T instance
		{
			get
			{
				if (singleton == null)
				{
					singleton = new T();
				}
				return singleton;
			}
		}
	}
}
