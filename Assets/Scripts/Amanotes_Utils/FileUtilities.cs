using System.IO;
using UnityEngine;

namespace Amanotes.Utils
{
	public class FileUtilities
	{
		public static string GetWritablePath(string relativeFilePath)
		{
			string empty = string.Empty;
			return Application.persistentDataPath + "/" + relativeFilePath;
		}

		public static byte[] LoadFile(string filePath, bool isAbsolutePath = false)
		{
			if (filePath == null || filePath.Length == 0)
			{
				return null;
			}
			string path = filePath;
			if (!isAbsolutePath)
			{
				path = GetWritablePath(filePath);
			}
			if (File.Exists(path))
			{
				return File.ReadAllBytes(path);
			}
			return null;
		}

		public static bool IsFileExist(string filePath, bool isAbsolutePath = false)
		{
			if (filePath == null || filePath.Length == 0)
			{
				return false;
			}
			string path = filePath;
			if (!isAbsolutePath)
			{
				path = GetWritablePath(filePath);
			}
			return File.Exists(path);
		}

		public static string SaveFile(byte[] bytes, string filePath, bool isAbsolutePath = false, bool isSaveResource = false)
		{
			if (filePath == null || filePath.Length == 0)
			{
				return null;
			}
			string text = filePath;
			if (!isAbsolutePath)
			{
				text = GetWritablePath(filePath);
			}
			string directoryName = Path.GetDirectoryName(text);
			if (!Directory.Exists(directoryName))
			{
				Directory.CreateDirectory(directoryName);
			}
			File.WriteAllBytes(text, bytes);
			return text;
		}

		public static string SaveFileOtherThread(byte[] bytes, string filePath)
		{
			if (filePath == null || filePath.Length == 0)
			{
				return null;
			}
			string directoryName = Path.GetDirectoryName(filePath);
			if (!Directory.Exists(directoryName))
			{
				Directory.CreateDirectory(directoryName);
			}
			File.WriteAllBytes(filePath, bytes);
			return filePath;
		}

		public static void DeleteFile(string filePath, bool isAbsolutePath = false)
		{
			if (filePath == null || filePath.Length == 0)
			{
				return;
			}
			if (isAbsolutePath)
			{
				if (File.Exists(filePath))
				{
					File.Delete(filePath);
				}
			}
			else
			{
				string writablePath = GetWritablePath(filePath);
				DeleteFile(writablePath);
			}
		}
	}
}
