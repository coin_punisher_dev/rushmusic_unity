using UnityEngine;

namespace Amanotes.Utils
{
	public class SingletonMono<T> : MonoBehaviour where T : MonoBehaviour
	{
		private static T singleton;

		public static T Instance
		{
			get
			{
				if (!Application.isPlaying)
				{
					return (T)null;
				}
				if ((Object)singleton == (Object)null)
				{
					singleton = (T)UnityEngine.Object.FindObjectOfType(typeof(T));
					if ((Object)singleton == (Object)null)
					{
						GameObject gameObject = new GameObject();
						gameObject.name = "[@" + typeof(T).Name + "]";
						singleton = gameObject.AddComponent<T>();
					}
				}
				return singleton;
			}
		}

		public static T instance
		{
			get
			{
				if (!Application.isPlaying)
				{
					return (T)null;
				}
				if ((Object)singleton == (Object)null)
				{
					singleton = (T)UnityEngine.Object.FindObjectOfType(typeof(T));
					if ((Object)singleton == (Object)null)
					{
						GameObject gameObject = new GameObject();
						gameObject.name = "[@" + typeof(T).Name + "]";
						singleton = gameObject.AddComponent<T>();
					}
				}
				return singleton;
			}
		}

		public static bool IsInstanceValid()
		{
			return (Object)singleton != (Object)null;
		}

		private void Reset()
		{
			base.gameObject.name = typeof(T).Name;
		}
	}
}
