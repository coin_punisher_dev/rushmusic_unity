using UnityEngine;
using UnityEngine.UI;

public class HomeManager : MonoBehaviour
{
	public static HomeManager instance;

	public GameObject homeContainer;

	public BallList ballList;

	public ThemeList themeList;

	public Image bg1;

	public Image bg2;

	public Text songName;

	public GroundMusic groundMusic;

	public GameObject[] bottomButtons;

	public Sprite[] bgs;

	public GameObject[] contentTabs;

	private int activeId;

	private bool onTransition;

	public Image disk;

	public Image diskMain;

	public static bool isShowEster;

	public GameObject achievementBadge;

	private void Awake()
	{
		instance = this;
	}

	private void Start()
	{
		ShowHome();
		groundMusic.PlayMusic();
		if (!Configuration.IntroIsOn() && DailyGift.GetActiveDay() >= 0)
		{
			Util.ShowPopUp("DailyGift");
		}
		RateUs.CheckAndOpenRate();
	}

	private void Update()
	{
		if (AchievementItem.IsFacebookShareAvailable() || AchievementItem.IsTwitterAvailable() || CoreUser.instance.user == null)
		{
			achievementBadge.SetActive(value: true);
		}
		else
		{
			achievementBadge.SetActive(value: false);
		}
	}

	public void ShowHome_Click()
	{
		if (!onTransition)
		{
			SoundManager.instance.PlayGameButton();
			ShowHome();
		}
	}

	public void ShowHome()
	{
		if (!onTransition)
		{
			TopBar.instance.ToggleDiamond(enable: true);
			Item currentSong = CoreData.instance.GetCurrentSong();
			songName.text = currentSong.name;
			diskMain.sprite = Util.GetThemeDisk(currentSong.path);
			if (Tips.instance != null)
			{
				UnityEngine.Object.Destroy(Tips.instance.gameObject);
			}
			ChangeContentTo(0);
			Configuration.instance.SetCurrentLocation(LOCATION_NAME.HOME);
		}
	}

	public void ShowBallList()
	{
		if (!onTransition)
		{
			SoundManager.instance.PlayGameButton();
			TopBar.instance.ToggleDiamond(enable: true);
			ChangeContentTo(2);
			Configuration.instance.SetCurrentLocation(LOCATION_NAME.BALL_LIST);
		}
	}

	public void ShowThemeList()
	{
		if (!onTransition)
		{
			SoundManager.instance.PlayGameButton();
			TopBar.instance.ToggleDiamond(enable: true);
			ChangeContentTo(1);
		}
	}

	public void ShowAchievement()
	{
		if (!onTransition)
		{
			SoundManager.instance.PlayGameButton();
			TopBar.instance.ToggleDiamond(enable: true);
			ChangeContentTo(3);
			Configuration.instance.SetCurrentLocation(LOCATION_NAME.ACHIEVEMENT);
		}
	}

	public void ShowSettings()
	{
		if (!onTransition)
		{
			SoundManager.instance.PlayGameButton();
			AnalyticHelper.ButtonClick(FIRE_EVENT.Button_Settings);
			ChangeContentTo(4);
			Configuration.instance.SetCurrentLocation(LOCATION_NAME.SETTINGS);
		}
	}

	public void ShowTips(int style)
	{
		SoundManager.instance.PlayGameButton();
		if (Tips.instance == null)
		{
			GameObject gameObject = Util.ShowPopUp("Tips");
			homeContainer.SetActive(value: false);
			gameObject.GetComponent<Tips>().SetStyle(style);
			if (style == 1)
			{
				AnalyticHelper.ButtonClick(FIRE_EVENT.Button_Tips);
			}
		}
		Configuration.instance.SetCurrentLocation(LOCATION_NAME.TIPS);
	}

	public void ShowDailyGift()
	{
		SoundManager.instance.PlayGameButton();
		Util.ShowPopUp("DailyGift");
		AnalyticHelper.ButtonClick(FIRE_EVENT.Button_DailyReward);
	}

	public void ShowLeaderBoard()
	{
		SoundManager.instance.PlayGameButton();
		int num = 0;
		if (num == 0)
		{
			foreach (string key in RemoteFilesData.instance.songList.Keys)
			{
				num += CoreData.GetBestScore(key);
			}
			CoreData.SetBestScore(num, "Total");
		}
		LeaderBoardUI.instance.ShowBoard("Total", num);
	}

	public void Play()
	{
		Util.GoToGamePlay(CoreData.instance.GetCurrentSong().path);
		groundMusic.StopMusic();
	}

	private void ChangeContentTo(int index)
	{
		if (index != activeId)
		{
			onTransition = true;
			Image image;
			Image image2;
			if (bg1.transform.gameObject.activeSelf)
			{
				image = bg2;
				image2 = bg1;
			}
			else
			{
				image = bg1;
				image2 = bg2;
			}
			image.transform.gameObject.SetActive(value: true);
			image.sprite = bgs[index];
			image.canvasRenderer.SetAlpha(0f);
			image.CrossFadeAlpha(1f, 0.3f, ignoreTimeScale: false);
			image2.CrossFadeAlpha(0f, 0.3f, ignoreTimeScale: false);
			bottomButtons[activeId].transform.GetChild(0).gameObject.SetActive(value: false);
			bottomButtons[index].transform.GetChild(0).gameObject.SetActive(value: true);
			contentTabs[activeId].SetActive(value: false);
			image2.transform.gameObject.SetActive(value: false);
			activeId = index;
			onTransition = false;
			contentTabs[activeId].SetActive(value: true);
		}
		else
		{
			contentTabs[activeId].SetActive(value: true);
		}
	}

	public void Event_Click()
	{
		SoundManager.instance.PlayGameButton();
		ShowEvent();
	}

	public void ShowEvent()
	{
		Util.ShowPopUp("EasterEvent");
	}
}
