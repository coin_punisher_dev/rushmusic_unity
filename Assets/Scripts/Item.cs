[System.Serializable]
public class Item
{
    public string path;

    public string name;

    public float bmp;

    public float startTime;

    public int diamonds;

    public string type;

    public int version = 0;

    public string tags = string.Empty;

    public int theme = 0;

    public Item()
    {
    }

    public Item(string path, string name, float bmp, float startTime, int diamonds, string type, int version)
    {
        this.path = path;
        this.name = name;
        this.bmp = bmp;
        this.startTime = startTime;
        this.diamonds = diamonds;
        this.type = type;
        this.version = version;
    }

    public void UpdateBPM(float bmp, float startTime)
    {
        this.startTime = startTime;
        this.bmp = bmp;
    }
}