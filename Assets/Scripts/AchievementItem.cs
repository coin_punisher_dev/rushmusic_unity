#if ENABLE_FACEBOOK_SDK
using Facebook.Unity;
#endif
using System;
using UnityEngine;
using UnityEngine.UI;

public class AchievementItem : MonoBehaviour
{
	public Text titleText;

	public Text diamondText;

	public Text subText;

	public Image icon;

	public Image statusIcon;

	public Image progressIcon;

	public Text progressText;

	public AchievementObj obj;

	private bool waitReturn;

	private string TIMEFORMAT = "{0:00} : {1:00} : {2:00}";

	public void SetValues(AchievementObj obj)
	{
		this.obj = obj;
		titleText.text = LocalizationManager.instance.GetLocalizedValue(obj.type.ToString());
		if (obj.isNewChallenge())
		{
			subText.text = LocalizationManager.instance.GetLocalizedValue("NEW_CHALLENGE");
			subText.gameObject.SetActive(value: true);
			diamondText.transform.parent.gameObject.SetActive(value: false);
		}
		else
		{
			diamondText.text = obj.diamonds.ToString();
			subText.gameObject.SetActive(value: false);
			diamondText.transform.parent.gameObject.SetActive(value: true);
		}
		icon.sprite = obj.icon;
		if ((obj.status == AchievementStatus.LOCK || obj.status == AchievementStatus.UNLOCK) && obj.value > 0)
		{
			progressText.text = obj.current + "/" + obj.value;
			progressIcon.fillAmount = (float)obj.current / (float)obj.value;
			statusIcon.gameObject.SetActive(value: true);
			if (obj.current == obj.value)
			{
				statusIcon.sprite = Achievements.instance.unlockIcon;
				progressIcon.gameObject.SetActive(value: false);
			}
			else
			{
				statusIcon.sprite = Achievements.instance.lockIcon;
				progressIcon.gameObject.SetActive(value: true);
			}
		}
		else
		{
			statusIcon.gameObject.SetActive(value: false);
		}
	}

	public void Claim()
	{
		SoundManager.instance.PlayGameButton();
		switch (obj.type)
		{
		case AchievementType.LOGIN_1ST:
			if (statusIcon.gameObject.activeSelf && CoreUser.instance.user == null)
			{
				CoreUser.instance.FacebookLogin();
			}
			return;
		case AchievementType.FB_SHARE:
			if (statusIcon.gameObject.activeSelf)
			{
				FacebookShare();
			}
			return;
		case AchievementType.TWITTER_SHARE:
			if (statusIcon.gameObject.activeSelf)
			{
				TwitterShare();
			}
			return;
		}
		if (obj.status == AchievementStatus.UNLOCK)
		{
			if (!obj.isNewChallenge())
			{
				SoundManager.instance.PlayCoins();
				TopBar.instance.UpdateDiamond(obj.diamonds, Income_Type.ACHIEVEMENT);
			}
			obj.SetStatus(AchievementStatus.RECEIVED);
			statusIcon.gameObject.SetActive(value: false);
		}
	}

	private void Update()
	{
		if (obj.type == AchievementType.TWITTER_SHARE || obj.type == AchievementType.FB_SHARE)
		{
			DateTime d = DateTime.Parse(Configuration.GetGiftTime(obj.type.ToString()));
			int num = (int)(DateTime.Now - d).TotalSeconds;
			int num2 = 86400 - num;
			int num3 = num2 / 3600;
			int num4 = (num2 - num3 * 3600) / 60;
			int num5 = num2 % 60;
			if (num2 > 0)
			{
				subText.text = string.Format(TIMEFORMAT, num3, num4, num5);
				statusIcon.gameObject.SetActive(value: false);
			}
			else
			{
				statusIcon.gameObject.SetActive(value: true);
				subText.text = LocalizationManager.instance.GetLocalizedValue("DAILY_QUEST");
			}
		}
		else if (obj.type == AchievementType.LOGIN_1ST)
		{
			if (CoreUser.instance.user != null)
			{
				statusIcon.gameObject.SetActive(value: false);
			}
			else
			{
				statusIcon.gameObject.SetActive(value: true);
			}
		}
	}

	public static bool IsFacebookShareAvailable()
	{
		DateTime d = DateTime.Parse(Configuration.GetGiftTime(AchievementType.FB_SHARE.ToString()));
		int num = (int)(DateTime.Now - d).TotalSeconds;
		return 86400 - num <= 0;
	}

	public static bool IsTwitterAvailable()
	{
		DateTime d = DateTime.Parse(Configuration.GetGiftTime(AchievementType.TWITTER_SHARE.ToString()));
		int num = (int)(DateTime.Now - d).TotalSeconds;
		return 86400 - num <= 0;
	}

	public void TwitterShare()
	{
		AnalyticHelper.FireEvent(FIRE_EVENT.Twitter_Share);
		string s = "What an awesome trending music game! \nVery funny, exicting, and challenge!\nCheck it Out! \ud83d\udc49" + Application.productName + "\ud83d\udc48 \n#" + Application.productName.Replace(" ", string.Empty) + " #Playing #Free #Music #Game #Awesome #Fun #Trending #Trend #Amanotes";
		string str = "https://t.co/swl2VZU0Hr";
		string url = "https://twitter.com/intent/tweet?url=" + str + "&text=" + WWW.EscapeURL(s);
		Application.OpenURL(url);
		waitReturn = true;
	}

	public void FacebookShare()
	{
		AnalyticHelper.FireEvent(FIRE_EVENT.Facebook_Share);
#if ENABLE_FACEBOOK_SDK
		FB.ShareLink(new Uri(Util.GetAppUrl()), null, null, null, FacebookShareBonus);
#endif
	}

#if ENABLE_FACEBOOK_SDK
	protected void FacebookShareBonus(IShareResult result)
	{
		if (string.IsNullOrEmpty(result.Error) && !result.Cancelled)
		{
			CompletedShare();
		}
	}
#endif

	private void OnApplicationPause(bool pauseStatus)
	{
		if (!pauseStatus && waitReturn && statusIcon.gameObject.activeSelf)
		{
			AchievementType type = obj.type;
			if (type != AchievementType.LOGIN_1ST && type == AchievementType.TWITTER_SHARE)
			{
				CompletedShare();
			}
		}
	}

	private void CompletedShare()
	{
		TopBar.instance.UpdateDiamond(obj.diamonds);
		statusIcon.gameObject.SetActive(value: false);
		Configuration.SetGiftTime(obj.type.ToString(), DateTime.Now.ToString());
	}
}
