using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;

public class CSVReader : MonoBehaviour
{
	public static void DebugOutputGrid(string[,] grid)
	{
		string str = string.Empty;
		for (int i = 0; i < grid.GetUpperBound(1); i++)
		{
			for (int j = 0; j < grid.GetUpperBound(0); j++)
			{
				str += grid[j, i];
				str += "|";
			}
			str += "\n";
		}
	}

	public static Dictionary<string, Item> GetConfig(string csvText)
	{
		string[,] array = SplitCsvGrid(csvText, useCommas: true);
		Dictionary<string, Item> dictionary = new Dictionary<string, Item>();
		if (!string.IsNullOrEmpty(csvText))
		{
			int num = -1;
			int num2 = -1;
			int num3 = -1;
			int num4 = -1;
			int num5 = -1;
			int num6 = -1;
			int num7 = -1;
			for (int i = 0; i < array.GetUpperBound(0); i++)
			{
				if (array[i, 0] == null)
				{
					continue;
				}
				string text = array[i, 0].Trim();
                if (text != string.Empty)
                {
                    switch (text)
                    {
                        case "Path":
                            num = i;
                            break;
                        case "Name":
                            num2 = i;
                            break;
                        case "BMP":
                            num3 = i;
                            break;
                        case "StartTime":
                            num4 = i;
                            break;
                        case "Diamonds":
                            num5 = i;
                            break;
                        case "Type":
                            num6 = i;
                            break;
                        case "MP3 version":
                            num7 = i;
                            break;
                    }
                }
			}
			for (int j = 1; j < array.GetUpperBound(1); j++)
			{
				Item item = new Item();
				for (int k = 0; k < array.GetUpperBound(0); k++)
				{
					if (array[k, j] != null)
					{
						string text2 = array[k, j].Trim();
						if (k == num)
						{
							item.path = text2;
						}
						else if (k == num2)
						{
							item.name = text2;
						}
						else if (k == num3)
						{
							item.bmp = 100f;
							float.TryParse(text2, out item.bmp);
						}
						else if (k == num4)
						{
							item.startTime = 0f;
							float.TryParse(text2, out item.startTime);
						}
						else if (k == num5)
						{
							item.diamonds = 50;
							int.TryParse(text2, out item.diamonds);
						}
						else if (k == num6)
						{
							item.type = text2;
						}
						else if (k == num7 && !string.IsNullOrEmpty(text2))
						{
							item.version = 0;
							int.TryParse(text2, out item.version);
						}
					}
				}
				if (!string.IsNullOrEmpty(item.name) && !string.IsNullOrEmpty(item.path) && !dictionary.ContainsKey(item.path))
				{
					dictionary.Add(item.path, item);
				}
			}
		}
		return dictionary;
	}

    public static string[,] SplitCsvGrid(string csvText, bool useCommas = false)
    {
        string[] array = csvText.Split("\n"[0]);
        int num = 0;
        for (int i = 0; i < array.Length; i++)
        {
            if (array[i].Length > 0)
            {
                string[] array2 = SplitCsvLine(array[i], useCommas);
                num = Mathf.Max(num, array2.Length);
            }
        }
        string[,] array3 = new string[num + 1, array.Length + 1];
        for (int j = 0; j < array.Length; j++)
        {
            string[] array4 = SplitCsvLine(array[j], useCommas);
            for (int k = 0; k < array4.Length; k++)
            {
                array3[k, j] = array4[k];
                array3[k, j] = array3[k, j].Replace("\"\"", "\"");
            }
        }
        return array3;
    }

	public static string[] SplitCsvLine(string line, bool useCommas)
	{
		if (!useCommas)
		{
			return line.Split(new char[3]
			{
				' ',
				'\t',
				','
			}, StringSplitOptions.RemoveEmptyEntries);
		}
		return (from Match m in Regex.Matches(line, "(((?<x>(?=[,\\r\\n]+))|\"(?<x>([^\"]|\"\")+)\"|(?<x>[^,\\r\\n]+)),?)", RegexOptions.ExplicitCapture)
			select m.Groups[1].Value).ToArray();
	}
}
