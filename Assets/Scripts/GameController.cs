using System;
using System.Collections;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public delegate void EventCall(GameSubscriber s);

    public Vector3 ballPosition;

    private Quaternion ballRotation;

    public GameStatus game;

    public int score;

    public int diamond;

    public int perfectCountMax;

    public int replayCount;

    public static GameController controller;

    public ParticleSystem streakPs;

    public ArrayList gameSubcribers;

    private float destination;

    public float pauseTime;

    public GameObject shadowBall;

    private DateTime playTime;

    private Vector2 pos;

    private float centerX;

    private void Awake()
    {
        gameSubcribers = new ArrayList();
        controller = this;
        Input.multiTouchEnabled = false;
    }

    public void callEvent(EventCall call)
    {
        IEnumerator enumerator = gameSubcribers.GetEnumerator();
        try
        {
            while (enumerator.MoveNext())
            {
                GameSubscriber s = (GameSubscriber)enumerator.Current;
                call(s);
            }
        }
        finally
        {
            IDisposable disposable;
            if ((disposable = (enumerator as IDisposable)) != null)
            {
                disposable.Dispose();
            }
        }
    }

    public void addSubcriber(GameSubscriber s)
    {
        gameSubcribers.Add(s);
    }

    private void Start()
    {
        controller = this;
        ballRotation = Ball.b.cTransform.localRotation;
        ballPosition = Ball.b.cTransform.position;
        if (SuperpoweredSDK.instance.IsReady())
        {
            GamePrepare();
        }
        else
        {
            Util.ShowMessage(LocalizationManager.instance.GetLocalizedValue("CANT_LOAD_SONG"), 1);
        }
        AnalyticHelper.Song(SONG_STATUS.song_start, Spawner.Intance.song);
        if (Configuration.instance.isTutorial)
        {
            AnalyticHelper.Tutorial("tutorial");
            AnalyticHelper.Tutorial("tutorial_start");
        }
    }

    private void Update()
    {
        if (game == GameStatus.LIVE)
        {
            OnGame();
        }
        else
        {
            OffGame();
        }
    }

    public void GamePrepare()
    {
        score = 0;
        diamond = 0;
        Ball.b.cTransform.localRotation = ballRotation;
        Ball.b.gameObject.SetActive(value: true);
        Ball.b.ph.rb.useGravity = false;
        Ball.b.ph.endlessMode = 0;
        Ball.b.mr.material.SetColor("_EmissionColor", Color.black);
        UIController.ui.gameui.SetActive(value: true);
        if (TopBar.instance != null)
        {
            TopBar.instance.GamePrepare();
        }
        Spawner.Intance.gamePrepare();
        if (streakPs != null)
        {
            streakPs.Clear();
            streakPs.Stop();
            if (streakPs.name == "Snow")
            {
                streakPs.Play();
            }
        }
        shadowBall.SetActive(value: true);
        Transform tfShadowBall = shadowBall.transform;
        Vector3 position = shadowBall.transform.position;
        Vector3 position2 = Ball.b.cTransform.position;
        tfShadowBall.position = Util.SetPositionZ(position, position2.z);
        UIController.ui.ResetSetRoadPercent();
    }

    public void GameStart()
    {
        playTime = DateTime.Now;
        shadowBall.SetActive(value: false);
        game = GameStatus.LIVE;
        callEvent(delegate (GameSubscriber s)
        {
            s.gameStart();
        });
        Ball.b.ph.enabled = true;
        Ball.b.ph.rb.useGravity = true;
        Ball.b.ph.Jump();
        Spawner.Intance.Hit(Spawner.Intance.blockList[0], 0);
        if (streakPs != null)
        {
            streakPs.Play();
        }
        if (replayCount == 0)
        {
            PlayerPrefs.SetInt("PlayCount", PlayerPrefs.GetInt("PlayCount", 0) + 1);
        }
    }

    public void GameContinue(SONG_PLAY_COST cost)
    {
        replayCount++;
        AnalyticHelper.Song(SONG_STATUS.song_revival, Spawner.Intance.song, Spawner.Intance.savedMusicTime, replayCount, cost);
        Ball.b.mr.material.SetColor("_EmissionColor", Color.black);
        Ball.b.cTransform.localRotation = ballRotation;
        Ball.b.ph.rb.useGravity = false;
        UIController.ui.gameui.SetActive(value: true);
        if (game == GameStatus.DIE)
        {
            callEvent(delegate (GameSubscriber s)
            {
                s.gameContinue();
            });
        }
        shadowBall.SetActive(value: true);
        Transform transform = shadowBall.transform;
        Vector3 position = shadowBall.transform.position;
        Vector3 position2 = Ball.b.cTransform.position;
        transform.position = Util.SetPositionZ(position, position2.z);
    }

    public void GameStop()
    {
        game = GameStatus.DIE;
        if (replayCount < RemoteVariables.MaxContinue && score > 10 && !Configuration.instance.isDebug)
        {
            UIController.ui.continueui.SetActive(value: true);
        }
        else
        {
            UIController.ui.gameover.SetActive(value: true);
            if (Configuration.isAdAvailable())
            {
                Configuration.SetTutorialAdOn();
                AdsManager.instance.RequestInterstitialVideo();
            }
        }
        callEvent(delegate (GameSubscriber s)
        {
            s.gameOver();
        });
        Configuration.instance.playDuration += (float)(DateTime.Now - playTime).TotalSeconds;
        playTime = DateTime.Now;
    }

    public int GetStars()
    {
        int result = 0;
        float num = Spawner.Intance.fullTime / Spawner.Intance.jumpTime;
        float num2 = (float)score / num;
        if (num2 >= 0.3f)
        {
            result = 1;
        }
        if (num2 >= 0.6f)
        {
            result = 2;
        }
        if (num2 >= 1f)
        {
            result = 3;
        }
        return result;
    }

    private void OnGame()
    {
        if (Mathf.Approximately(Time.timeScale, 1f))
        {
            callEvent(delegate (GameSubscriber s)
            {
                s.onGame();
            });
        }
        GameInput();
    }

    private void OffGame()
    {
        if (SuperpoweredSDK.instance.IsReady())
        {
            pos = UnityEngine.Input.mousePosition;
            centerX = 0f;
            callEvent(delegate (GameSubscriber s)
            {
                s.offGame();
            });
            if (Input.GetMouseButtonDown(0) && UIController.ui.gameui.activeSelf)
            {
                GameStart();
            }
        }
    }

    private void GameInput()
    {
        if (Input.GetMouseButtonDown(0))
        {
            pos = UnityEngine.Input.mousePosition;
            Vector3 position = Ball.b.cTransform.position;
            centerX = position.x;
            if (UIController.ui.pauseui.gameObject.activeSelf)
            {
                UIController.ui.pauseui.Resume();
            }
        }
        else if (Configuration.instance.isTutorial && Input.GetMouseButtonUp(0) && Mathf.Approximately(Time.timeScale, 1f))
        {
            Vector3 position2 = Ball.b.cTransform.position;
            float y = position2.y;
            Vector3 position3 = Spawner.Intance.block.transform.position;
            if (y > position3.y)
            {
                SuperpoweredSDK.instance.SmoothStop(0.5f);
                pauseTime = SuperpoweredSDK.instance.GetPosition();
                Time.timeScale = 0f;
                UIController.ui.pauseui.gameObject.SetActive(value: true);
            }
        }
        if (Mathf.Approximately(Time.timeScale, 1f))
        {
            if (Input.GetMouseButton(0))
            {
                float num = centerX;
                Vector3 mousePosition = UnityEngine.Input.mousePosition;
                destination = num + (mousePosition.x - pos.x) * RemoteVariables.FingerSpeed / (float)Screen.width;
            }
            float timeScale = Ball.b.ph.timeScale;
            float num2 = destination;
            Vector3 position4 = Ball.b.cTransform.position;
            float num3 = timeScale * (num2 - position4.x) / RemoteVariables.FingerSensitivity;
            num3 *= Spawner.Intance.bmp / 100f;
            Ball.b.cTransform.position += Vector3.right * num3;
            Ball.b.cTransform.Rotate(Vector3.right * Ball.b.ph.forwardSpeed * (Spawner.Intance.GetCurrentJumpTime(count: false) / Spawner.Intance.jumpTime) + Vector3.up * num3 * 2f, Space.World);
        }
    }
}
