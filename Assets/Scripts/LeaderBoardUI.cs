using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class LeaderBoardUI : MonoBehaviour
{
	public static LeaderBoardUI instance;

	public Image profileImage;

	public Text profileText;

	public Text titleText;

	public Text diamondText;

	private LocalizationManager.LanguageChangeHandler handler;

	private void Awake()
	{
		instance = this;
	}

	private void Start()
	{
		if (CoreUser.instance.user != null)
		{
			SetProfilePhoto(CoreUser.instance.user.photoUrl);
			if (CoreUser.instance.loadedFriend)
			{
				FriendLoadedTrigger();
			}
		}
	}

	public void FacebookLogin_Click()
	{
		SoundManager.instance.PlayGameButton();
		CoreUser.instance.FacebookLogin();
	}

	public void SetProfilePhoto(string url)
	{
		if (profileImage != null)
		{
			if (url == null)
			{
				profileImage.transform.gameObject.SetActive(value: true);
				profileImage.transform.parent.Find("EventEgg").localPosition = profileImage.transform.localPosition + Vector3.down * 80f;
			}
			else
			{
				StartCoroutine(UpdateProfilePhoto(url));
			}
		}
	}

	public void FriendLoadedTrigger()
	{
		profileText.text = CoreUser.instance.user.GetMyRank("Total").ToString();
	}

	private IEnumerator UpdateProfilePhoto(string url)
	{
		WWW www = new WWW(url);
		try
		{
			yield return www;
			if (www.error == null)
			{
				profileImage.transform.gameObject.SetActive(value: true);
				profileImage.sprite = Sprite.Create(www.texture, new Rect(0f, 0f, www.texture.width, www.texture.height), new Vector2(0f, 0f));
				profileImage.transform.parent.GetComponent<Image>().enabled = true;
				profileImage.transform.parent.parent.GetComponent<Image>().color = Color.white;
				www.Dispose();
			}
		}
		finally
		{
            //@TODO
			//base._003C_003E__Finally0();
		}
	}

	public void ShowBoard(string boardID, int yourScore)
	{
		if (BoardManager.instance == null)
		{
			GameObject popup = Util.ShowPopUp("LeaderBoard");
			popup.GetComponent<BoardManager>().SetupBoard(boardID, yourScore);
		}
	}

	private void LanguageHasChanged()
	{
		if (CoreUser.instance.user != null)
		{
			titleText.text = LocalizationManager.instance.GetLocalizedValue("SHARE_YOUR_FRIENDS");
		}
		else
		{
			titleText.text = LocalizationManager.instance.GetLocalizedValue("LOGIN_DESC");
		}
		diamondText.text = LocalizationManager.instance.GetLocalizedValue("GET_FREE_X_DIAMONDS").Replace("10", "20");
	}

    private void OnEnable()
    {
		LocalizationManager.OnLanguageChange += LanguageHasChanged;
        LanguageHasChanged();
    }

    private void OnDisable()
    {
		LocalizationManager.OnLanguageChange -= LanguageHasChanged;
    }
}
