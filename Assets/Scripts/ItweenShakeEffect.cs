using System;
using System.Collections;
using UnityEngine;

public class ItweenShakeEffect : MonoBehaviour
{
	private Vector3 originPosition;

	private Quaternion originRotation;

	private float shake_decay = 0.001f;

	private float shake_intensity = 0.2f;

	private float temp_shake_intensity;

	private bool waitsharking;

	private GameObject badge;

	private void Start()
	{
		originPosition = base.transform.localPosition;
		originRotation = base.transform.localRotation;
		badge = base.transform.parent.Find("Badge").gameObject;
	}

	private void OnEnable()
	{
		if (isShake())
		{
			temp_shake_intensity = shake_intensity;
			waitsharking = false;
		}
	}

	private void Update()
	{
		if (isShake())
		{
			if (temp_shake_intensity > 0f)
			{
				base.transform.localPosition = originPosition + UnityEngine.Random.insideUnitSphere * temp_shake_intensity * 20f;
				base.transform.localRotation = new Quaternion(originRotation.x + UnityEngine.Random.Range(0f - temp_shake_intensity, temp_shake_intensity) * 0.2f, originRotation.y + UnityEngine.Random.Range(0f - temp_shake_intensity, temp_shake_intensity) * 0.2f, originRotation.z + UnityEngine.Random.Range(0f - temp_shake_intensity, temp_shake_intensity) * 0.2f, originRotation.w + UnityEngine.Random.Range(0f - temp_shake_intensity, temp_shake_intensity) * 0.2f);
				temp_shake_intensity -= shake_decay;
			}
			else if (!waitsharking)
			{
				waitsharking = true;
				StartCoroutine(Shake());
			}
			badge.SetActive(value: true);
		}
		else
		{
			base.transform.localPosition = originPosition;
			base.transform.localRotation = originRotation;
			badge.SetActive(value: false);
		}
	}

	private IEnumerator Shake()
	{
		yield return new WaitForSeconds(1f);
		temp_shake_intensity = shake_intensity;
		waitsharking = false;
	}

	public static bool isShake()
	{
		DateTime d = DateTime.Parse(Configuration.GetGiftTime("hourlygift_time"));
		double totalSeconds = (DateTime.Now - d).TotalSeconds;
		return totalSeconds > 1800.0;
	}
}
