#if ENABLE_FACEBOOK_SDK
using Facebook.MiniJSON;
using Facebook.Unity;
#endif
#if ENABLE_FIREBASE
using Firebase.Auth;
using Firebase.Database;
#endif
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class UserEntry
{
	public string auId;

	public string fbId;

	public string name;

	public string photoUrl;

	public Dictionary<string, int> scores = new Dictionary<string, int>();

	public Dictionary<string, long> ranks = new Dictionary<string, long>();

	public string gameData;

	public string createdDate;

	public string lastDate;

	public string lang;

	public string version;

	public string profileKey;

	public UserEntry()
	{
	}

#if ENABLE_FIREBASE
	public UserEntry(DataSnapshot snapshot, string boardid, long rank)
	{
		auId = snapshot.Key;
		ranks[boardid] = rank;
		scores[boardid] = int.Parse(snapshot.Value.ToString());
	}

	public UserEntry(FirebaseUser user, string fbId)
	{
		auId = user.UserId;
		this.fbId = fbId;
		name = user.DisplayName;
		profileKey = "/Users/" + auId;
		photoUrl = "https://graph.facebook.com/" + fbId + "/picture?type=large";
		if (LeaderBoardUI.instance != null)
		{
			LeaderBoardUI.instance.SetProfilePhoto(photoUrl);
		}
	}

	public UserEntry(DataSnapshot snapshot)
	{
		Bind(snapshot);
	}

	public void Bind(DataSnapshot snapshot)
	{
		if (snapshot.Value != null)
		{
			auId = snapshot.Key;
			profileKey = "/Users/" + auId;
			if (snapshot.Child("fbId").Value != null)
			{
				fbId = snapshot.Child("fbId").Value.ToString();
				photoUrl = "https://graph.facebook.com/" + fbId + "/picture?type=normal";
			}
			if (snapshot.Child("name").Value != null)
			{
				name = snapshot.Child("name").Value.ToString();
			}
			if (snapshot.Child("score").Value != null)
			{
				Dictionary<string, object> dictionary = (Dictionary<string, object>)snapshot.Child("score").Value;
				foreach (KeyValuePair<string, object> item in dictionary)
				{
					try
					{
						scores[item.Key] = int.Parse(item.Value.ToString());
					}
					catch
					{
					}
				}
			}
			if (snapshot.Child("gameData").Value != null)
			{
				gameData = snapshot.Child("gameData").Value.ToString();
			}
			if (snapshot.Child("lastDate").Value != null)
			{
				lastDate = snapshot.Child("lastDate").Value.ToString();
			}
			if (snapshot.Child("createdDate").Value != null)
			{
				createdDate = snapshot.Child("createdDate").Value.ToString();
			}
			if (snapshot.Child("lang").Value != null)
			{
				lang = snapshot.Child("lang").Value.ToString();
			}
			if (snapshot.Child("version").Value != null)
			{
				version = snapshot.Child("version").Value.ToString();
			}
		}
	}
#endif

	public void SaveProfiles()
	{
		if (auId != null)
		{
			Dictionary<string, object> dictionary = new Dictionary<string, object>();
			dictionary[profileKey + "/name"] = name;
			if (fbId != null)
			{
				dictionary[profileKey + "/fbId"] = fbId;
				dictionary["/Links/" + fbId] = auId;
			}
			if (scores != null)
			{
				Dictionary<string, object> dictionary2 = new Dictionary<string, object>();
				foreach (KeyValuePair<string, int> score in scores)
				{
					dictionary2[score.Key] = score.Value;
					dictionary["/LeaderBoards/" + score.Key + "/" + auId] = score.Value;
				}
				dictionary[profileKey + "/score"] = dictionary2;
			}
#if ENABLE_FIREBASE
			CoreUser.instance.db.UpdateChildrenAsync(dictionary);
#endif
		}
	}

	public void Delete()
	{
		if (auId != null)
		{
#if ENABLE_FIREBASE
			CoreUser.instance.db.Child(profileKey).RemoveValueAsync();
			CoreUser.instance.db.Child("/LeaderBoards/Total/" + auId).RemoveValueAsync();
			foreach (Item value in RemoteFilesData.instance.songList.Values)
			{
				CoreUser.instance.db.Child("/LeaderBoards/" + Util.SongToBoardId(value.path) + "/" + auId).RemoveValueAsync();
			}
#endif
		}
	}

	public void SaveScores(string boardId, int score, int total)
	{
		scores[boardId] = score;
		scores["Total"] = total;
		Dictionary<string, object> dictionary = new Dictionary<string, object>();
		dictionary["/LeaderBoards/" + boardId + "/" + auId] = score;
		dictionary["/LeaderBoards/Total/" + auId] = total;
		dictionary[profileKey + "/score/" + boardId] = score;
		dictionary[profileKey + "/score/Total"] = total;
#if ENABLE_FIREBASE
		CoreUser.instance.db.UpdateChildrenAsync(dictionary);
#endif
		SetMyRank(boardId);
		SetMyRank("Total");
	}

	public void Backup()
	{
		Dictionary<string, object> dictionary = new Dictionary<string, object>();
		dictionary[profileKey + "/gameData"] = GetGameData();
		dictionary[profileKey + "/lang"] = Configuration.GetLanguageID();
		dictionary[profileKey + "/lastDate"] = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
		dictionary[profileKey + "/version"] = Application.version;
		if (createdDate != null)
		{
			dictionary[profileKey + "/createdDate"] = createdDate;
			createdDate = null;
		}
#if ENABLE_FIREBASE
		CoreUser.instance.db.UpdateChildrenAsync(dictionary);
#endif
	}

	public long GetMyRank(string boardID)
	{
		if (!ranks.ContainsKey(boardID) || ranks[boardID] == 0)
		{
			SetMyRank(boardID);
		}
		if (ranks.ContainsKey(boardID))
		{
			return ranks[boardID];
		}
		return 0L;
	}

	public void SetMyRank(string boardID)
	{
		CoreUser.instance.GetFriendList(boardID);
	}

	private string GetGameData()
	{
		Dictionary<string, object> dictionary = new Dictionary<string, object>();
		dictionary["noads"] = PlayerPrefs.GetInt("noads");
		dictionary["bonus_fb_login"] = PlayerPrefs.GetInt("bonus_fb_login");
		dictionary["bonus_sharing"] = PlayerPrefs.GetInt("bonus_sharing");
		dictionary["diamonds"] = PlayerPrefs.GetInt("diamonds");
		dictionary["themes"] = PlayerPrefs.GetString("themes");
		dictionary["selected_theme"] = PlayerPrefs.GetInt("selected_theme");
		dictionary["balls"] = PlayerPrefs.GetString("balls");
		dictionary["selected_ball"] = PlayerPrefs.GetInt("selected_ball");
		dictionary["RateLater"] = PlayerPrefs.GetInt("RateLater", 0);
		dictionary["dailygift_number"] = PlayerPrefs.GetInt("dailygift_number");
		dictionary["dailygift_date"] = PlayerPrefs.GetString("dailygift_date");
		dictionary["retry"] = PlayerPrefs.GetInt("retry");
		dictionary["revive"] = PlayerPrefs.GetInt("revive");
		dictionary["istop1"] = PlayerPrefs.GetInt("istop1");
		dictionary["perfect_count"] = PlayerPrefs.GetInt("perfect_count");
		foreach (Item value in RemoteFilesData.instance.songList.Values)
		{
			if (CoreData.GetSongType(value.path, value.type) == SONGTYPE.OPEN)
			{
				dictionary["beststars" + value.path] = PlayerPrefs.GetInt("beststars" + value.path);
				if (!scores.ContainsKey(Util.SongToBoardId(value.path)))
				{
					dictionary[value.path] = PlayerPrefs.GetString(value.path);
				}
			}
		}
		foreach (AchievementObj achievement in CoreData.instance.achievementList)
		{
			if (achievement.status == AchievementStatus.RECEIVED)
			{
				dictionary[AchievementObj.PRE_STATUS + achievement.type.ToString()] = PlayerPrefs.GetInt(AchievementObj.PRE_STATUS + achievement.type.ToString(), 0);
			}
		}
        //return dictionary.ToJson();
        return JsonConvert.SerializeObject(dictionary);
	}

	public void RestoreGameData()
	{
		if (gameData == null)
		{
			return;
		}
        //object obj = Json.Deserialize(gameData);
        object obj = JsonConvert.DeserializeObject<object>(gameData);
		if (obj != null)
		{
			PlayerPrefs.SetInt("opened_song_total", 0);
			if (scores != null)
			{
				foreach (Item value in RemoteFilesData.instance.songList.Values)
				{
					PlayerPrefs.DeleteKey("bestscore" + value.path);
					PlayerPrefs.DeleteKey("beststars" + value.path);
					PlayerPrefs.DeleteKey(value.path);
				}
				foreach (KeyValuePair<string, int> score in scores)
				{
					PlayerPrefs.SetInt("bestscore" + Util.BoardToSongId(score.Key), score.Value);
					PlayerPrefs.SetString(Util.BoardToSongId(score.Key), SONGTYPE.OPEN.ToString());
				}
			}
			foreach (AchievementObj achievement in CoreData.instance.achievementList)
			{
				PlayerPrefs.DeleteKey(AchievementObj.PRE_STATUS + achievement.type.ToString());
			}
			Dictionary<string, object> dictionary = (Dictionary<string, object>)obj;
			foreach (KeyValuePair<string, object> item in dictionary)
			{
				switch (item.Key)
				{
				case "diamonds":
				{
					int result = 0;
					if (int.TryParse(item.Value.ToString(), out result))
					{
						CoreData.instance.SetDiamonds(result);
					}
					break;
				}
				case "noads":
				case "bonus_fb_login":
				case "bonus_sharing":
				case "selected_theme":
				case "selected_ball":
				case "dailygift_number":
				case "RateLater":
				case "retry":
				case "revive":
				case "istop1":
				case "perfect_count":
					DataUpdate_Int(item);
					break;
				default:
					if (item.Key.Contains("beststars") || item.Key.Contains("bestscore") || item.Key.Contains(AchievementObj.PRE_STATUS))
					{
						DataUpdate_Int(item);
					}
					else
					{
						PlayerPrefs.SetString(item.Key, item.Value.ToString());
					}
					break;
				}
			}
		}
		CoreData.instance.UpdateAchievement();
		PlayerPrefs.Save();
	}

	private void DataUpdate_Int(KeyValuePair<string, object> pair)
	{
		int result = 0;
		if (int.TryParse(pair.Value.ToString(), out result))
		{
			PlayerPrefs.SetInt(pair.Key, result);
		}
	}
}
