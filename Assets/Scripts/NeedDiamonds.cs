using UnityEngine.UI;

public class NeedDiamonds : PopupUI
{
	public Text bonusText;

	private void Start()
	{
		bonusText.text = LocalizationManager.instance.GetLocalizedValue("X_DIAMONDS").Replace("10", Configuration.instance.diamondsVideoBonus.ToString());
	}
}
