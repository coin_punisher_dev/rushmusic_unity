using System.Collections;
using UnityEngine;

public class Diamond : MonoBehaviour
{
	public ParticleSystem effect;

	public MeshRenderer mesh;

	public bool x2;

	private void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.name == GAMETAG.Ball && mesh.enabled)
		{
			GameController.controller.diamond++;
			SoundManager.instance.PlayCoins();
			StartCoroutine(BrustEffect());
		}
	}

	private IEnumerator BrustEffect()
	{
		mesh.enabled = false;
		effect.Play();
		yield return new WaitForSeconds(0.6f);
		mesh.enabled = true;
		base.gameObject.transform.parent.gameObject.SetActive(value: false);
	}
}
