using System.Collections;
using UnityEngine;

public class DeathTrigger : MonoBehaviour, GameSubscriber
{
	public void gameOver()
	{
	}

	public void gamePrepare()
	{
	}

	public void gameStart()
	{
	}

	public void gameContinue()
	{
	}

	public void offGame()
	{
	}

	public void onGame()
	{
	}

	private void Start()
	{
		GameController.controller.addSubcriber(this);
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag(GAMETAG.Ball) && GameController.controller.game == GameStatus.LIVE)
		{
			SoundManager.instance.PlayDie();
			GameController.controller.GameStop();
		}
	}

	private IEnumerator DelayDie()
	{
		yield return new WaitForSeconds(2f);
		GameController.controller.GameStop();
	}

	private void OnCollisionEnter(Collision other)
	{
		if (other.gameObject.CompareTag(GAMETAG.Ball))
		{
			Ball.b.ph.rb.velocity = -1f * Ball.b.ph.rb.velocity;
			if (GameController.controller.game == GameStatus.LIVE)
			{
				SoundManager.instance.PlayDie();
				StartCoroutine(DelayDie());
			}
			GameController.controller.game = GameStatus.P_DIE;
		}
	}
}
