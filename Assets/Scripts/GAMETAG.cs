using System.Runtime.InteropServices;

[StructLayout(LayoutKind.Sequential, Size = 1)]
public struct GAMETAG
{
	public const string Ball = "Ball";

	public const string BlockClone = "BlockClone";

	public const string Block = "Block";

	public const string Perfect = "Perfect";

	public const string Tree = "Tree";

	public const string Ground = "Ground";

	public const string Road = "Road";
}
