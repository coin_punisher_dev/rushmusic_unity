//@TODO ENABLE_FIREBASE
#if ENABLE_FIREBASE
using Firebase.Database;
#endif

using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

public class BoardManager : PopupUI
{
	public Sprite bgMe;

	public Sprite bgDefault;

	public Sprite avatarDefault;

	public Sprite rankBgDefault;

	public Sprite total_rank1;

	public Sprite total_rank2;

	public Sprite total_rank3;

	public Sprite song_rank1;

	public Sprite song_rank2;

	public Sprite song_rank3;

	public Text titleText;

	public Text tab1Text;

	public Text tab2Text;

	public RectTransform container;

	public GameObject activeTab;

	public BoardItem boardItem;

	public BoardItem boardItemMe;

	private List<BoardItem> boardItems = new List<BoardItem>();

	public GameObject friendButton;

	public GameObject globalButton;

	private string boardID;

	private int myScore;

	private long myRank;

	private float positionY;

	private int currentEntry;

	private List<UserEntry> globalList;

	private List<UserEntry> friendList;

	public static BoardManager instance;

	public GameObject loading;

	public GameObject facebookLogin;

	private bool activedGlobal;

	private bool isLoadingFriend;

	private bool isLoadingGlobal;

	private CoreUser.FriendLoadedHandler friendloadedHandler;

	private CoreUser.LoggedHandler loggedHandler;

	private int limit = 10;

	private void Awake()
	{
		instance = this;
		SubscribeHandlers();
		Util.UpdateBannerContent(base.transform);
		Configuration.instance.SetCurrentLocation(LOCATION_NAME.LEADERBOARD, isPopup: true);
	}

	private void SubscribeHandlers()
	{
		friendloadedHandler = FriendLoaded;
		loggedHandler = UserLogged;
		CoreUser.instance.onFriendLoaded += friendloadedHandler;
		CoreUser.instance.onLogged += loggedHandler;
	}

	private void InsertEntries(List<UserEntry> entries, bool isGlobal = false)
	{
		if (isGlobal)
		{
			if (!activedGlobal)
			{
				return;
			}
			entries.Sort((UserEntry x, UserEntry y) => x.ranks[boardID].CompareTo(y.ranks[boardID]));
		}
		else if (activedGlobal)
		{
			return;
		}
		for (int i = 0; i < boardItems.Count; i++)
		{
			UnityEngine.Object.Destroy(boardItems[i].gameObject);
		}
		boardItems.Clear();
		loading.SetActive(value: false);
		currentEntry = 0;
		positionY = 0f;
		if (!base.gameObject.activeSelf)
		{
			return;
		}
		bool flag = true;
		long rank = myRank;
		if (flag)
		{
			positionY += 50f;
		}
		for (int j = 0; j < entries.Count; j++)
		{
			if (entries[j].auId == CoreUser.instance.user.auId)
			{
				rank = entries[j].ranks[boardID];
			}
			InsertEntry(entries[j]);
		}
		container.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, positionY + 150f);
		if (flag)
		{
			boardItemMe.gameObject.SetActive(value: true);
			boardItemMe.SetupMe(rank, myScore, CoreUser.instance.user.photoUrl);
		}
		else
		{
			boardItemMe.gameObject.SetActive(value: false);
		}
		if (isGlobal && myRank == 1)
		{
			PlayerPrefs.SetInt("istop1", 1);
			AchievementObj.CheckTypes(new AchievementType[1]
			{
				AchievementType.HIGHEST_SCORE
			});
		}
	}

	private void InsertEntry(UserEntry entry)
	{
		if (currentEntry >= boardItems.Count)
		{
			GameObject gameObject = Object.Instantiate(boardItem.gameObject);
			boardItems.Add(gameObject.GetComponent<BoardItem>());
			gameObject.transform.transform.SetParent(boardItem.transform.parent, worldPositionStays: false);
		}
		boardItems[currentEntry].gameObject.SetActive(value: true);
		boardItems[currentEntry].Setup(entry, boardID);
		StartCoroutine(DelayUpdate(boardItems[currentEntry].gameObject, Vector3.down * positionY));
		positionY += 105f;
		currentEntry++;
	}

	private IEnumerator DelayUpdate(GameObject item, Vector3 localPosition)
	{
		yield return null;
		if (item != null)
		{
			item.transform.localPosition += localPosition;
		}
	}

	public void ShowFriendsBoard()
	{
		friendButton.SetActive(value: true);
		globalButton.SetActive(value: false);
		ShowLeaderBoard();
	}

	public void ShowGlobalBoard()
	{
		friendButton.SetActive(value: false);
		globalButton.SetActive(value: true);
		ShowLeaderBoard(isGlobal: true);
	}

	public void FacebookLogin_Click()
	{
		SoundManager.instance.PlayGameButton();
		CoreUser.instance.FacebookLogin();
	}

	public void SetupBoard(string boardID, int myScore)
	{
		container.anchoredPosition = Vector2.zero;
		base.gameObject.SetActive(value: true);
		if (this.boardID != boardID)
		{
			if (boardID == "Total")
			{
				titleText.text = LocalizationManager.instance.GetLocalizedValue("LEADERBOARD");
			}
			else
			{
				titleText.text = RemoteFilesData.instance.songList[Util.BoardToSongId(boardID)].name;
			}
			this.boardID = boardID;
			this.myScore = myScore;
			globalList = null;
			friendList = null;
			isLoadingFriend = false;
			isLoadingGlobal = false;
		}
		ShowFriendsBoard();
	}

	private void ShowLeaderBoard(bool isGlobal = false)
	{
		activedGlobal = isGlobal;
		boardItemMe.gameObject.SetActive(value: false);
		if (CoreUser.instance.user != null)
		{
			facebookLogin.SetActive(value: false);
			friendButton.transform.parent.gameObject.SetActive(value: true);
			globalButton.transform.parent.gameObject.SetActive(value: true);
			for (int i = 0; i < boardItems.Count; i++)
			{
				boardItems[i].gameObject.SetActive(value: false);
			}
			if (isGlobal)
			{
				if (isLoadingGlobal)
				{
					loading.SetActive(value: true);
				}
				else if (globalList == null)
				{
					AnalyticHelper.Rank_View(FIRE_EVENT.Global_Rank_View, boardID);
					globalList = new List<UserEntry>();
					loading.SetActive(value: true);
					isLoadingGlobal = true;
#if ENABLE_FIREBASE
					CoreUser.instance.db.Child("LeaderBoards").Child(boardID).OrderByValue()
						.StartAt(myScore)
						.LimitToFirst(999999)
						.GetValueAsync()
						.ContinueWith(delegate(Task<DataSnapshot> task)
						{
							BoardManager boardManager = this;
							if (!task.IsFaulted && task.IsCompleted)
							{
								myRank = task.Result.ChildrenCount;
								if (boardID == "Total" || myRank < 50)
								{
									long num = 0L;
									long num2 = task.Result.ChildrenCount - limit * 2;
									foreach (DataSnapshot child in task.Result.Children)
									{
										num++;
										if (num > num2)
										{
											UserEntry item = new UserEntry(child, boardID, task.Result.ChildrenCount + 1 - num);
											globalList.Add(item);
										}
									}
									if (globalList.Count < limit * 2)
									{
										long rank = globalList.Count;
										CoreUser.instance.db.Child("LeaderBoards").Child(boardID).OrderByValue()
											.EndAt(myScore - 1)
											.LimitToLast(limit * 2 - globalList.Count)
											.GetValueAsync()
											.ContinueWith(delegate(Task<DataSnapshot> task2)
											{
												if (!task.IsFaulted && task.IsCompleted)
												{
													rank += task2.Result.ChildrenCount;
													foreach (DataSnapshot child2 in task2.Result.Children)
													{
														UserEntry item4 = new UserEntry(child2, boardManager.boardID, rank);
														boardManager.globalList.Add(item4);
														rank--;
													}
													boardManager.UpdateEntries();
												}
											});
									}
									else
									{
										UpdateEntries();
									}
								}
								else
								{
									long rank2 = task.Result.ChildrenCount;
									foreach (DataSnapshot child3 in task.Result.Children)
									{
										UserEntry item2 = new UserEntry(child3, boardID, rank2);
										globalList.Add(item2);
										if (task.Result.ChildrenCount - rank2 == limit)
										{
											break;
										}
										rank2--;
									}
									rank2 = task.Result.ChildrenCount;
									CoreUser.instance.db.Child("LeaderBoards").Child(boardID).OrderByValue()
										.EndAt(myScore - 1)
										.LimitToLast(limit)
										.GetValueAsync()
										.ContinueWith(delegate(Task<DataSnapshot> task2)
										{
											if (!task.IsFaulted && task.IsCompleted)
											{
												rank2 += task2.Result.ChildrenCount;
												foreach (DataSnapshot child4 in task2.Result.Children)
												{
													UserEntry item3 = new UserEntry(child4, boardManager.boardID, rank2);
													boardManager.globalList.Add(item3);
													rank2--;
												}
												boardManager.UpdateEntries();
											}
										});
								}
							}
                    });
#endif
                }
                else
				{
					InsertEntries(globalList, isGlobal: true);
				}
			}
			else if (isLoadingFriend)
			{
				loading.SetActive(value: true);
			}
			else if (friendList == null)
			{
				loading.SetActive(value: true);
				isLoadingFriend = true;
				AnalyticHelper.Rank_View(FIRE_EVENT.Friend_Rank_View, boardID);
				CoreUser.instance.ReloadFriends();
			}
			else
			{
				InsertEntries(friendList);
			}
		}
		else
		{
			facebookLogin.SetActive(value: true);
			friendButton.transform.parent.gameObject.SetActive(value: false);
			globalButton.transform.parent.gameObject.SetActive(value: false);
		}
	}

	private void UpdateEntries()
	{
#if ENABLE_FIREBASE
		int itemDone = 0;
		foreach (UserEntry entry in globalList)
		{
			CoreUser.instance.db.Child("Users").Child(entry.auId).GetValueAsync()
				.ContinueWith(delegate(Task<DataSnapshot> task)
				{
					if (!task.IsFaulted && task.IsCompleted)
					{
						int value = entry.scores[boardID];
						entry.Bind(task.Result);
						entry.scores[boardID] = value;
					}
					itemDone++;
					if (itemDone == globalList.Count)
					{
						isLoadingGlobal = false;
						InsertEntries(globalList, isGlobal: true);
					}
				});
		}
#endif
    }

    private void UserLogged()
	{
		if (base.gameObject.activeSelf)
		{
			loading.SetActive(value: true);
			facebookLogin.SetActive(value: false);
		}
	}

	private void FriendLoaded()
	{
		isLoadingFriend = false;
		friendList = CoreUser.instance.GetFriendList(boardID);
		if (base.gameObject != null && base.gameObject.activeSelf)
		{
			ShowFriendsBoard();
		}
	}

	private void OnDestroy()
	{
		CoreUser.instance.onFriendLoaded -= friendloadedHandler;
		CoreUser.instance.onLogged -= loggedHandler;
	}
}
