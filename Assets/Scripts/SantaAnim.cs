using UnityEngine;

public class SantaAnim : MonoBehaviour
{
	public Animator animator;

	public RectTransform rect;

	private int positionX = 350;

	private int positionY = 500;

	private Vector2 endPositions;

	private Vector2 startPositions;

	private int direction;

	private float startTime;

	private float v0 = 40f;

	private float a = -3.5f;

	private void Start()
	{
		RandomPosition();
	}

	private void RandomPosition()
	{
		startTime = Time.time;
		Quaternion localRotation = rect.transform.localRotation;
		if (UnityEngine.Random.Range(0, 2) == 0)
		{
			direction = 1;
			localRotation.eulerAngles = new Vector3(0f, 180f, 0f);
		}
		else
		{
			direction = -1;
			localRotation.eulerAngles = Vector3.zero;
		}
		rect.transform.localRotation = localRotation;
		startPositions = new Vector2(positionX * direction, UnityEngine.Random.Range((float)positionY / 2f, (float)positionY / 4f));
		endPositions = new Vector2(positionX * direction * -1, 0f);
		rect.anchoredPosition = startPositions;
	}

	private void Update()
	{
		float num = Time.time - startTime;
		if (num > 25f)
		{
			RandomPosition();
		}
		else
		{
			if (direction == 0)
			{
				return;
			}
			Vector2 anchoredPosition = rect.anchoredPosition;
			if (!(anchoredPosition.x > endPositions.x) || direction != -1)
			{
				Vector2 anchoredPosition2 = rect.anchoredPosition;
				if (!(anchoredPosition2.x < endPositions.x) || direction != 1)
				{
					rect.anchoredPosition -= Vector2.right * direction * Time.deltaTime * 60f;
					Vector2 anchoredPosition3 = rect.anchoredPosition;
					anchoredPosition3.y = v0 * num + 0.5f * a * num * num + startPositions.y;
					rect.anchoredPosition = anchoredPosition3;
				}
			}
		}
	}
}
