using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Networking;

public class RemoteFilesData : MonoBehaviour
{
	public delegate void SongListLoadedHandler();

	public const string languageFileName = "localization.csv";

	public static string configFileName = "songconfig.json";
    public static string configFileNameNew = "songconfig_new.json";

	public static RemoteFilesData instance;

#if UNITY_2018_3_OR_NEWER
    UnityWebRequest _webRequest;
#else
    private WWW _www;
#endif

	[HideInInspector]
	public FILE_DATA_STATUS currentFileStatus;

	public Dictionary<string, Item> songList;

	public Dictionary<string, Item> localSongList;

    //@TODO

    private string[] alreadySongs = 
    {
        //"TrapHipHop.mp3"
        //"None.mp3"
    };

    //private string[] alreadySongs =
    //{
    //};

    public bool isCheckedVersions;

	public event SongListLoadedHandler onSongListLoaded;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
			UnityEngine.Object.DontDestroyOnLoad(base.gameObject);
			songList = new Dictionary<string, Item>();
			localSongList = new Dictionary<string, Item>();
		}
		else if (instance != null)
		{
			UnityEngine.Object.Destroy(base.gameObject);
		}
	}

	public void CheckVersions()
	{
		if (!isCheckedVersions)
		{
			isCheckedVersions = true;
            System.Version v = new System.Version(Configuration.GetDataVersion());
            System.Version v2 = new System.Version(RemoteVariables.DataVersion);
            Debug.LogErrorFormat("RemoteFilesData.CheckVersions v:{0} - v2:{1}", v, v2);
            if (v < v2)
			{
				StartCoroutine(DownloadCSVData(configFileName));
			}
			else
			{
				LoadConfigData();
			}
			v = new System.Version(Configuration.GetLanguageVersion());
			v2 = new System.Version(RemoteVariables.LanguageVersion);
			if (v < v2)
			{
				StartCoroutine(DownloadCSVData(languageFileName));
			}

            LoadSongLocal();
        }
    }

	public void CopyFileStreaming(string fileName)
	{
		StartCoroutine(IECopyFileStreaming(fileName));
	}

	public bool IsReady()
	{
		if (RemoteVariables.loaded)
		{
			CheckVersions();
		}
		return songList.Count > 0 && RemoteVariables.loaded;
	}

	public bool IsNewData(string song)
	{
		currentFileStatus = FILE_DATA_STATUS.NONE;
		if (File.Exists(LocalPath(song)))
		{
			if (songList[song].version > CoreData.GetSongVersion(song))
			{
				currentFileStatus = FILE_DATA_STATUS.NEED_UPDATE;
				return true;
			}
			currentFileStatus = FILE_DATA_STATUS.READY;
			return false;
		}
		if (alreadySongs.Length > 0)
		{
			for (int i = 0; i < alreadySongs.Length; i++)
			{
				if (song == alreadySongs[i])
				{
					StartCoroutine(IECopyFileStreaming(song));
					return false;
				}
			}
			return true;
		}
		StartCoroutine(IECopyFileStreaming(song));
		return false;
	}

	public void PrepareRemoteData(string song)
	{
		StartCoroutine(DownloadMp3Data(song));
	}

	private IEnumerator DownloadMp3Data(string song)
	{
        //@TODO DEBUG DownloadMp3Data
        var path = RemotePath(song);
        Debug.LogError("RemoteFilesData.DownloadMp3Data song:" + song + " at " + path);
#if UNITY_2018_3_OR_NEWER
        /*_webRequest = UnityWebRequest.Get(path);
        yield return _webRequest.SendWebRequest();
        if(string.IsNullOrEmpty(_webRequest.error))
        {
            SaveFileBinary(_webRequest.downloadHandler.data, song);
        }*/
		currentFileStatus = FILE_DATA_STATUS.READY;
		if (songList.ContainsKey(song))
		{
			CoreData.SetSongVersion(song, songList[song].version);
		}
		yield return "";
#else
        /*_www = new WWW(path);
		yield return _www;
		if (_www.error == null)
		{
			SaveFileBinary(_www.bytes, song);
		}*/
#endif
    }

    private IEnumerator DownloadCSVData(string fileName)
	{
        //@TODO DEBUG DownloadCSVData
        var path = RemotePath(fileName);
        Debug.LogError("RemoteFilesData.DownloadCSVData fileName:" + fileName + " at " + path);
#if UNITY_2018_3_OR_NEWER
        UnityWebRequest webRequest = UnityWebRequest.Get(path);
        yield return webRequest.SendWebRequest();
        if (fileName == configFileName)
        {
            if (string.IsNullOrEmpty(webRequest.error) && webRequest.isDone)
            {
                Dictionary<string, Item> config = CSVReader.GetConfig(webRequest.downloadHandler.text);
                if (config.Count > 0)
                {
                    songList = config;
                    if (this.onSongListLoaded != null)
                    {
                        this.onSongListLoaded();
                    }
                    SaveFile(webRequest.downloadHandler.text, fileName);
                    Configuration.SetDataVersion(RemoteVariables.DataVersion);
                }
            }
            if (songList.Count == 0)
            {
                LoadConfigData();
            }
        }
        else if (fileName == languageFileName && string.IsNullOrEmpty(webRequest.error)
            && webRequest.isDone && LocalizationManager.instance.GetDictionary(webRequest.downloadHandler.text))
        {
            SaveFile(webRequest.downloadHandler.text, fileName);
            Configuration.SetLanguageVersion(RemoteVariables.LanguageVersion);
        }
#else
        WWW www = new WWW(path);
		yield return www;
		if (fileName == configFileName)
		{
			if (www.error == null && www.isDone)
			{
				Dictionary<string, Item> config = CSVReader.GetConfig(www.text);
				if (config.Count > 0)
				{
					songList = config;
					if (this.onSongListLoaded != null)
					{
						this.onSongListLoaded();
					}
					SaveFile(www.text, fileName);
					Configuration.SetDataVersion(RemoteVariables.DataVersion);
				}
			}
			if (songList.Count == 0)
			{
				LoadConfigData();
			}
		}
		else if (fileName == languageFileName && www.error == null && www.isDone && LocalizationManager.instance.GetDictionary(www.text))
		{
			SaveFile(www.text, fileName);
			Configuration.SetLanguageVersion(RemoteVariables.LanguageVersion);
		}
#endif
    }

    public float GetProgress()
	{
#if UNITY_2018_3_OR_NEWER
        if (_webRequest != null)
        {
            return _webRequest.downloadProgress;
        }
#else
        if (_www != null)
		{
			return _www.progress;
		}
#endif
        return 0f;
	}

	public string GetError()
	{
#if UNITY_2018_3_OR_NEWER
        if (_webRequest != null)
        {
            return _webRequest.error;
        }
#else
        if (_www != null)
		{
			return _www.error;
		}
#endif
		return null;
	}

	public void DownloadConfigFile()
	{
		string modifiedDate = GetModifiedDate(configFileName);
		if (modifiedDate != null)
		{
			StartCoroutine(DownloadCSVData(configFileName));
		}
		else
		{
			LoadConfigData();
		}
	}

	private string GetModifiedDate(string fileName)
	{
		try
		{
			WebRequest webRequest = WebRequest.Create(RemotePath(fileName));
			webRequest.Method = "HEAD";
			string text = null;
			using (WebResponse webResponse = webRequest.GetResponse())
			{
				text = webResponse.Headers.Get("Last-Modified");
			}
			string @string = PlayerPrefs.GetString("Last-Modified-" + fileName);
			if (@string != text || @string == string.Empty)
			{
				return text;
			}
			return null;
		}
        catch (Exception ex)
        {
            Util.ShowMessage(ex.Message.ToString());
        }
        return null;
	}

	public static string GetFileName(int level)
	{
		return level.ToString();
	}

	public void SaveFile(string data, string fileName)
	{
		try
		{
			FileInfo fileInfo = new FileInfo(LocalPath(fileName));
			StreamWriter streamWriter;
			if (!fileInfo.Exists)
			{
				streamWriter = fileInfo.CreateText();
			}
			else
			{
				fileInfo.Delete();
				streamWriter = fileInfo.CreateText();
			}
			streamWriter.Write(data);
			streamWriter.Close();
		}
		catch (Exception ex)
		{
			Util.ShowMessage(ex.Message.ToString());
		}
	}

	private void SaveFileBinary(byte[] data, string fileName)
	{
		try
		{
			FileInfo fileInfo = new FileInfo(LocalPath(fileName));
			BinaryWriter binaryWriter = new BinaryWriter(fileInfo.OpenWrite());
			binaryWriter.Write(data);
			binaryWriter.Close();
			currentFileStatus = FILE_DATA_STATUS.READY;
			if (songList.ContainsKey(fileName))
			{
				CoreData.SetSongVersion(fileName, songList[fileName].version);
			}
		}
		catch (Exception ex)
		{
			SceneFader.instance.SetError(ex.Message.ToString());
			currentFileStatus = FILE_DATA_STATUS.ERROR;
		}
	}

	public static string LoadFile(string fileName)
	{
		string result = string.Empty;
		string path = LocalPath(fileName);
		if (File.Exists(path))
		{
			StreamReader streamReader = File.OpenText(path);
			result = streamReader.ReadToEnd();
			streamReader.Close();
		}
		return result;
	}

	public static byte[] LoadFileBinary(string path)
	{
		byte[] result = null;
		if (File.Exists(path))
		{
			using (FileStream fileStream = File.OpenRead(path))
			{
				using (BinaryReader binaryReader = new BinaryReader(fileStream))
				{
					return binaryReader.ReadBytes((int)fileStream.Length);
				}
			}
		}
		return result;
	}

	public void LoadConfigData()
	{
        Debug.LogErrorFormat("RemoteFilesData.LoadConfigData configFileName:{0}", configFileName);

        // Load song old
        string text = null;
		if (Configuration.GetDataVersion() != RemoteVariables.DefaultDataVersion)
		{
			text = LoadFile(configFileName);
		}
		if (text == null || text == string.Empty)
		{
			TextAsset textAsset = Resources.Load("Sound/" + configFileName.Replace(".json", string.Empty)) as TextAsset;
			text = textAsset.text;
		}
        //Dictionary<string, Item> config = CSVReader.GetConfig(text);
        Dictionary<string, Item> config = new Dictionary<string, Item>();
        if(!string.IsNullOrEmpty(text))
        {
            var items = JsonConvert.DeserializeObject<List<Item>>(text);
            if(items != null && items.Count > 0)
            {
                foreach(var item in items)
                {
                    config[item.path] = item;
                }
            }
        }

        if (config.Count > 0)
		{
            songList = config;
            onSongListLoaded?.Invoke();
        }

//#if UNITY_EDITOR
//        List<Item> items = new List<Item>();
//        foreach(var item in songList)
//        {
//            items.Add(item.Value);
//        }

//        string @str = JsonConvert.SerializeObject(items);
//        File.WriteAllText(Path.Combine(Application.dataPath.Replace("Assets", "GameInfo"), "songconfig-old.json"), str);
//#endif
    }

    public void LoadSongLocal()
	{
		Dictionary<string, Item> config = CSVReader.GetConfig(LoadFile("songlocal.csv"));
		foreach (Item value in config.Values)
		{
			if (!value.path.Contains(".mp3"))
			{
				try
				{
					value.path = Util.Base64Decode(value.path);
					value.name = Util.Base64Decode(value.name);
				}
                catch (Exception ex)
                {
                    Util.ShowMessage(ex.Message.ToString());
                }
                value.type = SONGTYPE.OPEN.ToString();
				if (value.bmp < 50f)
				{
					value.bmp = 100f;
				}
				localSongList.Add(value.path, value);
			}
		}
	}

	public void AddSongLocal(Item song)
	{
		Dictionary<string, Item> dictionary = new Dictionary<string, Item>();
		dictionary.Add(song.path, song);
		foreach (Item value in localSongList.Values)
		{
			dictionary.Add(value.path, value);
		}
		localSongList = dictionary;
		SaveFileLocalSong();
		CoreData.instance.SetSelectedThemeBySong(song.path, isLocal: true);
	}

	private void SaveFileLocalSong()
	{
		string text = "Path,Name,BMP,StartTime,Diamonds,Type,MP3 version\n";
		int num = 0;
		foreach (Item value in localSongList.Values)
		{
			num++;
			text = text + Util.Base64Encode(value.path) + ",";
			text = text + Util.Base64Encode(value.name) + ",";
			text = text + value.bmp + ",";
			text = text + value.startTime + ",";
			text = text + value.diamonds + ",";
			text = text + value.type + ",";
			text += value.version;
			if (num < localSongList.Count)
			{
				text += "\n";
			}
		}
		SaveFile(text, "songlocal.csv");
	}

	public void RemoveSongLocal(string key)
	{
		SuperpoweredSDK.instance.RemoveMusic(key);
		localSongList.Remove(key);
		SaveFileLocalSong();
	}

	public bool IsLocalSong(string songpath)
	{
		return localSongList.ContainsKey(songpath);
	}

	public static string GetLanguageData()
	{
        Debug.LogError("TilesHop RemoteFilesData.GetLanguageData Get language file local " + languageFileName);
		string text = null;
		if (Configuration.GetLanguageVersion() != RemoteVariables.DefaultLanguageVersion)
		{
			//text = LoadFile(languageFileName);
		}
        if (text == null || text == string.Empty)
        {
            TextAsset textAsset = Resources.Load(languageFileName.Replace(".csv", string.Empty)) as TextAsset;
            text = textAsset.text;
        }
		return text;
	}

	public static string RemotePath(string fileName)
	{
		if (fileName == configFileName)
		{
			return RemoteVariables.ConfigPath;
		}
        if (fileName == languageFileName)
        {
            int length = RemoteVariables.ConfigPath.LastIndexOf('/');
            return RemoteVariables.ConfigPath.Substring(0, length) + "/" + languageFileName;
        }
		return RemoteVariables.DataPath + fileName;
	}

	public static void DeleteLocalFile(string fileName)
	{
		string path = LocalPath(fileName);
		if (File.Exists(path))
		{
			File.Delete(path);
		}
	}

	public static string LocalPath(string fileName)
	{
		return Application.persistentDataPath + "/" + fileName;
	}

	public static string StreamingURL()
	{
		string text = "file://" + Application.streamingAssetsPath + "/";
		return "jar:file://" + Application.dataPath + "!/assets/";
	}

	private IEnumerator IECopyFileStreaming(string fileName)
	{
#if UNITY_2018_3_OR_NEWER
        /*
		UnityWebRequest webRequest = UnityWebRequest.Get(StreamingURL() + fileName);
        yield return webRequest.SendWebRequest();

        if (webRequest.isNetworkError)
        {
            Debug.Log("TilesHop RemoteFilesData.IECopyFileStreaming fileName:" + fileName + " Error: " + webRequest.error);
        }
        else
        {
            Debug.Log("TilesHop RemoteFilesData.IECopyFileStreaming fileName:" + fileName + " Success!");
            SaveFileBinary(webRequest.downloadHandler.data, fileName);
        }
		*/
		currentFileStatus = FILE_DATA_STATUS.READY;
		if (songList.ContainsKey(fileName))
		{
			CoreData.SetSongVersion(fileName, songList[fileName].version);
		}
		yield return "";
#else
        /*
		WWW www = new WWW(StreamingURL() + fileName);
        yield return www;
		if (www.error == null)
		{
			SaveFileBinary(www.bytes, fileName);
		}*/
#endif
    }
}
