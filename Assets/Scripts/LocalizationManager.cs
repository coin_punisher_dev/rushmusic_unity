using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalizationManager : MonoBehaviour
{
	public delegate void LanguageChangeHandler();

	public static LocalizationManager instance;

	private Dictionary<string, string> localizedText;

	private bool isReady;

	public List<string> supportedList;

	private string defaultLanguage = "English";

	private string defaultKey = "Key";

	public static event LanguageChangeHandler OnLanguageChange = delegate {	};

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
			GetDictionary(RemoteFilesData.GetLanguageData());
			Object.DontDestroyOnLoad(base.gameObject);
		}
		else if (instance != null)
		{
			UnityEngine.Object.Destroy(base.gameObject);
		}
	}

	public string GetLocalizedValue(string key)
	{
		string result = key;
		if (localizedText != null)
		{
			result = ((!localizedText.ContainsKey(key)) ? key.Replace("_", " ") : localizedText[key]);
		}
		return result;
	}

	public bool GetIsReady()
	{
		return isReady;
	}

	public bool GetDictionary(string csvText, string langID = null)
	{
        string[,] array = CSVReader.SplitCsvGrid(csvText, useCommas: true);
        if (array.Length > 0)
        {
            StartCoroutine(AsyncGetDictionary(array, langID));
            return true;
        }
        return false;
	}

    IEnumerator AsyncGetDictionary(string[,] array, string langID)
    {
        if (string.IsNullOrEmpty(langID))
        {
            langID = Configuration.GetLanguageID();
        }
        if (string.IsNullOrEmpty(langID))
        {
            langID = ((Application.systemLanguage != SystemLanguage.Chinese) ? Application.systemLanguage.ToString() : SystemLanguage.ChineseSimplified.ToString());
        }
        string b = defaultKey;
        localizedText = new Dictionary<string, string>();
        supportedList = new List<string>();
        int num = 0;
        int num2 = 0;
        for (int i = 0; i < array.GetUpperBound(0); i++)
        {
            string text = array[i, 0].Trim();
            if (text != string.Empty)
            {
                if (text == b)
                {
                    num = i;
                }
                supportedList.Add(text);
            }
        }
        num2 = supportedList.IndexOf(langID);
        if (num2 < 0)
        {
            num2 = supportedList.IndexOf(defaultLanguage);
        }
        if (num >= 0 && num2 >= 0)
        {
            for (int j = 0; j < array.GetUpperBound(1); j++)
            {
                if (array[num, j] != string.Empty)
                {
                    localizedText.Add(array[num, j], array[num2, j].Replace("\\n", "\n"));
                }
            }
        }
        yield return new WaitForSeconds(0.25f);
        OnLanguageChange?.Invoke();
    }
}
