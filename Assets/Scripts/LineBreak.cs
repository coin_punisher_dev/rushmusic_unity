using System.Collections;
using UnityEngine;

public class LineBreak : MonoBehaviour
{
	public TextMesh text;

	private Vector3 originalPosition;

	private bool isHiding;

	private void Start()
	{
		originalPosition = base.transform.localPosition;
	}

	private void OnEnable()
	{
		isHiding = false;
	}

	private void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.name == GAMETAG.Ball && !isHiding)
		{
			StartCoroutine(Hide());
			SoundManager.instance.PlayCheckPoint();
			isHiding = true;
		}
	}

	public void SetText(string str)
	{
		text.text = str;
	}

	public void StartHide()
	{
		if (!isHiding)
		{
			StartCoroutine(Hide());
			SoundManager.instance.PlayCheckPoint();
			isHiding = true;
		}
	}

	private IEnumerator Hide(float time = 0.5f)
	{
		float t = 0f;
		while (t < time)
		{
			t += Time.deltaTime;
			base.transform.position += Vector3.back * Time.deltaTime * 20f;
			yield return null;
		}
		base.transform.localPosition = originalPosition;
		base.gameObject.SetActive(value: false);
	}
}
