using UnityEngine;
using UnityEngine.UI;

public class SettingList : MonoBehaviour
{
	public Image sound;

	public Image music;

	public Text langText;

	public Text versionText;

	private LocalizationManager.LanguageChangeHandler handler;

	public static SettingList instance;

	private void Awake()
	{
		instance = this;
	}

	private void Start()
	{
		Util.UpdateBannerContent(base.transform);
		DisplayMusicButton();
		DisplaySoundButton();
		versionText.text = "V" + Application.version;
	}

	public void SoundToggle()
	{
		SoundManager.instance.PlayGameButton();
		if (Configuration.instance.SoundIsOn())
		{
			Configuration.instance.SetSoundOff();
		}
		else
		{
			Configuration.instance.SetSoundOn();
		}
		DisplaySoundButton();
	}

	public void MusicToggle()
	{
		SoundManager.instance.PlayGameButton();
		if (Configuration.instance.MusicIsOn())
		{
			Configuration.instance.SetMusicOff();
			HomeManager.instance.groundMusic.StopMusic();
		}
		else
		{
			Configuration.instance.SetMusicOn();
			HomeManager.instance.groundMusic.PlayMusic();
		}
		DisplayMusicButton();
	}

	public void DisplayMusicButton()
	{
		if (Configuration.instance.MusicIsOn())
		{
			music.enabled = true;
			music.transform.Find("Off").gameObject.SetActive(value: false);
			music.transform.Find("Text").GetComponent<Text>().text = LocalizationManager.instance.GetLocalizedValue("ON");
		}
		else
		{
			music.enabled = false;
			music.transform.Find("Off").gameObject.SetActive(value: true);
			music.transform.Find("Text").GetComponent<Text>().text = LocalizationManager.instance.GetLocalizedValue("OFF");
		}
	}

	public void DisplaySoundButton()
	{
		if (Configuration.instance.SoundIsOn())
		{
			sound.enabled = true;
			sound.transform.Find("Off").gameObject.SetActive(value: false);
			sound.transform.Find("Text").GetComponent<Text>().text = LocalizationManager.instance.GetLocalizedValue("ON");
		}
		else
		{
			sound.enabled = false;
			sound.transform.Find("Off").gameObject.SetActive(value: true);
			sound.transform.Find("Text").GetComponent<Text>().text = LocalizationManager.instance.GetLocalizedValue("OFF");
		}
	}

	public void RateUs()
	{
		Util.RateUs();
	}

	public void Close()
	{
		base.gameObject.SetActive(value: false);
		SoundManager.instance.PlayGameButton();
		HomeManager.instance.ShowHome();
	}

	public void ShowAdsPurchase()
	{
		if (AdsPurchase.instance == null)
		{
			AnalyticHelper.ButtonClick(FIRE_EVENT.Button_Remove_Ads);
			if (!Configuration.instance.isNoAds)
			{
				SoundManager.instance.PlayGameButton();
				Util.ShowPopUp("AdsPurchase");
			}
		}
	}

	public void ShowLanguageSelection()
	{
		SoundManager.instance.PlayGameButton();
		Util.ShowPopUp("LanguageSelection");
	}

	public void ShowCredit()
	{
		SoundManager.instance.PlayGameButton();
		Util.ShowPopUp("Credit");
	}

	private void LanguageHasChanged()
	{
		langText.text = Configuration.GetLanguageID();
	}

    public void OnEnable()
	{
        LocalizationManager.OnLanguageChange += LanguageHasChanged;
        LanguageHasChanged();
    }

    private void OnDisable()
    {
		LocalizationManager.OnLanguageChange -= LanguageHasChanged;
    }
}
