using System.Collections;
using UnityEngine;

public class GroundMusic : MonoBehaviour
{
    private static string songID = string.Empty;

    private static float time;

    private bool isLoading;

    private Coroutine coroutine;

    public AudioSource audioSource;

    private bool waitingStop;

    private IEnumerator LoadMp3Data()
    {
        if (!RemoteFilesData.instance.IsLocalSong(songID))
        {
            if (!RemoteFilesData.instance.IsNewData(songID))
            {
                isLoading = true;
                while (RemoteFilesData.instance.currentFileStatus != FILE_DATA_STATUS.READY)
                {
                    yield return null;
                }
                SuperpoweredSDK.instance.LoadMusic(RemoteFilesData.LocalPath(songID), isPlayAfterLoad: false, isLoop: true);
                while (!SuperpoweredSDK.instance.IsReady())
                {
                    yield return null;
                }
                SuperpoweredSDK.instance.SetVolume(0.4f);
                SuperpoweredSDK.instance.SetPosition(time);
                SuperpoweredSDK.instance.SetSpeed(1f);
                SuperpoweredSDK.instance.SmoothPlay(0.5f);
                isLoading = false;
            }
        }
        else
        {
            isLoading = true;
            SuperpoweredSDK.instance.LoadMusic(songID, isPlayAfterLoad: false, isLoop: true);
            while (!SuperpoweredSDK.instance.IsReady())
            {
                yield return null;
            }
            SuperpoweredSDK.instance.SetVolume(0.4f);
            SuperpoweredSDK.instance.SetPosition(time);
            SuperpoweredSDK.instance.SetSpeed(1f);
            SuperpoweredSDK.instance.SmoothPlay(0.5f);
            isLoading = false;
        }
    }

    public void PlayMusic()
    {
        if (Configuration.instance.MusicIsOn() && !isLoading)
        {
            Item currentSong = CoreData.instance.GetCurrentSong();
            if (songID != currentSong.path)
            {
                time = 0f;
                songID = currentSong.path;
            }
            StartCoroutine(LoadMp3Data());
        }
    }

    public void ResumeMusic()
    {
        SuperpoweredSDK.instance.SetVolume(0.4f);
        SuperpoweredSDK.instance.SetPosition(time);
        SuperpoweredSDK.instance.SetSpeed(1f);
        SuperpoweredSDK.instance.SmoothPlay(0.5f);
    }

    public void StopMusic(bool replayGround = false)
    {
        if (coroutine != null)
        {
            StopCoroutine(coroutine);
        }
        if (SuperpoweredSDK.instance.IsPlaying())
        {
            time = SuperpoweredSDK.instance.GetPosition();
            SuperpoweredSDK.instance.SmoothStop(0.3f);
        }
        if (audioSource.isPlaying)
        {
            StartCoroutine(SmoothPauseAudioSource());
        }
        if (replayGround)
        {
            ResumeMusic();
        }
    }

    public void PlayMusicOnline(string songName)
    {
        AnalyticHelper.PreviewSong_Click(songName);
        StopMusic();
        if (coroutine != null)
        {
            StopCoroutine(coroutine);
        }
        coroutine = StartCoroutine(ReadyToPlay(RemoteFilesData.RemotePath("Preview/" + songName)));
    }

    private IEnumerator ReadyToPlay(string url)
    {
        if (SongItem.loadingImg != null)
        {
            SongItem.loadingImg.SetActive(value: true);
        }
        WWW www = new WWW(url);
        yield return www;
        if (www.error == null)
        {
            AudioClip audioClip = www.GetAudioClip(threeD: false, stream: false);
            if (audioClip.loadState == AudioDataLoadState.Loaded)
            {
                audioSource.clip = audioClip;
                if (SongItem.loadingImg != null)
                {
                    SongItem.loadingImg.SetActive(value: false);
                }
                coroutine = StartCoroutine(SmoothPlayAudioSource());
            }
            else if (SongItem.loadingImg != null)
            {
                SongItem.loadingImg.SetActive(value: false);
            }
        }
        else if (SongItem.loadingImg != null)
        {
            SongItem.loadingImg.SetActive(value: false);
        }
    }

    private IEnumerator SmoothPlayAudioSource(float timer = 1f)
    {
        while (waitingStop)
        {
            yield return null;
        }
        if (SongItem.loadingImg != null)
        {
            SongItem.loadingImg.SetActive(value: false);
        }
        audioSource.Play();
        float orignial = 0f;
        float destination = 1f;
        float t = 0f;
        float delta = Time.deltaTime / timer;
        while (t < 1f)
        {
            t += delta;
            audioSource.volume = Mathf.Lerp(orignial, destination, t);
            yield return null;
        }
        yield return new WaitForSeconds(audioSource.clip.length - timer * 2f);
        StartCoroutine(SmoothPauseAudioSource());
        yield return new WaitForSeconds(timer * 1.1f);
        coroutine = StartCoroutine(SmoothPlayAudioSource());
    }

    private IEnumerator SmoothPauseAudioSource(float timer = 1f)
    {
        if (!waitingStop)
        {
            waitingStop = true;
            float orignial = audioSource.volume;
            float destination = 0f;
            float t = 0f;
            float delta = Time.deltaTime / timer;
            while (t < 1f)
            {
                t += delta;
                audioSource.volume = Mathf.Lerp(orignial, destination, t);
                yield return null;
            }
            audioSource.Stop();
            waitingStop = false;
        }
    }
}