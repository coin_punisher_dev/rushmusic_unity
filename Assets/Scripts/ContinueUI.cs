using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ContinueUI : MonoBehaviour
{
	public Button continueButton;

	public Button continueButtonDiamond;

	public Button cancelButton;

	public Text countDownText;

	public IEnumerator ie;

	public Text continueText;

	public Text priceText;

	public GameObject[] countDownParts;

	private int continuePrice = 20;

	private void Start()
	{
		switch (Configuration.GetABTestID(3, AdIronSource.eventName))
		{
		case 1:
			continuePrice = 20;
			break;
		case 2:
			continuePrice = 30;
			break;
		case 3:
			continuePrice = 50;
			break;
		}
		priceText.text = continuePrice.ToString();
		if (cancelButton != null)
		{
			cancelButton.onClick.AddListener(Cancel);
		}
		continueButton.onClick.AddListener(delegate
		{
			AdsManager.instance.ShowRewardAds(VIDEOREWARD.CONTINUE);
			AnalyticHelper.FireEvent(FIRE_EVENT.Continue, new Dictionary<string, object>
			{
				{
					"Type",
					"Video"
				}
			});
			if (ie != null)
			{
				StopCoroutine(ie);
			}
		});
		continueButtonDiamond.onClick.AddListener(delegate
		{
			GameController.controller.GameContinue(SONG_PLAY_COST.Diamonds);
			TopBar.instance.UpdateDiamond(-continuePrice);
			AnalyticHelper.FireEvent(FIRE_EVENT.Continue, new Dictionary<string, object>
			{
				{
					"Type",
					"Diamond"
				}
			});
			UIController.ui.continueui.SetActive(value: false);
			if (ie != null)
			{
				StopCoroutine(ie);
			}
		});
	}

	private void OnEnable()
	{
		Configuration.instance.SetCurrentLocation(LOCATION_NAME.CONTINUE_GAME);
		if (CoreData.instance.GetDiamonds() >= continuePrice)
		{
			continueButtonDiamond.gameObject.SetActive(value: true);
		}
		else
		{
			continueButtonDiamond.gameObject.SetActive(value: false);
		}
		if (ie != null)
		{
			StopCoroutine(ie);
		}
		ie = CountDown(3f);
		StartCoroutine(ie);
		int bestScore = CoreData.GetBestScore(CoreData.instance.GetCurrentSong().path);
		if (bestScore > GameController.controller.score)
		{
			int num = bestScore - GameController.controller.score;
			continueText.text = LocalizationManager.instance.GetLocalizedValue("NEED_MORE_SCORE").Replace("{0}", "<size=42><color='#00E031'> " + num + " </color></size>");
		}
		else
		{
			continueText.text = LocalizationManager.instance.GetLocalizedValue("CONTINUE_TEXT");
		}
	}

	private IEnumerator CountDown(float time)
	{
		do
		{
			countDownText.text = time.ToString();
			time -= 1f;
			yield return new WaitForSeconds(1f);
			if (Mathf.Approximately(time, 0f))
			{
				countDownText.text = "0";
				yield return null;
				Cancel();
			}
		}
		while (time >= 0f);
	}

	public void Cancel()
	{
		if (ie != null)
		{
			StopCoroutine(ie);
		}
		UIController.ui.gameover.SetActive(value: true);
		base.gameObject.SetActive(value: false);
		if (Configuration.isAdAvailable())
		{
			Configuration.SetTutorialAdOn();
			AdsManager.instance.RequestInterstitialVideo();
		}
	}
}
