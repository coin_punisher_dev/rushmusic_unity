using UnityEngine;

public class Follow : MonoBehaviour
{
	public GameObject aim;

	public GameObject overlay;

	private Vector3 position;

	private float y;

	private float deltaZ;

	private Transform transformCache;

	private float zOverlay;

	private void Awake()
	{
		transformCache = base.transform;
		Vector3 vector = transformCache.position;
		y = vector.y;
		if (overlay != null)
		{
			Vector3 vector2 = overlay.transform.position;
			float z = vector2.z;
			Vector3 vector3 = transformCache.position;
			zOverlay = z - vector3.z;
		}
	}


    private void LateUpdate()
    {
        if (GameController.controller.game == GameStatus.LIVE)
        {
            Vector3 a = aim.transform.position;
            if (Mathf.Approximately(Time.timeScale, 0f))
            {
                a -= 4f * Vector3.forward;
            }
            position = transformCache.position;
            float x = a.x;
            Vector3 vector = transformCache.position;
            float num = (x - vector.x) * RemoteVariables.CammeraFollowSpeed;
            num /= 2.5f;
            position.x += num;
            position.y = y;
            float z = a.z;
            Vector3 vector2 = transformCache.position;
            deltaZ = z - vector2.z;
            position.z += Spawner.Intance.bmp / 2500f * deltaZ * 60f / (float)Application.targetFrameRate;
            transformCache.position = position;
        }
        if (overlay != null)
        {
            Transform transform = overlay.transform;
            Vector3 vector3 = overlay.transform.position;
            Vector3 vector4 = transformCache.position;
            transform.position = Util.SetPositionZ(vector3, vector4.z + zOverlay);
        }
    }

    //private void Update()
    //{
    //	if (GameController.controller.game == GameStatus.LIVE)
    //	{
    //		Vector3 a = aim.transform.position;
    //		if (Mathf.Approximately(Time.timeScale, 0f))
    //		{
    //			a -= 4f * Vector3.forward;
    //		}
    //		position = transformCache.position;
    //		float x = a.x;
    //		Vector3 vector = transformCache.position;
    //		float num = (x - vector.x) * RemoteVariables.CammeraFollowSpeed;
    //		num /= 2.5f;
    //		position.x += num;
    //		position.y = y;
    //		float z = a.z;
    //		Vector3 vector2 = transformCache.position;
    //		deltaZ = z - vector2.z;
    //		position.z += Spawner.Intance.bmp / 2500f * deltaZ * 60f / (float)Application.targetFrameRate;
    //		transformCache.position = position;
    //	}
    //	if (overlay != null)
    //	{
    //		Transform transform = overlay.transform;
    //		Vector3 vector3 = overlay.transform.position;
    //		Vector3 vector4 = transformCache.position;
    //		transform.position = Util.SetPositionZ(vector3, vector4.z + zOverlay);
    //	}
    //}
}
