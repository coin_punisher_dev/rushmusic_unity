using UnityEngine;
using UnityEngine.UI;

public class LanguageSelection : PopupUI
{
	public static LanguageSelection instance;

	public Text langText;

	public GameObject langContainer;

	public GameObject langActive;

	private int selectedId;

	private void Start()
	{
		instance = this;
		string languageID = Configuration.GetLanguageID();
		Configuration.instance.SetCurrentLocation(LOCATION_NAME.LANGUAGE_SELECTION, isPopup: true);
		for (int i = 1; i < LocalizationManager.instance.supportedList.Count; i++)
		{
			Transform tfLanguage = langContainer.transform.Find(LocalizationManager.instance.supportedList[i]);
			if (tfLanguage != null)
			{
				if (!(LocalizationManager.instance.supportedList[i] != languageID))
				{
					ActiveItem(tfLanguage.gameObject);
					selectedId = i;
					langText.text = LocalizationManager.instance.supportedList[i];
				}
				int id = i;
				tfLanguage.GetComponent<Button>().onClick.AddListener(delegate
				{
					LanguageClick(id);
				});
			}
		}
	}

	private void LanguageClick(int i)
	{
		SoundManager.instance.PlayGameButton();
		if (selectedId != i)
		{
			langText.text = LocalizationManager.instance.supportedList[i];
			string text = LocalizationManager.instance.supportedList[i];
			Configuration.SetLanguageID(text);
			LocalizationManager.instance.GetDictionary(RemoteFilesData.GetLanguageData(), text);
			Transform tfLanguageDeactive = langContainer.transform.Find(LocalizationManager.instance.supportedList[selectedId]);
			if (tfLanguageDeactive != null)
			{
				DeactiveItem(tfLanguageDeactive.gameObject);
			}
			Transform tfLanguageActive = langContainer.transform.Find(LocalizationManager.instance.supportedList[i]);
			if (tfLanguageActive != null)
			{
				ActiveItem(tfLanguageActive.gameObject);
			}
			selectedId = i;
			AnalyticHelper.ButtonClick(FIRE_EVENT.Change_Language);
		}
	}

	private void ActiveItem(GameObject item)
	{
		item.transform.Find("Deactive").gameObject.SetActive(value: false);
		langActive.transform.SetParent(item.transform, worldPositionStays: false);
	}

	private void DeactiveItem(GameObject item)
	{
		item.transform.Find("Deactive").gameObject.SetActive(value: true);
	}
}
