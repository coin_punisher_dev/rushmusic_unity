using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FreeVideo : PopupUI
{
	public Sprite ballSprite;

	public Sprite diamondSprite;

	public Sprite themeSprite;

	public Sprite musicSprite;

	public GameObject button;

	private List<GameObject> buttonList;

	public static FreeVideo instance;

	private float positionY;

	public int currentIndex = -1;

	public RectTransform container;

	public Text diamondBonusText;

	private void Awake()
	{
		instance = this;
	}

	private void Start()
	{
		Util.UpdateBannerContent(base.transform);
		buttonList = new List<GameObject>();
		BuildList();
		container.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, positionY + 240f);
		Configuration.instance.SetCurrentLocation(LOCATION_NAME.FREE_GIFT, isPopup: true);
	}

	public void BuildList()
	{
		positionY = 0f;
		AddButton(FREEVIDEO_TYPE.DIAMOND, diamondSprite, null, 0);
		foreach (int videoItem in BallList.videoItems)
		{
			if (!CoreData.IsOpenBall(videoItem))
			{
				AddButton(FREEVIDEO_TYPE.BALL, ballSprite, null, videoItem);
			}
		}
		foreach (Item value in RemoteFilesData.instance.songList.Values)
		{
			if (CoreData.GetSongType(value.path, value.type) == SONGTYPE.VIDEO)
			{
				AddButton(FREEVIDEO_TYPE.SONG, musicSprite, value.path, 0);
			}
		}
	}

	private void AddButton(FREEVIDEO_TYPE type, Sprite bg, string idString, int idInt)
	{
		GameObject gameObject = Object.Instantiate(button);
		gameObject.SetActive(value: true);
		gameObject.GetComponent<FreeVideoItem>().SetValues(type, bg, idString, idInt, buttonList.Count);
		gameObject.transform.SetParent(button.transform.parent, worldPositionStays: false);
		StartCoroutine(DelayUpdate(gameObject, Vector3.down * positionY));
		buttonList.Add(gameObject);
		positionY += 100f;
	}

	private IEnumerator DelayUpdate(GameObject item, Vector3 localPosition)
	{
		yield return null;
		item.transform.localPosition += localPosition;
	}

	public static void ShowPopUp()
	{
		if (instance == null)
		{
			SoundManager.instance.PlayGameButton();
			Util.ShowPopUp("FreeVideo");
		}
	}

	public void CompletedVideoAds()
	{
		if (buttonList.Count > currentIndex && buttonList[currentIndex] != null)
		{
			buttonList[currentIndex].GetComponent<FreeVideoItem>().Completed();
		}
	}

	public new void Close()
	{
		Configuration.instance.showFreeGifts = Configuration.FreeGifts.CLOSE;
		base.Close();
	}

	public void IncreaseDiamondsVideoBonus()
	{
		if (Configuration.instance.diamondsVideoBonus == 0)
		{
			Configuration.instance.diamondsVideoBonus = RemoteVariables.DiamondsVideoBonus;
		}
		int num = Configuration.instance.diamondsVideoBonus + 5;
		if (num > 40)
		{
			num = RemoteVariables.DiamondsVideoBonus;
		}
		StartCoroutine(UpdateText(Configuration.instance.diamondsVideoBonus, num));
		Configuration.instance.diamondsVideoBonus = num;
	}

	private IEnumerator UpdateText(int from, int to)
	{
		float t = 0f;
		float time = 1f;
		string text = LocalizationManager.instance.GetLocalizedValue("X_DIAMONDS");
		while (t < time)
		{
			t += Time.deltaTime;
			diamondBonusText.text = text.Replace("10", ((int)Mathf.Lerp(from, to, t / time)).ToString());
			yield return null;
		}
	}
}
