//@TODO IRON_SOURCE
//#define ENABLE_IRON_SOURCE

using UnityEngine;

public class AdIronSource : AdNetwork
{
	public string androidAppKey;

	public string iosAppKey;

	private bool _listenerAttached;

	public static string eventName = "Reborn_Diamond";

	public override void init()
	{
#if ENABLE_IRON_SOURCE
        if (!_listenerAttached)
		{
            //@TODO
			//IronSource.Agent.reportAppStarted();
			IronSourceSegment ironSourceSegment = new IronSourceSegment();
			switch (Configuration.GetABTestID(3, eventName))
			{
			case 1:
				ironSourceSegment.segmentName = eventName + "_A";
				break;
			case 2:
				ironSourceSegment.segmentName = eventName + "_B";
				break;
			case 3:
				ironSourceSegment.segmentName = eventName + "_C";
				break;
			}
			IronSource.Agent.setSegment(ironSourceSegment);
			IronSourceConfig.Instance.setClientSideCallbacks(status: true);
			IronSource.Agent.validateIntegration();
			IronSource.Agent.setUserId(SystemInfo.deviceUniqueIdentifier);
			IronSource.Agent.init(androidAppKey, IronSourceAdUnits.REWARDED_VIDEO, IronSourceAdUnits.INTERSTITIAL, IronSourceAdUnits.BANNER);
			IronSourceEvents.onInterstitialAdReadyEvent += InterstitialAdReadyEvent;
			IronSourceEvents.onInterstitialAdLoadFailedEvent += InterstitialAdLoadFailedEvent;
			IronSourceEvents.onInterstitialAdClosedEvent += InterstitialAdClosedEvent;
			IronSourceEvents.onInterstitialAdClickedEvent += InterstitialAdClickedEvent;
			IronSourceEvents.onRewardedVideoAdClosedEvent += RewardedVideoAdClosedEvent;
			IronSourceEvents.onRewardedVideoAdRewardedEvent += RewardedVideoAdRewardedEvent;
			IronSourceEvents.onRewardedVideoAdShowFailedEvent += RewardedVideoAdShowFailedEvent;
			IronSourceEvents.onRewardedVideoAdClickedEvent += RewardedVideoAdClickedEvent;
			IronSourceEvents.onRewardedVideoAdEndedEvent += RewardedVideoAdEndedEvent;
			IronSourceEvents.onBannerAdLoadedEvent += BannerAdLoadedEvent;
			IronSourceEvents.onBannerAdLoadFailedEvent += BannerAdLoadFailedEvent;
			IronSourceEvents.onBannerAdClickedEvent += BannerAdClickedEvent;
			IronSourceEvents.onBannerAdScreenPresentedEvent += BannerAdScreenPresentedEvent;
			IronSourceEvents.onBannerAdScreenDismissedEvent += BannerAdScreenDismissedEvent;
			IronSourceEvents.onBannerAdLeftApplicationEvent += BannerAdLeftApplicationEvent;
			_listenerAttached = true;
		}
#endif
	}

	public override bool isAdVideoAvailable()
	{
#if ENABLE_IRON_SOURCE
		return IronSource.Agent.isRewardedVideoAvailable();
#endif
        return false;
	}

	public override void showAdVideo()
	{
#if ENABLE_IRON_SOURCE
		IronSource.Agent.showRewardedVideo();
#endif
	}

	public override void cacheAdVideo()
	{
	}

	public override bool isInterstitialVideoAvailable()
	{
#if ENABLE_IRON_SOURCE
		return IronSource.Agent.isInterstitialReady();
#endif
        return false;
	}

	public override void cacheInterstitialVideo()
	{
#if ENABLE_IRON_SOURCE
		IronSource.Agent.loadInterstitial();
#endif
	}

	public override void showInterstitialVideo()
	{
#if ENABLE_IRON_SOURCE
		IronSource.Agent.showInterstitial();
#endif
	}

	public override void loadBanner()
	{
#if ENABLE_IRON_SOURCE
		IronSource.Agent.loadBanner(IronSourceBannerSize.BANNER, IronSourceBannerPosition.BOTTOM);
		IronSource.Agent.hideBanner();
#endif
	}

	public override void showBanner()
	{
#if ENABLE_IRON_SOURCE
		IronSource.Agent.displayBanner();
		bannerStatus = BANNER_STATUS.SHOWED;
		AnalyticHelper.Bad(Click_Impression.impression);
#endif
	}

	public override void hideBanner()
	{
#if ENABLE_IRON_SOURCE
		IronSource.Agent.hideBanner();
		bannerStatus = BANNER_STATUS.HIDE;
#endif
	}

	private void InterstitialAdReadyEvent()
	{
		AdsManager.instance.InterstitialAdReadyEvent();
	}

	private void InterstitialAdClickedEvent()
	{
		AdsManager.instance.InterstitialAdClickedEvent();
	}
#if ENABLE_IRON_SOURCE
    private void InterstitialAdLoadFailedEvent(IronSourceError error)
	{
		AdsManager.instance.InterstitialAdLoadFailedEvent(error.getDescription());
	}
#endif

	private void InterstitialAdClosedEvent()
	{
		AdsManager.instance.InterstitialAdClosedEvent();
	}

#if ENABLE_IRON_SOURCE
    private void RewardedVideoAdRewardedEvent(IronSourcePlacement ssp)
	{
		AdsManager.instance.RewardedVideoAdRewardedEvent();
	}
#endif

	private void RewardedVideoAdClosedEvent()
	{
		AdsManager.instance.RewardedVideoAdClosedEvent();
	}

	private void RewardedVideoAdEndedEvent()
	{
		AdsManager.instance.RewardedVideoAdEndedEvent();
	}

#if ENABLE_IRON_SOURCE
	private void RewardedVideoAdShowFailedEvent(IronSourceError error)
	{
		AdsManager.instance.RewardedVideoAdShowFailedEvent(error.getDescription());
	}
#endif

#if ENABLE_IRON_SOURCE
	private void RewardedVideoAdClickedEvent(IronSourcePlacement ssp)
	{
		AdsManager.instance.RewardedVideoClickedEvent();
	}
#endif

#if ENABLE_IRON_SOURCE
	public void OnApplicationPause(bool pauseStatus)
	{
		IronSource.Agent.onApplicationPause(pauseStatus);
	}
#endif

	private void BannerAdLoadedEvent()
	{
		bannerStatus = BANNER_STATUS.LOADED;
	}

#if ENABLE_IRON_SOURCE
	private void BannerAdLoadFailedEvent(IronSourceError error)
	{
		bannerStatus = BANNER_STATUS.FAILED;
	}
#endif

	private void BannerAdClickedEvent()
	{
		AnalyticHelper.Bad(Click_Impression.click);
	}

	private void BannerAdScreenPresentedEvent()
	{
	}

	private void BannerAdScreenDismissedEvent()
	{
	}

	private void BannerAdLeftApplicationEvent()
	{
	}
}
