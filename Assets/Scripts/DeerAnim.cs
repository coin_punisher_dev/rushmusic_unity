using UnityEngine;
using UnityEngine.UI;

public class DeerAnim : MonoBehaviour
{
	public Animator animator;

	public RectTransform rect;

	private int[] positions;

	private int nextPositions;

	private float eatTime;

	public Image sr;

	private void Start()
	{
		positions = new int[5]
		{
			-50,
			70,
			292,
			426,
			536
		};
		Walk(UnityEngine.Random.Range(1, positions.Length - 2));
	}

	private void Update()
	{
		Vector2 anchoredPosition = rect.anchoredPosition;
		if (anchoredPosition.x > (float)positions[positions.Length - 1])
		{
			Vector2 anchoredPosition2 = rect.anchoredPosition;
			anchoredPosition2.x = positions[0];
			rect.anchoredPosition = anchoredPosition2;
			Walk(UnityEngine.Random.Range(1, positions.Length - 2));
			return;
		}
		Vector2 anchoredPosition3 = rect.anchoredPosition;
		if (anchoredPosition3.x < (float)positions[nextPositions])
		{
			rect.anchoredPosition += Vector2.right * Time.deltaTime * 40f;
		}
		else if (eatTime == 0f)
		{
			eatTime = Time.time + (float)UnityEngine.Random.Range(6, 12);
			animator.Play("Eat");
		}
		else if (eatTime < Time.time)
		{
			eatTime = 0f;
			Walk(positions.Length - 1);
			animator.StopPlayback();
		}
	}

	public void Walk(int i)
	{
		nextPositions = i;
		animator.Play("Walk");
	}
}
