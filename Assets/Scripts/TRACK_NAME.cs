using System.Runtime.InteropServices;

[StructLayout(LayoutKind.Sequential, Size = 1)]
public struct TRACK_NAME
{
	public const string rad = "rad_";

	public const string fad = "fad_";

	public const string bad = "bad_";

	public const string iap = "iap_";

	public const string song_start = "song_start";

	public const string song_fail = "song_fail";

	public const string song_end = "song_end";

	public const string share = "share";

	public const string item_name = "item_name";

	public const string item_buy = "item_buy";

	public const string item_type = "item_type";

	public const string item_price = "item_price";

	public const string item_value = "item_value";

	public const string item_id = "item_id";

	public const string item_source = "item_source";

	public const string item_usage = "item_usage";

	public const string currency_income = "currency_income";

	public const string session = "session";

	public const string abtesting = "abtesting";

	public const string location = "location";

	public const string reward = "reward";

	public const string pack_id = "pack_id";

	public const string pack_name = "pack_name";

	public const string price = "price";

	public const string song_name = "song_name";

	public const string song_id = "song_id";

	public const string song_type = "song_type";

	public const string song_unlock = "song_unlock";

	public const string song_time = "song_time";

	public const string local_song = "local_song";

	public const string name = "name";

	public const string sample = "sample";

	public const string start_time = "start_time";

	public const string end_time = "end_time";

	public const string duration = "duration";

	public const string play = "play";

	public const string country = "country";

	public const string revival_cost = "revival_cost";

	public const string revival_times = "revival_times";

	public const string retry_cost = "retry_cost";

	public const string retry_times = "retry_times";

	public const string loading = "loading";

	public const string main_menu = "main_menu";

	public const string tutorial = "tutorial";

	public const string tutorial_start = "tutorial_start";

	public const string tutorial_finish = "tutorial_finish";

	public const string total_time = "total_time";

	public const string score = "score";

	public const string stars = "stars";

	public const string screen = "screen";
}
