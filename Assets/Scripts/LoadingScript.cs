using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadingScript : MonoBehaviour
{
	private bool moved;

	public float loadingTime;

	private float startWaitTime;

	private AsyncOperation async;

	private void Start()
	{
		Configuration.instance.SetCurrentLocation(LOCATION_NAME.LOADING_GAME);
		startWaitTime = Time.time;
		if (!Configuration.IntroIsOn())
		{
			async = SceneManager.LoadSceneAsync("Home");
			async.allowSceneActivation = false;
		}
		else
		{
			Configuration.instance.showFacebookLogin = false;
			Configuration.instance.isTutorial = true;
		}
		AnalyticHelper.FireString("loading");
	}

	private void Update()
	{
		if (Configuration.instance.gameloaded)
		{
			return;
		}
		if (Time.time - startWaitTime > loadingTime && RemoteFilesData.instance.IsReady() && !moved)
		{
			Configuration.instance.gameloaded = true;
			AnalyticHelper.ABTest();
			AdsManager.instance.LoadBanner();
			if (Configuration.IntroIsOn())
			{
				AnalyticHelper.FireEvent(FIRE_EVENT.First_Open_Game);
				AppsFlyerInit.TrackEvent(AppsFlyerInit.KEY.firstopen);
				moved = true;
				string path = CoreData.instance.GetCurrentSong().path;
				CoreData.instance.SetSelectedThemeBySong(path);
				Util.GoToGamePlay(path);
				AnalyticHelper.FireString("StartTheme_" + CoreData.GetSelectedTheme(path).ToString());
				Configuration.SetIntroOff();
				Configuration.ScheduleNotification();
			}
			else
			{
				ShowHome();
			}
		}
		else if (Time.time - startWaitTime > 5f)
		{
			RemoteFilesData.instance.LoadConfigData();
			startWaitTime = Time.time;
		}
	}

	private void ShowHome()
	{
		SceneFader.instance.allowFadeIn = false;
		async.allowSceneActivation = true;
	}
}
