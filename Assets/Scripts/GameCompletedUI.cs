//using com.F4A.MobileThird;
using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.Advertisements;

public class GameCompletedUI : MonoBehaviour
{
    public Button menuButton;

    public Text scoreText;

    public Text bestScoreText;

    public Text diamondText;

    public Text rankText;

    public Text songName;

    public Button replayButton;

    public Button shareButton;

    public GameObject x2Diamond;

    public GameObject unlockSong;

    public GameObject unlockSongPlay;

    public GameObject unlockVideo;

    public GameObject unlockPurchase;

    public Text unlockSongName;

    public Text unlockSongAuthor;

    public Text unlockSongPrice;

    public Text unlockSongDesc;

    public GameObject star1;

    public GameObject star2;

    public GameObject star3;

    public GameObject starExplo;

    public GameObject shareScene;

    private Item nextSong;

    private int score;

    private int highestScore;

    private float scoreUpdate;

    private float diamondUpdate;

    private bool effect = true;

    private int retry;

    private int stars;

    private int oldStars;
	
	void Start(){
		GlobalVar.bannerView.Destroy();
	}

    private void OnEnable()
    {
        if (!effect)
        {
            return;
        }
        Configuration.instance.SetCurrentLocation(LOCATION_NAME.RESULT_GAME);
        TopBar.instance.ToggleScore(enable: false);
        SoundManager.instance.PlayGameCompleted();
        Item currentSong = CoreData.instance.GetCurrentSong();
        score = GameController.controller.score;
        oldStars = CoreData.GetBestStars(currentSong.path);
        if (GameController.controller.diamond > 0)
        {
            TopBar.instance.UpdateDiamond(GameController.controller.diamond, Income_Type.COLLECT_DIAMOND);
        }
        highestScore = CoreData.GetBestScore(currentSong.path);
        scoreUpdate = 0f;
        diamondUpdate = 0f;
        stars = GameController.controller.GetStars();
        if (highestScore < score)
        {
            CoreUser.instance.ReportScore(score, currentSong.path);
            CoreData.SetBestStars(stars, currentSong.path);
            highestScore = score;
            AchievementObj.CheckTypes(new AchievementType[4]
            {
                AchievementType.SCORE_100,
                AchievementType.SCORE_250,
                AchievementType.SCORE_500,
                AchievementType.SCORE_1000
            });
            AchievementObj.CheckTypes(new AchievementType[3]
            {
                AchievementType.STARS_1,
                AchievementType.STARS_10,
                AchievementType.STARS_ALL
            });
        }
        AnalyticHelper.Song(SONG_STATUS.song_result, currentSong);
        if (Ball.b.ph.endlessMode > 0)
        {
            AnalyticHelper.Song(SONG_STATUS.song_end, currentSong);
        }
        else
        {
            AnalyticHelper.Song(SONG_STATUS.song_fail, currentSong, Spawner.Intance.savedMusicTime);
        }
        songName.text = currentSong.name;
        unlockSongDesc.text = LocalizationManager.instance.GetLocalizedValue("NEW_SONG_CAN_UNLOCK");
        int diamonds = CoreData.instance.GetDiamonds();
        foreach (Item value in RemoteFilesData.instance.songList.Values)
        {
            SONGTYPE songType = CoreData.GetSongType(value.path, value.type);
            if ((songType == SONGTYPE.LOCK && value.diamonds <= diamonds) || songType == SONGTYPE.VIDEO)
            {
                nextSong = value;
                break;
            }
        }
        if (nextSong == null)
        {
            string key = null;
            int num = 4;
            foreach (string key2 in RemoteFilesData.instance.songList.Keys)
            {
                if (key2 != currentSong.path && CoreData.GetBestStars(key2) < num)
                {
                    num = CoreData.GetBestStars(key2);
                    key = key2;
                }
            }
            nextSong = RemoteFilesData.instance.songList[key];
        }
        if (nextSong != null)
        {
            unlockSong.SetActive(value: true);
            unlockSongName.text = nextSong.name;
            switch (CoreData.GetSongType(nextSong.path, nextSong.type))
            {
                case SONGTYPE.VIDEO:
                    unlockSongPlay.SetActive(value: false);
                    unlockSongPrice.transform.parent.gameObject.SetActive(value: false);
                    unlockVideo.SetActive(value: true);
                    unlockPurchase.SetActive(value: false);
                    break;
                case SONGTYPE.LOCK:
                    unlockSongPrice.text = nextSong.diamonds.ToString();
                    unlockSongPlay.SetActive(value: false);
                    unlockSongPrice.transform.parent.gameObject.SetActive(value: true);
                    unlockVideo.SetActive(value: false);
                    break;
                default:
                    unlockSongPlay.SetActive(value: true);
                    unlockSongPrice.transform.parent.gameObject.SetActive(value: false);
                    unlockVideo.SetActive(value: false);
                    unlockSongDesc.text = LocalizationManager.instance.GetLocalizedValue("SONG_NEED_IMPROVE");
                    break;
            }
        }
        else
        {
            unlockSong.SetActive(value: false);
        }
        StartCoroutine(MakeStarEffect());
        x2Diamond.SetActive(value: true);
        if ((double)((float)Screen.height / (float)Screen.width) <= 1.5)
        {
            unlockSong.gameObject.SetActive(value: false);
        }
        if (GameController.controller.diamond == 0)
        {
            diamondText.gameObject.SetActive(value: false);
        }
        else
        {
            diamondText.gameObject.SetActive(value: true);
        }
        long num2 = 0L;
        if (CoreUser.instance.user != null && CoreUser.instance.loadedFriend)
        {
            num2 = CoreUser.instance.user.GetMyRank(Util.SongToBoardId(currentSong.path));
        }
        rankText.text = ((num2 <= 0) ? "--" : num2.ToString());
        if (GameController.controller.replayCount > 0)
        {
            PlayerPrefs.SetInt("revive", PlayerPrefs.GetInt("revive", 0) + GameController.controller.replayCount);
            AchievementObj.CheckTypes(new AchievementType[2]
            {
                AchievementType.REVIVE_20,
                AchievementType.REVIVE_50
            });
        }
        else if (retry > 0)
        {
            PlayerPrefs.SetInt("retry", PlayerPrefs.GetInt("retry", 0) + 1);
            AchievementObj.CheckTypes(new AchievementType[2]
            {
                AchievementType.RETRY_50,
                AchievementType.RETRY_100
            });
        }
        GameController.controller.replayCount = 0;
        int @int = PlayerPrefs.GetInt("perfect_count", 0);
        if (@int < GameController.controller.perfectCountMax)
        {
            PlayerPrefs.SetInt("perfect_count", GameController.controller.perfectCountMax);
            AchievementObj.CheckTypes(new AchievementType[1]
            {
                AchievementType.PERFECT_10
            });
        }
        bestScoreText.text = highestScore.ToString();
    }

    private void OnDisable()
    {
        if (effect)
        {
            star1.SetActive(value: false);
            star2.SetActive(value: false);
            star3.SetActive(value: false);
        }
    }

    public void X2Diamond_Click()
    {
        SoundManager.instance.PlayGameButton();
        AdsManager.instance.ShowRewardAds(VIDEOREWARD.X2DIAMOND);
    }

    public void X2DiamondFinish()
    {
        x2Diamond.SetActive(value: false);
        TopBar.instance.UpdateDiamond(GameController.controller.diamond, Income_Type.VIDEO_X2);
        GameController.controller.diamond *= 2;
    }

    public void Rank_Click()
    {
        string path = CoreData.instance.GetCurrentSong().path;
        if (!RemoteFilesData.instance.IsLocalSong(path))
        {
            SoundManager.instance.PlayGameButton();
            if (BoardManager.instance == null)
            {
                GameObject gameObject = Util.ShowPopUp("LeaderBoard");
                gameObject.GetComponent<BoardManager>().SetupBoard(Util.SongToBoardId(path), CoreData.GetBestScore(path));
            }
        }
    }

    private void Update()
    {
        if (scoreUpdate < (float)score)
        {
            scoreUpdate += Time.deltaTime * (float)score;
            scoreText.text = Mathf.Round(scoreUpdate).ToString();
        }
        else
        {
            scoreText.text = score.ToString();
        }
        if (diamondUpdate < (float)GameController.controller.diamond)
        {
            diamondUpdate += Time.deltaTime * (float)GameController.controller.diamond;
            diamondText.text = "+" + Mathf.Round(diamondUpdate).ToString();
        }
        else
        {
            diamondText.text = "+" + GameController.controller.diamond.ToString();
        }
		
    }

    private IEnumerator MakeStarEffect()
    {
        yield return new WaitForSeconds(0.5f);
        Vector3 scale = new Vector3(1f, 1f, 0f);
        if (stars >= 1)
        {
            star1.SetActive(value: true);
            float t3 = 1f;
            while (t3 > 0f)
            {
                star1.transform.localScale = Configuration.instance.defaultCurve.Evaluate(t3) * 13f * scale;
                t3 -= Time.deltaTime * 3f;
                yield return null;
            }
            star1.transform.localScale = scale;
            MakeCameraEffect();
            SoundManager.instance.PlayStar();
            if (oldStars < 1)
            {
                UpdateDiamondStarBonus();
            }
        }
        if (stars >= 2)
        {
            star2.SetActive(value: true);
            float t2 = 1f;
            while (t2 > 0f)
            {
                star2.transform.localScale = Configuration.instance.defaultCurve.Evaluate(t2) * 13f * scale;
                t2 -= Time.deltaTime * 3f;
                yield return null;
            }
            star2.transform.localScale = scale;
            MakeCameraEffect();
            SoundManager.instance.PlayStar();
            if (oldStars < 2)
            {
                UpdateDiamondStarBonus();
            }
        }
        if (stars >= 3)
        {
            star3.SetActive(value: true);
            float t = 1f;
            while (t > 0f)
            {
                star3.transform.localScale = Configuration.instance.defaultCurve.Evaluate(t) * 13f * scale;
                t -= Time.deltaTime * 3f;
                yield return null;
            }
            star3.transform.localScale = scale;
            MakeCameraEffect();
            SoundManager.instance.PlayStar();
            if (oldStars < 3)
            {
                UpdateDiamondStarBonus();
            }
        }
        if (Configuration.instance.isNoAds)
        {
            TopBar.instance.UpdateDiamond(GameController.controller.diamond);
            GameController.controller.diamond += GameController.controller.diamond;
            diamondText.text = "+" + GameController.controller.diamond.ToString();
        }
    }

    private void UpdateDiamondStarBonus()
    {
        int starBonus = RemoteVariables.StarBonus;
        TopBar.instance.UpdateDiamond(starBonus, Income_Type.COLLECT_STARS);
        GameController.controller.diamond += starBonus;
        diamondText.text = "+" + GameController.controller.diamond.ToString();
    }

    private void MakeCameraEffect()
    {
        iTween.ShakePosition(amount: new Vector3(2f, 2f, 0f), target: base.gameObject, time: 0.1f);
    }

    public void UnlockNextSong()
    {
        if (nextSong != null)
        {
            if (CoreData.GetSongType(nextSong.path, nextSong.type) == SONGTYPE.VIDEO)
            {
                SoundManager.instance.PlayGameButton();
                AdsManager.instance.ShowRewardAds(VIDEOREWARD.UNLOCKSONG);
            }
            else if (CoreData.instance.GetDiamonds() >= nextSong.diamonds)
            {
                TopBar.instance.UpdateDiamond(-nextSong.diamonds);
                CoreData.instance.SetOpenSong(nextSong.path);
                unlockSongPlay.SetActive(value: true);
                unlockSongPrice.transform.parent.gameObject.SetActive(value: false);
                unlockSongDesc.text = LocalizationManager.instance.GetLocalizedValue("SONG_UNLOCKED");
            }
            else
            {
                Util.ShowNeedMore(nextSong.diamonds - CoreData.instance.GetDiamonds());
            }
        }
    }
	
	public IEnumerator PostRunningAds(string _param, string _message, int _ads, double _amount)
    {
		Login Udata = new Login();
        WWWForm form = new WWWForm();

        form.AddField("memberID", _param);
        form.AddField("gameID", "23");
        form.AddField("message", _message);
        form.AddField("ads", _ads);
        form.AddField("amount", _amount.ToString());

        UnityWebRequest www = UnityWebRequest.Post(Udata.base_url.ToString() + "runningAds.php", form);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log(www.downloadHandler.text);
        }

    }

    public void UnlockedVideo()
    {
        if (nextSong != null)
        {
            string path = nextSong.path;
            CoreData.instance.SetOpenSong(path);
            unlockVideo.SetActive(value: false);
            unlockSongPlay.SetActive(value: true);
            unlockSongDesc.text = LocalizationManager.instance.GetLocalizedValue("SONG_UNLOCKED");
        }
    }

    public void PlayNextSong()
    {
		
		Login lg = new Login();
		PlayerPrefs.SetString("klikStatus","1");
		PlayerPrefs.SetString("statusVAds", "1");
		SoundManager.instance.PlayGameButton();
		Util.GoToGamePlay(nextSong.path);
    }

    public void Replay()
    {
        
		Login lg = new Login();
		PlayerPrefs.SetString("klikStatus","1");
		PlayerPrefs.SetString("statusVAds", "1");
		//retry++;	
		SoundManager.instance.PlayGameButton();
		Util.GoToGamePlay(CoreData.instance.GetCurrentSong().path);
		//GameController.controller.GamePrepare();
		//AnalyticHelper.Song(SONG_STATUS.song_start, Spawner.Intance.song, 0f, retry, SONG_PLAY_COST.Free);
		//base.gameObject.SetActive(value: false);
    }

    public void Back()
    {
        if (Configuration.instance.isTutorial)
		{
			string path = CoreData.instance.GetCurrentSong().path;
			AnalyticHelper.Tutorial("tutorial_finish", Configuration.instance.playDuration, CoreData.GetBestScore(path), CoreData.GetBestStars(path), PlayerPrefs.GetInt("revive", 0), retry);
		}
		Configuration.instance.isTutorial = false;
		SoundManager.instance.PlayGameButton();
		SceneFader.instance.FadeTo("Home");
    }

    public void Share()
    {
        if (scoreUpdate >= (float)score)
        {
            effect = false;
            SoundManager.instance.PlayGameButton();
            string text = "screenshot.png";
            string screenShotPath = Application.persistentDataPath + "/" + text;
            base.transform.SetParent(shareScene.transform, worldPositionStays: false);
            RectTransform component = GetComponent<RectTransform>();
            RectTransform rectTransform = component;
            Vector2 offsetMin = component.offsetMin;
            rectTransform.offsetMin = new Vector2(offsetMin.x, -200f);
            RectTransform rectTransform2 = component;
            Vector2 offsetMax = component.offsetMax;
            rectTransform2.offsetMax = new Vector2(offsetMax.x, -200f);
            base.transform.localScale = new Vector3(0.75f, 0.75f, 0.75f);
            shareScene.SetActive(value: true);
            ScreenCapture.CaptureScreenshot(text);
            StartCoroutine(delayedShare(screenShotPath, string.Empty));
            AnalyticHelper.Share(CoreData.instance.GetCurrentSong());
        }
        AnalyticHelper.ButtonClick(FIRE_EVENT.Button_Share);
    }

    private IEnumerator delayedShare(string screenShotPath, string text)
    {
        yield return new WaitForSeconds(1f);
        base.transform.SetParent(shareScene.transform.parent, worldPositionStays: false);
        base.transform.SetSiblingIndex(6);
        RectTransform rectTransform = GetComponent<RectTransform>();
        RectTransform rectTransform2 = rectTransform;
        Vector2 offsetMin = rectTransform.offsetMin;
        rectTransform2.offsetMin = new Vector2(offsetMin.x, 0f);
        RectTransform rectTransform3 = rectTransform;
        Vector2 offsetMax = rectTransform.offsetMax;
        rectTransform3.offsetMax = new Vector2(offsetMax.x, 0f);
        base.transform.localScale = new Vector3(1f, 1f, 1f);
        shareScene.SetActive(value: false);
        effect = true;

        //@TODO
        //NativeShare.Share(text, screenShotPath, string.Empty, string.Empty, "image/png", chooser: true, string.Empty);
        //SocialManager.Instance.ShareNative(screenShotPath, text, string.Empty);
        //if (!File.Exists(screenShotPath))
        //{
        //    Util.ShowMessage("Can't save: " + screenShotPath);
        //}
    }
}
