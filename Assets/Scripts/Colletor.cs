using UnityEngine;

public class Colletor : MonoBehaviour
{
	private void OnTriggerExit(Collider col)
	{
		if (GameController.controller.game == GameStatus.LIVE)
		{
			if (col.CompareTag(GAMETAG.Tree))
			{
				col.gameObject.transform.position += Vector3.forward * 120f;
			}
			else if (col.CompareTag(GAMETAG.Ground))
			{
				Transform transformGround = col.transform;
				Vector3 localPosition = col.transform.localPosition;
				float num = Spawner.groundPathLength * (float)(Spawner.Intance.groundParts.Count - 1);
				Vector3 localPosition2 = col.transform.localPosition;
				transformGround.localPosition = Util.SetPositionZ(localPosition, num + localPosition2.z);
			}
			else if (col.CompareTag(GAMETAG.Block))
			{
				Spawner.Intance.HitByColector(col.transform.parent.gameObject.name);
			}
		}
	}
}
