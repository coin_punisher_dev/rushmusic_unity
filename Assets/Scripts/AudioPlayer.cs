using System.Collections.Generic;
using UnityEngine;

public class AudioPlayer : MonoBehaviour
{
	private struct AudioPlayerData
	{
		public int id;

		public float time;

		public bool flag;
	}

	public int numChannels = 4;

	public bool sound = true;

	private static AudioPlayer instance;

	private static Dictionary<string, AudioPlayerData> dictionary;

	private static AudioPlayerData[] streams;

	private static AndroidJavaObject androidSoundPool;

	private static AndroidJavaObject androidMediaPlayer;

	private static bool paused;

	//private void Awake()
	//{
	//	if (instance == null)
	//	{
	//		instance = this;
	//		Initialize();
	//	}
	//}

	//public static bool GetSound()
	//{
	//	return instance.sound;
	//}

	//public static void SetSound(bool sound)
	//{
	//	instance.sound = sound;
	//}

	//private void Initialize()
	//{
	//	dictionary = new Dictionary<string, AudioPlayerData>();
	//	streams = new AudioPlayerData[numChannels];
	//	int num = numChannels;
	//	int num2 = 3;
	//	int num3 = 0;
	//	androidSoundPool = new AndroidJavaObject("android.media.SoundPool", num, num2, num3);
	//	androidMediaPlayer = new AndroidJavaObject("android.media.MediaPlayer");
	//}

	//private void Destroy()
	//{
	//	if (androidMediaPlayer != null)
	//	{
	//		androidMediaPlayer.Call("release");
	//		androidMediaPlayer.Dispose();
	//		androidMediaPlayer = null;
	//	}
	//	if (androidSoundPool != null)
	//	{
	//		androidSoundPool.Call("release");
	//		androidSoundPool.Dispose();
	//		androidSoundPool = null;
	//	}
	//	streams = null;
	//	dictionary = null;
	//}

	//private void Update()
	//{
	//	if (!instance.sound || paused)
	//	{
	//		return;
	//	}
	//	float deltaTime = Time.deltaTime;
	//	if (deltaTime == 0f)
	//	{
	//		return;
	//	}
	//	for (int i = 0; i < instance.numChannels; i++)
	//	{
	//		if (streams[i].id != 0)
	//		{
	//			streams[i].time -= deltaTime;
	//			if (streams[i].time < 0f)
	//			{
	//				streams[i].time = 0f;
	//			}
	//		}
	//	}
	//}

	//public static void Load(string name)
	//{
	//	if (androidSoundPool != null && androidMediaPlayer != null && !dictionary.ContainsKey(name))
	//	{
	//		using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
	//		{
	//			using (AndroidJavaObject androidJavaObject = androidJavaClass.GetStatic<AndroidJavaObject>("currentActivity"))
	//			{
	//				using (AndroidJavaObject androidJavaObject2 = androidJavaObject.Call<AndroidJavaObject>("getAssets", new object[0]))
	//				{
	//					using (AndroidJavaObject androidJavaObject3 = androidJavaObject2.Call<AndroidJavaObject>("openFd", new object[1]
	//					{
	//						name + ".mp3"
	//					}))
	//					{
	//						float time = 1f;
	//						long num = androidJavaObject3.Call<long>("getStartOffset", new object[0]);
	//						long num2 = androidJavaObject3.Call<long>("getLength", new object[0]);
	//						using (AndroidJavaObject androidJavaObject4 = androidJavaObject3.Call<AndroidJavaObject>("getFileDescriptor", new object[0]))
	//						{
	//							androidMediaPlayer.Call("reset");
	//							androidMediaPlayer.Call("setDataSource", androidJavaObject4, num, num2);
	//							androidMediaPlayer.Call("prepare");
	//							int num3 = androidMediaPlayer.Call<int>("getDuration", new object[0]);
	//							time = (float)num3 / 1000f;
	//						}
	//						int num4 = 1;
	//						int num5 = androidSoundPool.Call<int>("load", new object[2]
	//						{
	//							androidJavaObject3,
	//							num4
	//						});
	//						if (num5 != 0)
	//						{
	//							AudioPlayerData value = default(AudioPlayerData);
	//							value.id = num5;
	//							value.time = time;
	//							value.flag = true;
	//							dictionary.Add(name, value);
	//						}
	//					}
	//				}
	//			}
	//		}
	//	}
	//}

	//public static void Play(int channel, string name, float volume = 1f, bool loop = false)
	//{
	//	if (androidSoundPool == null)
	//	{
	//		return;
	//	}
	//	Stop(channel);
	//	AudioPlayerData value;
	//	if (Configuration.instance.SoundIsOn() && dictionary.TryGetValue(name, out value) && value.flag)
	//	{
	//		int num = 0;
	//		int num2 = loop ? (-1) : 0;
	//		float num3 = 1f;
	//		int num4 = androidSoundPool.Call<int>("play", new object[6]
	//		{
	//			value.id,
	//			volume,
	//			volume,
	//			num,
	//			num2,
	//			num3
	//		});
	//		if (num4 != 0)
	//		{
	//			streams[channel].id = num4;
	//			streams[channel].time = ((!loop) ? value.time : float.PositiveInfinity);
	//		}
	//	}
	//}

	//public static void Stop(int channel)
	//{
	//	if (androidSoundPool != null && streams[channel].id != 0)
	//	{
	//		androidSoundPool.Call("stop", streams[channel].id);
	//		streams[channel].id = 0;
	//		streams[channel].time = 0f;
	//	}
	//}

	//public static bool IsPlaying(int channel)
	//{
	//	if (androidSoundPool != null && streams[channel].time != 0f)
	//	{
	//		return true;
	//	}
	//	return false;
	//}

	//public static void Pause()
	//{
	//	if (androidSoundPool != null && !paused)
	//	{
	//		paused = true;
	//		androidSoundPool.Call("autoPause");
	//	}
	//}

	//public static void Resume()
	//{
	//	if (androidSoundPool == null || !paused)
	//	{
	//		return;
	//	}
	//	paused = false;
	//	if (instance.sound)
	//	{
	//		androidSoundPool.Call("autoResume");
	//		return;
	//	}
	//	for (int i = 0; i < instance.numChannels; i++)
	//	{
	//		Stop(i);
	//	}
	//}
}
