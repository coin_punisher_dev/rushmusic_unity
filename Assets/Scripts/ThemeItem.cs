using UnityEngine;
using UnityEngine.UI;

public class ThemeItem : MonoBehaviour
{
	public int id;

	public Text priceText;

	public THEME_STATUS status;

	public GameObject[] selectedIcons;

	public GameObject[] lockedIcons;

	public Text[] texts;

	public Image[] images;

	private void OnEnable()
	{
		if (CoreData.IsOpenTheme(id))
		{
			UnlockTheme();
			if (CoreData.GetSelectedTheme(null) == id)
			{
				SelectTheme(sound: false);
			}
		}
		if (priceText != null)
		{
			priceText.text = RemoteVariables.ThemePrice.ToString();
		}
	}

	public void SetColor(float alpha)
	{
		for (int i = 0; i < texts.Length; i++)
		{
			if (texts[i] != null)
			{
				texts[i].canvasRenderer.SetAlpha(alpha);
			}
		}
		for (int j = 0; j < images.Length; j++)
		{
			if (images[j] != null)
			{
				images[j].canvasRenderer.SetAlpha(alpha);
			}
		}
	}

	public void Active()
	{
		if (status != 0)
		{
			if (CoreData.GetSelectedTheme(null) == id)
			{
				HomeManager.instance.ShowHome();
				HomeManager.instance.ballList.gameObject.SetActive(value: false);
				SoundManager.instance.PlayGameButton();
			}
			else
			{
				SelectTheme();
			}
		}
	}

	public void UnlockTheme()
	{
		for (int i = 0; i < lockedIcons.Length; i++)
		{
			if (lockedIcons[i] != null)
			{
				lockedIcons[i].SetActive(value: false);
			}
		}
		CoreData.SetOpenTheme(id);
		status = THEME_STATUS.OPENED;
	}

	public void UnSelect()
	{
		for (int i = 0; i < selectedIcons.Length; i++)
		{
			if (selectedIcons[i] != null)
			{
				selectedIcons[i].SetActive(value: false);
			}
		}
	}

	public void SelectTheme(bool sound = true)
	{
		if (sound)
		{
			SoundManager.instance.PlaySelect();
		}
		HomeManager.instance.themeList.UnSelectTheme();
		for (int i = 0; i < selectedIcons.Length; i++)
		{
			if (selectedIcons[i] != null)
			{
				selectedIcons[i].SetActive(value: true);
			}
		}
		CoreData.instance.SetSelectedTheme(id);
	}

	public void PayDiamonds()
	{
		SoundManager.instance.PlayGameButton();
		if (CoreData.instance.GetDiamonds() >= RemoteVariables.ThemePrice)
		{
			TopBar.instance.UpdateDiamond(-RemoteVariables.ThemePrice);
			UnlockTheme();
			SelectTheme();
		}
		else
		{
			Util.ShowNeedMore(RemoteVariables.ThemePrice - CoreData.instance.GetDiamonds());
		}
	}

	public void PlayVideo()
	{
		SoundManager.instance.PlayGameButton();
		AdsManager.instance.ShowRewardAds(VIDEOREWARD.UNLOCKTHEME);
	}
}
