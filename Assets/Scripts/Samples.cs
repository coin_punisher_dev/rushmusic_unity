#if ENABLE_AMANOTES
using Amanotes.Data;
#endif
using Newtonsoft.Json;
#if ENABLE_JSON_FX
using Pathfinding.Serialization.JsonFx;
#endif
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Samples : MonoBehaviour
{
	private void Start()
	{
		Load();
	}

	private void Update()
	{
	}

	public void Load()
	{
		string str = "test.bin";
		string url = Application.streamingAssetsPath + "/" + str;
		StartCoroutine(LoadFileForAndroid(url, delegate(byte[] res)
		{
			UnityEngine.Debug.LogError("kich thuoc file android:" + res.Length);
			LoadContent(res);
		}));
	}

	public IEnumerator LoadFileForAndroid(string url, Action<byte[]> callback)
	{
		WWW www = new WWW(url);
		yield return www;
		callback(www.bytes);
	}

	public void LoadContent(byte[] data)
	{

	}

	public static byte[] LoadFile(string filePath)
	{
		if (filePath == null || filePath.Length == 0)
		{
			return null;
		}
		if (File.Exists(filePath))
		{
			return File.ReadAllBytes(filePath);
		}
		return null;
	}
}
