using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class TopBar : MonoBehaviour
{
	public Text diamiondText;

	public GameObject diamondContainer;

	public Text scoreTextWhite;

	public Text scoreTextBlack;

	public static TopBar instance;

	private int updateAmount;

	private int updateAmountFrame;

	private int updateSign = 1;

	public Text diamondPlusText;

	public Animation diamondPlusAnim;

	public Animation scorePlusAnim;

	public Animation highScoreAnim;

	private void Awake()
	{
		instance = this;
	}

	private void Start()
	{
		diamiondText.text = CoreData.instance.GetDiamonds().ToString();
	}

	private void Update()
	{
		if (updateAmount > 0)
		{
			if (updateAmount - updateAmountFrame > 0)
			{
				updateAmount -= updateAmountFrame;
			}
			else
			{
				updateAmountFrame = updateAmount;
				updateAmount = 0;
			}
			diamiondText.text = (int.Parse(diamiondText.text) + updateSign * updateAmountFrame).ToString();
		}
	}

	public void UpdateDiamond(int change, Income_Type type = Income_Type.NONE)
	{
		if (type != Income_Type.NONE)
		{
			AnalyticHelper.Currency_Income(type, change);
		}
		if (change > 0)
		{
			if (change >= 5)
			{
				SoundManager.instance.PlayCoins();
			}
		}
		else if (change < 0)
		{
			SoundManager.instance.PlayBuy();
		}
		if (base.gameObject.activeInHierarchy)
		{
			StartCoroutine(PlayTextEffect(change));
			return;
		}
		if (change < 0)
		{
			updateSign = -1;
		}
		else
		{
			updateSign = 1;
		}
		updateAmount += updateSign * change;
		updateAmountFrame = (int)(Time.deltaTime * (float)updateAmount) * 4;
		if (updateAmountFrame == 0)
		{
			updateAmountFrame = 1;
		}
		CoreData.instance.IncreaseDiamonds(change);
	}

	private IEnumerator PlayTextEffect(int amount)
	{
		string updateString = (amount <= 0) ? ("-" + Mathf.Abs(amount).ToString()) : ("+" + amount.ToString());
		diamondPlusText.text = updateString;
		if (diamondPlusText.transform.gameObject.activeSelf)
		{
			diamondPlusAnim.Play();
		}
		else
		{
			diamondPlusAnim.Play();
			diamondPlusText.transform.gameObject.SetActive(value: true);
		}
		yield return new WaitForSeconds(0.4f);
		diamondPlusText.transform.gameObject.SetActive(value: false);
		if (amount < 0)
		{
			updateSign = -1;
		}
		else
		{
			updateSign = 1;
		}
		updateAmount += updateSign * amount;
		updateAmountFrame = (int)(Time.deltaTime * (float)updateAmount) * 4;
		if (updateAmountFrame == 0)
		{
			updateAmountFrame = 1;
		}
		CoreData.instance.IncreaseDiamonds(amount);
	}

	public void UpdateScore(int score)
	{
		scoreTextWhite.text = score.ToString();
		scoreTextBlack.text = scoreTextWhite.text;
		scorePlusAnim.Play();
	}

	public void ToggleDiamond(bool enable)
	{
		diamondContainer.SetActive(enable);
	}

	public void ShowLeaderBoard()
	{
		SoundManager.instance.PlayGameButton();
		int num = 0;
		if (num == 0)
		{
			foreach (string key in RemoteFilesData.instance.songList.Keys)
			{
				num += CoreData.GetBestScore(key);
			}
			CoreData.SetBestScore(num, "Total");
		}
		if (BoardManager.instance == null)
		{
			GameObject gameObject = Util.ShowPopUp("LeaderBoard");
			gameObject.GetComponent<BoardManager>().SetupBoard("Total", num);
		}
	}

	public void ToggleScore(bool enable)
	{
		scoreTextBlack.transform.parent.gameObject.SetActive(enable);
		diamondContainer.SetActive(value: false);
	}

	public void GamePrepare()
	{
		ToggleScore(enable: true);
		scoreTextWhite.text = GameController.controller.score.ToString();
		scoreTextBlack.text = scoreTextWhite.text;
	}

	public void ShowShop()
	{
		/*if (Shop.instance == null)
		{
			SoundManager.instance.PlayGameButton();
			Util.ShowPopUp("Shop");
			AnalyticHelper.ButtonClick(FIRE_EVENT.Button_Shop);
		}*/
	}

	public void ShowFreeVideo()
	{
		SoundManager.instance.PlayGameButton();
		AnalyticHelper.ButtonClick(FIRE_EVENT.Button_FreeVideo);
		FreeVideo.ShowPopUp();
	}
}
