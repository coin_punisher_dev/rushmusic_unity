using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FreeVideoItem : MonoBehaviour
{
	public Text title;

	public Text text;

	public Image bg;

	public Image ballPreview;

	public FREEVIDEO_TYPE type = FREEVIDEO_TYPE.DIAMOND;

	private string idString;

	private int idInt;

	private int index;

	public Color giftColorInactive;

	private Color giftColor;

	private Image giftImg;

	private string freeText = string.Empty;

	private string TIMEFORMAT = "{0:00} : {1:00}";

	private void Awake()
	{
		if (type == FREEVIDEO_TYPE.HOURLY)
		{
			title.text = LocalizationManager.instance.GetLocalizedValue("X_DIAMONDS").Replace("10", "5");
			giftImg = base.transform.Find("Image").GetComponent<Image>();
			giftColor = giftImg.color;
		}
		freeText = LocalizationManager.instance.GetLocalizedValue("FREE");
	}

	private void Update()
	{
		if (type == FREEVIDEO_TYPE.HOURLY)
		{
			DateTime d = DateTime.Parse(Configuration.GetGiftTime("hourlygift_time"));
			double totalSeconds = (DateTime.Now - d).TotalSeconds;
			double num = 1800.0 - totalSeconds;
			if (num > 0.0)
			{
				text.text = string.Format(TIMEFORMAT, (int)(num / 60.0), (int)(num % 60.0));
				giftImg.color = giftColorInactive;
			}
			else
			{
				text.text = freeText;
				giftImg.color = giftColor;
			}
		}
	}

	public void SetValues(FREEVIDEO_TYPE type, Sprite bg, string idString, int idInt, int index)
	{
		this.type = type;
		this.bg.sprite = bg;
		this.idString = idString;
		this.idInt = idInt;
		this.index = index;
		text.text = LocalizationManager.instance.GetLocalizedValue("FREE");
		if (this.type == FREEVIDEO_TYPE.DIAMOND)
		{
			title.text = LocalizationManager.instance.GetLocalizedValue("X_DIAMONDS").Replace("10", Configuration.instance.diamondsVideoBonus.ToString());
			FreeVideo.instance.diamondBonusText = title;
		}
		else if (this.type == FREEVIDEO_TYPE.BALL)
		{
			title.text = "Ball " + idInt.ToString();
			ballPreview.gameObject.SetActive(value: true);
			GameObject gameObject = HomeManager.instance.ballList.ballContainer.transform.GetChild(idInt).transform.GetChild(0).gameObject;
			ballPreview.sprite = gameObject.GetComponent<Image>().sprite;
		}
		else if (this.type == FREEVIDEO_TYPE.THEME)
		{
			title.text = LocalizationManager.instance.GetLocalizedValue("THEME2_NAME") + " " + LocalizationManager.instance.GetLocalizedValue("THEME");
		}
		else
		{
			title.text = RemoteFilesData.instance.songList[idString].name;
		}
	}

	public void Button_Click()
	{
		if (FreeVideo.instance != null)
		{
			FreeVideo.instance.currentIndex = index;
		}
		SoundManager.instance.PlayGameButton();
		bool activeSelf = base.transform.Find("Play").gameObject.activeSelf;
		if (Configuration.instance.showFreeGifts == Configuration.FreeGifts.SHOW)
		{
			AnalyticHelper.FireEvent(FIRE_EVENT.FreeVideo_Click_Force, new Dictionary<string, object>
			{
				{
					"Type",
					type.ToString()
				}
			});
		}
		else
		{
			AnalyticHelper.FireEvent(FIRE_EVENT.FreeVideo_Click_Normal, new Dictionary<string, object>
			{
				{
					"Type",
					type.ToString()
				}
			});
		}
		switch (type)
		{
		case FREEVIDEO_TYPE.DIAMOND:
			AdsManager.instance.ShowRewardAds(VIDEOREWARD.GETDIAMOND);
			break;
		case FREEVIDEO_TYPE.BALL:
			if (!activeSelf)
			{
				AdsManager.instance.ShowRewardAds(VIDEOREWARD.UNLOCKBALL);
				break;
			}
			CoreData.SetSelectedBall(idInt);
			Util.GoToGamePlay(CoreData.instance.GetCurrentSong().path);
			break;
		case FREEVIDEO_TYPE.THEME:
			if (!activeSelf)
			{
				AdsManager.instance.ShowRewardAds(VIDEOREWARD.UNLOCKTHEME);
				break;
			}
			CoreData.instance.SetSelectedTheme(idInt);
			Util.GoToGamePlay(CoreData.instance.GetCurrentSong().path);
			break;
		case FREEVIDEO_TYPE.SONG:
			if (!activeSelf)
			{
				AdsManager.instance.ShowRewardAds(VIDEOREWARD.UNLOCKSONG);
				break;
			}
			Util.GoToGamePlay(idString);
			if (HomeManager.instance != null)
			{
				HomeManager.instance.groundMusic.StopMusic();
			}
			break;
		case FREEVIDEO_TYPE.HOURLY:
			if (ItweenShakeEffect.isShake())
			{
				Configuration.SetGiftTime("hourlygift_time", DateTime.Now.ToString());
				TopBar.instance.UpdateDiamond(5, Income_Type.HOURLY_GIFT);
			}
			break;
		}
	}

	public void Completed()
	{
		switch (type)
		{
		case FREEVIDEO_TYPE.DIAMOND:
			TopBar.instance.UpdateDiamond(20, Income_Type.VIDEO);
			break;
		case FREEVIDEO_TYPE.BALL:
			base.transform.Find("Play").gameObject.SetActive(value: true);
			CoreData.SetOpenBall(idInt);
			break;
		case FREEVIDEO_TYPE.THEME:
			base.transform.Find("Play").gameObject.SetActive(value: true);
			CoreData.SetOpenTheme(idInt);
			break;
		case FREEVIDEO_TYPE.SONG:
			base.transform.Find("Play").gameObject.SetActive(value: true);
			if (SongList.instance != null)
			{
				SongList.instance.OpenSongByID(idString);
			}
			else
			{
				CoreData.instance.SetOpenSong(idString);
			}
			break;
		}
	}
}
