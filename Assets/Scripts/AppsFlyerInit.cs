//@TODO ENABLE_APPSFLYER
//#define ENABLE_APPSFLYER

using System.Collections.Generic;
using UnityEngine;

public class AppsFlyerInit : MonoBehaviour
{
	public enum KEY
	{
		lvup,
		video_ads,
		interstitial_ads,
		video_ads_click,
		interstitial_ads_click,
		banner_ad_click,
		firstopen,
		session
	}

	public string iosAppId;

	public string appsFlyerKey;

	public static AppsFlyerInit instance;

	private static Dictionary<string, string> param = new Dictionary<string, string>();

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
			Object.DontDestroyOnLoad(base.gameObject);
		}
		else if (instance != null)
		{
			UnityEngine.Object.Destroy(base.gameObject);
		}
	}

	private void Start()
	{
#if ENABLE_APPSFLYER
        AppsFlyer.setAppsFlyerKey(appsFlyerKey);
		AppsFlyer.setAppID(Application.identifier);
		AppsFlyer.init(appsFlyerKey);
#endif
    }

    public static void TrackEvent(KEY k)
	{
		LogEvent(k.ToString());
		if (k == KEY.firstopen)
		{
			TrackLvl();
		}
	}

	public static void TrackLvl()
	{
		int num = CoreData.SongUnlockCount();
		if (num <= 10)
		{
			LogEvent("lvup_" + CoreData.SongUnlockCount().ToString());
		}
		else if (num == RemoteFilesData.instance.songList.Count - 2)
		{
			LogEvent("lvup_maxminus2");
		}
		else if (num == RemoteFilesData.instance.songList.Count)
		{
			LogEvent("lvup_max");
		}
	}

	public static void TrackAd(KEY k)
	{
		LogEvent(k.ToString());
		LogEvent(k.ToString() + "_" + GetCountNumber(k.ToString()));
	}

	public static void TrackAdBanner(KEY k)
	{
		LogEvent(k.ToString());
		LogEvent(k.ToString() + "_" + GetCountNumber(k.ToString(), 5, 5));
	}

	public static void TrackSession()
	{
		LogEvent(KEY.session.ToString() + "_" + GetCountNumber(KEY.session.ToString()));
	}

	public static string GetCountNumber(string k, int N = 5, int max = 15)
	{
		int @int = PlayerPrefs.GetInt("COUNT_" + k, 0);
		@int++;
		PlayerPrefs.SetInt("COUNT_" + k, @int);
		if (@int > 1)
		{
			if (k == KEY.session.ToString())
			{
				if (@int > max)
				{
					return "N";
				}
			}
			else
			{
				if (@int > max)
				{
					return "N";
				}
				@int = @int / N * N + N;
			}
		}
		return @int.ToString();
	}

	public static void LogEvent(string eventName)
	{
#if ENABLE_APPSFLYER
		AppsFlyer.trackRichEvent(eventName, param);
#endif
		AnalyticHelper.FireString(eventName);
	}
}
