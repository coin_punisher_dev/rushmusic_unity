#if ENABLE_FACEBOOK_SDK
using Facebook.MiniJSON;
using Facebook.Unity;
#endif
#if ENABLE_FIREBASE
using Firebase;
using Firebase.Analytics;
using Firebase.Auth;
using Firebase.Database;
using Firebase.Messaging;
#endif
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class CoreUser : MonoBehaviour
{
	public enum FirebaseStatus
	{
		INIT,
		READY,
		FAILED
	}

	public delegate void FriendLoadedHandler();

	public delegate void LoggedHandler();

	public static CoreUser instance;

#if ENABLE_FIREBASE
	public DatabaseReference db;
#endif

#if ENABLE_FIREBASE
	private FirebaseAuth auth;
#endif

	public UserEntry user;
    [SerializeField]
	private List<UserEntry> friendList = new List<UserEntry>();

	//[HideInInspector]
	public bool loadedFriend;

	public static FirebaseStatus firebaseStatus;

	public event FriendLoadedHandler onFriendLoaded;

	public event LoggedHandler onLogged;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
			UnityEngine.Object.DontDestroyOnLoad(base.gameObject);
		}
		else if (instance != null)
		{
			UnityEngine.Object.Destroy(base.gameObject);
		}
	}

	private void Start()
	{
		InitializeFacebook();
#if ENABLE_FIREBASE
		DependencyStatus dependencyStatus = FirebaseApp.CheckDependencies();
		if (dependencyStatus != 0)
		{
			FirebaseApp.FixDependenciesAsync().ContinueWith(delegate
			{
				dependencyStatus = FirebaseApp.CheckDependencies();
				if (dependencyStatus == DependencyStatus.Available)
				{
					InitializeFirebase();
				}
				else
				{
					firebaseStatus = FirebaseStatus.FAILED;
				}
			});
		}
		else
		{
			InitializeFirebase();
		}
#endif
    }

    private void GetStats()
	{
#if ENABLE_FIREBASE
		instance.db.Child("Users").GetValueAsync().ContinueWith(delegate(Task<DataSnapshot> task)
		{
			if (!task.IsFaulted && task.IsCompleted)
			{
				StartCoroutine(ParseStatsData(task.Result));
			}
		});
#endif
	}

#if ENABLE_FIREBASE
	private IEnumerator ParseStatsData(DataSnapshot data)
	{
		string alldt11 = "Total";
		foreach (Item value in RemoteFilesData.instance.songList.Values)
		{
			alldt11 = alldt11 + "," + value.name;
		}
		alldt11 += ",createdDate";
		alldt11 += ",lastDate";
		alldt11 += ",lang";
		alldt11 += ",version";
		alldt11 += ",Retry";
		alldt11 += ",Revive";
		alldt11 += ",Diamonds";
		alldt11 += ",PerfectCount";
		alldt11 += ",SelectedTheme";
		alldt11 += "\n";
		int i = 0;
		UnityEngine.Debug.Log(data.ChildrenCount);
		foreach (DataSnapshot childSnapshot in data.Children)
		{
			UserEntry usr = new UserEntry(childSnapshot);
			if (usr.scores.ContainsKey("Total"))
			{
				alldt11 += usr.scores["Total"];
			}
			foreach (Item value2 in RemoteFilesData.instance.songList.Values)
			{
				string key = Util.SongToBoardId(value2.path);
				alldt11 += ",";
				if (usr.scores.ContainsKey(key))
				{
					alldt11 += usr.scores[key];
				}
			}
			alldt11 += ",";
			if (usr.createdDate != null)
			{
				alldt11 += usr.createdDate;
			}
			alldt11 += ",";
			if (usr.lastDate != null)
			{
				alldt11 += usr.lastDate;
			}
			alldt11 += ",";
			if (usr.lang != null)
			{
				alldt11 += usr.lang;
			}
			alldt11 += ",";
			if (usr.version != null)
			{
				alldt11 += usr.version;
			}
			if (usr.gameData != null)
			{
				object obj = Json.Deserialize(usr.gameData);
				if (obj != null)
				{
					Dictionary<string, object> dictionary = (Dictionary<string, object>)obj;
					alldt11 += ",";
					if (dictionary.ContainsKey("diamonds"))
					{
						alldt11 += dictionary["diamonds"].ToString();
					}
					alldt11 += ",";
					if (dictionary.ContainsKey("retry"))
					{
						alldt11 += dictionary["retry"].ToString();
					}
					alldt11 += ",";
					if (dictionary.ContainsKey("revive"))
					{
						alldt11 += dictionary["revive"].ToString();
					}
					alldt11 += ",";
					if (dictionary.ContainsKey("perfect_count"))
					{
						alldt11 += dictionary["perfect_count"].ToString();
					}
					alldt11 += ",";
					if (dictionary.ContainsKey("selected_theme"))
					{
						alldt11 += dictionary["selected_theme"].ToString();
					}
				}
			}
			alldt11 += "\n";
			i++;
			if (i % 200 == 0)
			{
				UnityEngine.Debug.Log(i);
				yield return null;
			}
		}
		RemoteFilesData.instance.SaveFile(alldt11, "stats.csv");
	}
#endif

	private void InitializeFacebook()
	{
#if ENABLE_FACEBOOK_SDK
		FB.Init(OnInitFacebookComplete, OnFacebookHideUnity);
#endif
	}

	protected virtual void InitializeFirebase()
	{
#if ENABLE_FIREBASE
		try
		{
			db = FirebaseDatabase.DefaultInstance.RootReference;
			auth = FirebaseAuth.DefaultInstance;
			auth.StateChanged += AuthStateChanged;
			AuthStateChanged(this, null);
			FirebaseMessaging.TokenReceived += OnTokenReceived;
			FirebaseMessaging.MessageReceived += OnMessageReceived;
			firebaseStatus = FirebaseStatus.READY;
			Application.RequestAdvertisingIdentifierAsync(delegate(string advertisingId, bool trackingEnabled, string error)
			{
				string text = advertisingId;
				if (string.IsNullOrEmpty(text))
				{
					text = SystemInfo.deviceUniqueIdentifier;
				}
				FirebaseAnalytics.SetUserProperty("uid", text);
			});
		}
		catch (Exception)
		{
			auth = null;
			firebaseStatus = FirebaseStatus.FAILED;
		}
#endif
    }

#if ENABLE_FIREBASE
	public void OnTokenReceived(object sender, TokenReceivedEventArgs token)
	{
        //@TODO ENABLE_APPSFLYER
#if ENABLE_APPSFLYER
        AppsFlyer.updateServerUninstallToken(token.Token);
#endif
    }

    public void OnMessageReceived(object sender, MessageReceivedEventArgs e)
	{
		AnalyticHelper.FireNotifications(e.Message.MessageId);
	}
#endif
    public void FacebookLogin()
	{
#if ENABLE_FIREBASE
		if (auth != null && auth.CurrentUser == null)
		{
			List<string> list = new List<string>();
			list.Add("public_profile");
			list.Add("email");
			list.Add("user_friends");
			FB.LogInWithReadPermissions(list, FacebookLoginHandleResult);
			SceneFader.instance.ShowOverlay();
		}
#endif
	}

#if ENABLE_FACEBOOK_SDK
	protected void FacebookLoginHandleResult(IResult result)
	{
		if (FB.IsLoggedIn)
		{
			AccessToken currentAccessToken = AccessToken.CurrentAccessToken;
			FacebookLoginFireBase(currentAccessToken.TokenString);
		}
		else
		{
			SceneFader.instance.HideOverlay();
		}
	}
#endif

	private void OnInitFacebookComplete()
	{
#if ENABLE_FACEBOOK_SDK
		if (AccessToken.CurrentAccessToken != null)
		{
			AuthStateChanged(this, null);
		}
#endif
	}

	private void OnFacebookHideUnity(bool isGameShown)
	{
	}

	private void FacebookLoginFireBase(string accessToken)
	{
#if ENABLE_FIREBASE
		Credential credential = FacebookAuthProvider.GetCredential(accessToken);
		auth.SignInWithCredentialAsync(credential).ContinueWith(delegate(Task<FirebaseUser> task)
		{
			if (task.IsCanceled || task.IsFaulted)
			{
				SceneFader.instance.HideOverlay();
			}
		});
#endif
	}

	private void AuthStateChanged(object sender, EventArgs eventArgs)
	{
#if ENABLE_FIREBASE
		if (user == null && auth != null && auth.CurrentUser != null && AccessToken.CurrentAccessToken != null)
		{
			bool flag = PlayerPrefs.GetInt("bonus_fb_login", 0) == 0;
			user = new UserEntry(auth.CurrentUser, AccessToken.CurrentAccessToken.UserId);
			if (flag)
			{
				PlayerPrefs.SetInt("bonus_fb_login", 1);
				db.Child("Users").Child(user.auId).GetValueAsync()
					.ContinueWith(delegate(Task<DataSnapshot> task)
					{
						if (task.IsFaulted)
						{
							user = null;
							Util.ShowMessage("Can't load the user. Please restart the game!");
						}
						else if (task.IsCompleted)
						{
							DataSnapshot result = task.Result;
							if (result.Value != null)
							{
								AnalyticHelper.FacebookLogin(LOGIN_TYPE.Existing_User);
								user = new UserEntry(result);
								user.RestoreGameData();
								ReloadUI();
							}
							else
							{
								AnalyticHelper.FacebookLogin(LOGIN_TYPE.New_User);
								StartCoroutine(UpdateMyScores());
								user.createdDate = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
								user.Backup();
								TopBar.instance.UpdateDiamond(20, Income_Type.ACHIEVEMENT);
							}
							FB.API("/me/friends", HttpMethod.GET, HandleLoadFriends);
						}
					});
			}
			else
			{
				StartCoroutine(UpdateMyScores());
				FB.API("/me/friends", HttpMethod.GET, HandleLoadFriends);
			}
			if (this.onLogged != null)
			{
				this.onLogged();
			}
		}
#endif
	}

	private void ReloadUI()
	{
		if (SongList.instance != null)
		{
			SongList.instance.ReloadSongs();
		}
		TopBar.instance.diamiondText.text = CoreData.instance.GetDiamonds().ToString();
	}

	private IEnumerator UpdateMyScores()
	{
		while (RemoteFilesData.instance.songList.Count == 0)
		{
			yield return null;
		}
		user.scores = new Dictionary<string, int>();
		int totalScore = 0;
		foreach (Item value in RemoteFilesData.instance.songList.Values)
		{
			SONGTYPE songType = CoreData.GetSongType(value.path, value.type);
			if (songType == SONGTYPE.OPEN)
			{
				int bestScore = CoreData.GetBestScore(value.path);
				if (bestScore > 0)
				{
					user.scores[Util.SongToBoardId(value.path)] = bestScore;
					totalScore += bestScore;
				}
			}
		}
		user.scores["Total"] = totalScore;
		user.SaveProfiles();
	}

#if ENABLE_FACEBOOK_SDK
	private void HandleLoadFriends(IGraphResult result)
	{
		if (string.IsNullOrEmpty(result.Error))
		{
			friendList = new List<UserEntry>();
			IDictionary<string, object> resultDictionary = result.ResultDictionary;
			List<object> friends = (List<object>)resultDictionary["data"];
			if (friends.Count == 0)
			{
				CompletedLoadFriend();
				return;
			}
			int i = 0;
#if ENABLE_FIREBASE
			foreach (object item2 in friends)
			{
				Dictionary<string, object> dictionary = (Dictionary<string, object>)item2;
				db.Child("Links").Child(dictionary["id"].ToString()).GetValueAsync()
					.ContinueWith(delegate(Task<DataSnapshot> task)
					{
						i++;
						if (!task.IsFaulted && task.IsCompleted)
						{
							DataSnapshot result2 = task.Result;
							if (result2 != null && result2.Value != null)
							{
								UserEntry item = new UserEntry
								{
									fbId = result2.Key,
									auId = result2.Value.ToString()
								};
								friendList.Add(item);
							}
						}
						if (i == friends.Count)
						{
							ReloadFriends(force: true);
						}
					});
			}
#endif
        }
    }
#endif
    public void ReloadFriends(bool force = false)
	{
		if (loadedFriend || force)
		{
			int i = 0;
			if (friendList.Count > 0)
			{
#if ENABLE_FIREBASE
				foreach (UserEntry friend in friendList)
				{
					db.Child("Users").Child(friend.auId).GetValueAsync()
						.ContinueWith(delegate(Task<DataSnapshot> task)
						{
							i++;
							if (!task.IsFaulted && task.IsCompleted)
							{
								DataSnapshot result = task.Result;
								if (result.Value != null)
								{
									friend.Bind(result);
								}
							}
							if (i == friendList.Count)
							{
								CompletedLoadFriend();
							}
						});
				}
#endif
            }
            else
			{
				CompletedLoadFriend();
			}
		}
	}

	public void CompletedLoadFriend()
	{
		SceneFader.instance.HideOverlay();
		loadedFriend = true;
		if (this.onFriendLoaded != null)
		{
			this.onFriendLoaded();
		}
		if (LeaderBoardUI.instance != null)
		{
			LeaderBoardUI.instance.FriendLoadedTrigger();
		}
	}

	public void ReportScore(int score, string songID)
	{
		CoreData.SetBestScore(score, songID);
		int num = 0;
		foreach (string key in RemoteFilesData.instance.songList.Keys)
		{
			num += CoreData.GetBestScore(key);
		}
		CoreData.SetBestScore(num, "Total");
		if (user != null && !RemoteFilesData.instance.IsLocalSong(songID))
		{
			user.SaveScores(Util.SongToBoardId(songID), score, num);
		}
	}

	public List<UserEntry> GetFriendList(string boardId)
	{
		List<UserEntry> list = new List<UserEntry>();
		if (user != null && user.scores != null && user.scores.ContainsKey(boardId))
		{
			list.Add(user);
		}
		for (int i = 0; i < friendList.Count; i++)
		{
			if (friendList[i].scores != null && friendList[i].scores.ContainsKey(boardId))
			{
				list.Add(friendList[i]);
			}
		}
		list.Sort((UserEntry x, UserEntry y) => -1 * x.scores[boardId].CompareTo(y.scores[boardId]));
		for (int j = 0; j < list.Count; j++)
		{
			list[j].ranks[boardId] = j + 1;
		}
		return list;
	}

	private void OnApplicationPause(bool pauseStatus)
	{
		if (pauseStatus)
		{
			PlayerPrefs.Save();
			if (user != null)
			{
				user.Backup();
			}
		}
	}

	private void OnApplicationQuit()
	{
		PlayerPrefs.Save();
	}
}
