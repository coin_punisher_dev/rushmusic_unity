using UnityEngine;

public class HandEffect : MonoBehaviour
{
	public ParticleSystem left;

	public ParticleSystem right;

	private ParticleSystem.EmissionModule leftEmi;

	private ParticleSystem.EmissionModule rightEmi;

	private float x;

	private void Start()
	{
		leftEmi = left.emission;
		rightEmi = right.emission;
	}

	private void Update()
	{
		if (left.isPlaying)
		{
			Vector3 position = base.transform.position;
			if (position.x - x > 0f)
			{
				leftEmi.rateOverTime = 0f;
				rightEmi.rateOverTime = 1.8f;
			}
			else
			{
				leftEmi.rateOverTime = 1.8f;
				rightEmi.rateOverTime = 0f;
			}
			Vector3 position2 = base.transform.position;
			x = position2.x;
		}
	}
}
