using System.Collections;
using UnityEngine;

public class RemoteVariables : MonoBehaviour
{
	public static string DefaultDataVersion = "4.0";

	public static string DefaultLanguageVersion = "2.0";

	public float DefaultBallSpeed = 4.5f;

	public float DefaultFingerSensitivity = 3f;

	public float DefaultFingerSpeed = 30f;

	public float DefaultCammeraFollowSpeed = 30f;

	public float DefaultJumpHeight = 5f;

	public float DefaultBlock_MaxDistance = 4.5f;

	public float DefaultBlock_Span = 9f;

	public float DefaultFixLatency;

	public float DefaultUpSpeedMax = 1.2f;

	public float DefaultBlock_DeltaSize = 0.25f;

	public float DefaultBlock_PerfectSize = 0.02f;

	public float DefaultAdsTime = 2f;

	public float DefaultBlock_MaxSize = 6f;

	public float DefaultBlock_MinSize = 1f;

	public float DefaultBlock_Sag = 0.65f;

	public float DefaultBlock_MovingTime = 0.6f;

	public int DefaultBlock_EndlessMovingOnly = 1;

	public int DefaultVideo_LocalSong = 2;

	public int DefaultEnableCBCrosspromotion;

	public int DefaultBlock_Total = 5;

	public int DefaultUpSpeedScore = 30;

	public int DefaultMaxContinue = 1;

	public int DefaultBallPrice = 20;

	public int DefaultThemePrice = 20;

	public int DefaultStarBonus = 10;

	public int DefaultBlock_PerfectTrigger = 5;

	public int DefaultBlock_BadTrigger = 5;

	public int DefaultUnlockAllSongs;

	public int DefaultDiamondsRate = 10;

	public int DefaultBurstMarketing = 1;

	public int DefaultDefaultTheme = 5;

	public int DefaultDiamondsVideoBonus = 20;

	public int DefaultScoreToRate = 200;

	public int DefaultPlayCountToRate = 5;

	public int DefaultUnlock_NewChallenge = 5;

	public int DefaultEnablePokkt;

	public float DefaultPushNewSongDate = 2f;

	public int DefaultDistance_Interval = 20;

	public string DefaultDistance_MinMax1;

	public string DefaultDistance_MinMax2;

	public string DefaultDistance_MinMax3;

	public string DefaultDistance_MinMax4;

	public int DefaultTutorial_AdsStar = 2;

	public int DefaultActiveBanner;

	public string DefaultConfigPath = "http://undefinedemail47.byethost4.com/rush_music/config/songconfig.csv";

	public string DefaultDataPath = "http://undefinedemail47.byethost4.com/rush_music/music/";

	public static bool loaded;

	public static float BallSpeed
	{
		get;
		private set;
	}

	public static float FingerSensitivity
	{
		get;
		private set;
	}

	public static float FingerSpeed
	{
		get;
		private set;
	}

	public static float CammeraFollowSpeed
	{
		get;
		private set;
	}

	public static float JumpHeight
	{
		get;
		private set;
	}

	public static float Block_MaxDistance
	{
		get;
		private set;
	}

	public static float Block_Span
	{
		get;
		private set;
	}

	public static float FixLatency
	{
		get;
		private set;
	}

	public static float UpSpeedMax
	{
		get;
		private set;
	}

	public static float Block_DeltaSize
	{
		get;
		private set;
	}

	public static float Block_PerfectSize
	{
		get;
		private set;
	}

	public static float AdsTime
	{
		get;
		private set;
	}

	public static float PushNewSongDate
	{
		get;
		private set;
	}

	public static float Block_MaxSize
	{
		get;
		private set;
	}

	public static float Block_MinSize
	{
		get;
		private set;
	}

	public static float Block_Sag
	{
		get;
		private set;
	}

	public static float Block_MovingTime
	{
		get;
		private set;
	}

	public static int Block_Total
	{
		get;
		private set;
	}

	public static int UpSpeedScore
	{
		get;
		private set;
	}

	public static int ColorChangeInterval
	{
		get;
		private set;
	}

	public static int MaxContinue
	{
		get;
		private set;
	}

	public static int Block_EndlessMovingOnly
	{
		get;
		private set;
	}

	public static int Video_LocalSong
	{
		get;
		private set;
	}

	public static bool EnableCBCrosspromotion
	{
		get;
		set;
	}

	public static bool EnablePokkt
	{
		get;
		set;
	}

	public static int BallPrice
	{
		get;
		private set;
	}

	public static int ThemePrice
	{
		get;
		private set;
	}

	public static int Block_PerfectTrigger
	{
		get;
		private set;
	}

	public static int Block_BadTrigger
	{
		get;
		private set;
	}

	public static int UnlockAllSongs
	{
		get;
		private set;
	}

	public static int DiamondsRate
	{
		get;
		private set;
	}

	public static int StarBonus
	{
		get;
		private set;
	}

	public static int BurstMarketing
	{
		get;
		private set;
	}

	public static int DefaultTheme
	{
		get;
		private set;
	}

	public static int DiamondsVideoBonus
	{
		get;
		private set;
	}

	public static int PlayCountToRate
	{
		get;
		private set;
	}

	public static int ScoreToRate
	{
		get;
		private set;
	}

	public static int Unlock_NewChallenge
	{
		get;
		private set;
	}

	public static int Tutorial_AdsStar
	{
		get;
		private set;
	}

	public static int Distance_Interval
	{
		get;
		private set;
	}

	public static float[] Distance_MinMax1
	{
		get;
		private set;
	}

	public static float[] Distance_MinMax2
	{
		get;
		private set;
	}

	public static float[] Distance_MinMax3
	{
		get;
		private set;
	}

	public static float[] Distance_MinMax4
	{
		get;
		private set;
	}

	public static string ConfigPath
	{
		get;
		private set;
	}

	public static string DataPath
	{
		get;
		private set;
	}

	public static string DataVersion
	{
		get;
		private set;
	}

	public static string LanguageVersion
	{
		get;
		private set;
	}

	private void Awake()
	{
		if (!loaded)
		{
			StartCoroutine(DelayForceUpdate());
			RemoteSettings.Updated += HandleRemoteUpdate;
		}
	}

	private IEnumerator DelayForceUpdate()
	{
		yield return new WaitForSeconds(3f);
		if (!loaded)
		{
			HandleRemoteUpdate();
		}
	}

	private void HandleRemoteUpdate()
	{
		BallSpeed = UpdateFloat("BallSpeed", DefaultBallSpeed);
		FingerSensitivity = UpdateFloat("FingerSensitivity", DefaultFingerSensitivity);
		FingerSpeed = UpdateFloat("FingerSpeed", DefaultFingerSpeed);
		CammeraFollowSpeed = UpdateFloat("CameraFollowSpeed", DefaultCammeraFollowSpeed);
		JumpHeight = UpdateFloat("JumpHeight", DefaultJumpHeight);
		Block_MaxDistance = UpdateFloat("Block_MaxDistance", DefaultBlock_MaxDistance);
		Block_Span = UpdateFloat("Block_Span", DefaultBlock_Span);
		FixLatency = UpdateFloat("FixLatency_Superpowered", DefaultFixLatency);
		UpSpeedMax = UpdateFloat("UpSpeedMax", DefaultUpSpeedMax);
		Block_DeltaSize = UpdateFloat("Block_DeltaSize", DefaultBlock_DeltaSize);
		Block_PerfectSize = UpdateFloat("Block_PerfectSize", DefaultBlock_PerfectSize);
		AdsTime = DefaultAdsTime * 60f;
		Block_MaxSize = UpdateFloat("Block_MaxSize", DefaultBlock_MaxSize);
		Block_MinSize = UpdateFloat("Block_MinSize", DefaultBlock_MinSize);
		Block_Sag = UpdateFloat("Block_Sag", DefaultBlock_Sag);
		Block_MovingTime = UpdateFloat("Block_MovingTime", DefaultBlock_MovingTime);
		PushNewSongDate = UpdateFloat("PushNewSongDate", DefaultPushNewSongDate);
		Block_Total = UpdateInt("Block_Total", DefaultBlock_Total);
		UpSpeedScore = UpdateInt("UpSpeedFromScore", DefaultUpSpeedScore);
		MaxContinue = UpdateInt("ContinueMax", DefaultMaxContinue);
		Block_EndlessMovingOnly = UpdateInt("Block_EndlessMovingOnly", DefaultBlock_EndlessMovingOnly);
		Video_LocalSong = UpdateInt("Video_LocalSong", DefaultVideo_LocalSong);
		EnableCBCrosspromotion = (UpdateInt("EnableCBCrosspromotion", DefaultEnableCBCrosspromotion) == 1);
		EnablePokkt = (UpdateInt("EnablePokkt", DefaultEnablePokkt) == 1);
		BallPrice = UpdateInt("PriceBall", DefaultBallPrice);
		ThemePrice = UpdateInt("PriceTheme", DefaultThemePrice);
		Tutorial_AdsStar = UpdateInt("Tutorial_AdsStar", DefaultTutorial_AdsStar);
		Block_PerfectTrigger = UpdateInt("Block_PerfectTrigger", DefaultBlock_PerfectTrigger);
		Block_BadTrigger = UpdateInt("Block_BadTrigger", DefaultBlock_BadTrigger);
		UnlockAllSongs = UpdateInt("UnlockAllSongs", DefaultUnlockAllSongs);
		DiamondsRate = UpdateInt("DiamondsRate", DefaultDiamondsRate);
		StarBonus = UpdateInt("StarBonus", DefaultStarBonus);
		BurstMarketing = UpdateInt("BurstMarketing", DefaultBurstMarketing);
		DefaultTheme = UpdateInt("DefaultThemeNew", DefaultDefaultTheme) - 1;
		DiamondsVideoBonus = UpdateInt("DiamondsVideoBonus", DefaultDiamondsVideoBonus);
		ScoreToRate = UpdateInt("RateAtScore", DefaultScoreToRate);
		PlayCountToRate = UpdateInt("RateAtPlayCount", DefaultPlayCountToRate);
		Unlock_NewChallenge = UpdateInt("Unlock_NewChallenge", DefaultUnlock_NewChallenge);
		//ConfigPath = UpdateString("ConfigPathNew2", DefaultConfigPath);
		ConfigPath = UpdateString("ConfigPathNew2", "http://undefinedemail47.byethost4.com/rush_music/config/songconfig.csv");
		//DataPath = UpdateString("DataPath", DefaultDataPath);
		DataPath = UpdateString("DataPath", "http://undefinedemail47.byethost4.com/rush_music/music/");
		DataVersion = UpdateString("DataVersion", DefaultDataVersion);
		LanguageVersion = UpdateString("LanguageVersion", DefaultLanguageVersion);
		Distance_Interval = UpdateInt("Distance_Interval", DefaultDistance_Interval);
		Distance_MinMax1 = UpdateStringFloat2("Distance_MinMax1", DefaultDistance_MinMax1);
		Distance_MinMax2 = UpdateStringFloat2("Distance_MinMax2", DefaultDistance_MinMax2);
		Distance_MinMax3 = UpdateStringFloat2("Distance_MinMax3", DefaultDistance_MinMax3);
		Distance_MinMax4 = UpdateStringFloat2("Distance_MinMax4", DefaultDistance_MinMax4);
		AdsManager.instance.allowBanner = (UpdateInt("ActiveBanner", DefaultActiveBanner) == 1);
		loaded = true;
	}

	private float[] UpdateStringFloat2(string key, string defaultValue)
	{
		string[] array = UpdateString(key, defaultValue).Split(':');
		try
		{
			return new float[2]
			{
				int.Parse(array[0]),
				int.Parse(array[1])
			};
		}
		catch
		{
			return new float[2]
			{
				1f,
				4f
			};
		}
	}

	private string UpdateString(string key, string defaultValue)
	{
		string @string = RemoteSettings.GetString(key, PlayerPrefs.GetString(key, defaultValue));
		PlayerPrefs.SetString(key, @string);
		return @string;
	}

	private int UpdateInt(string key, int defaultValue)
	{
		int @int = RemoteSettings.GetInt(key, PlayerPrefs.GetInt(key, defaultValue));
		PlayerPrefs.SetInt(key, @int);
		return @int;
	}

	private float UpdateFloat(string key, float defaultValue)
	{
		float @float = RemoteSettings.GetFloat(key, PlayerPrefs.GetFloat(key, defaultValue));
		PlayerPrefs.SetFloat(key, @float);
		return @float;
	}
}
