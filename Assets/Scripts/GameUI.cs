using System.Collections;
using UnityEngine;

public class GameUI : MonoBehaviour
{
	public HandEffect handEffect;

	private void OnEnable()
	{
		Configuration.instance.SetCurrentLocation(LOCATION_NAME.PREPARE_GAME);
		StartCoroutine(DelayPlay());
	}

	private IEnumerator DelayPlay()
	{
		yield return new WaitForSeconds(0.1f);
		handEffect.left.Play();
		handEffect.right.Play();
	}

	private void OnDisable()
	{
		handEffect.left.Stop();
		handEffect.right.Stop();
		Configuration.instance.SetCurrentLocation(LOCATION_NAME.PLAY_GAME);
	}
}
