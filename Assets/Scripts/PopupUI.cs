using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class PopupUI : MonoBehaviour
{
	public PopUpAnimate animate;

	public Image bg;

	public bool destroyOnClose;

	private float moveY = 820f;

	private void OnEnable()
	{
		StartCoroutine(ShowPopup());
	}

	private IEnumerator ShowPopup()
	{
		Transform window = base.transform.Find("Window");
		if (animate == PopUpAnimate.SLIDEDOWN)
		{
			if (window != null)
			{
				bg.canvasRenderer.SetAlpha(0f);
				bg.CrossFadeAlpha(1f, 0.3f, ignoreTimeScale: false);
				Vector3 localPosition = window.transform.localPosition;
				localPosition.y = moveY;
				window.transform.localPosition = localPosition;
				iTween.MoveTo(window.gameObject, iTween.Hash("y", 0, "time", 0.3f, "delay", 0f, "easetype", "easeOutExpo", "islocal", true));
			}
			else
			{
				Vector3 localPosition2 = base.gameObject.transform.localPosition;
				localPosition2.y = moveY;
				base.gameObject.transform.localPosition = localPosition2;
				iTween.MoveTo(base.gameObject, iTween.Hash("y", 0, "time", 0.3f, "delay", 0f, "easetype", "easeOutExpo", "islocal", true));
			}
		}
		else
		{
			bg.CrossFadeAlpha(1f, 0.3f, ignoreTimeScale: false);
			yield return new WaitForSeconds(0.3f);
		}
		yield return null;
	}

	public void Close()
	{
		SoundManager.instance.PlayGameButton();
		Configuration.instance.SetCurrentLocation(LOCATION_NAME.POPUP_CLOSE, isPopup: true);
		StartCoroutine(HidePopup());
	}

	public IEnumerator HidePopup()
	{
		Transform window = base.transform.Find("Window");
		if (animate == PopUpAnimate.SLIDEDOWN)
		{
			if (window != null)
			{
				bg.CrossFadeAlpha(0f, 0.3f, ignoreTimeScale: false);
				iTween.MoveTo(window.gameObject, iTween.Hash("y", moveY, "time", 0.3f, "delay", 0f, "easetype", "easeInExpo", "islocal", true));
				yield return new WaitForSeconds(0.3f);
			}
			else
			{
				iTween.MoveTo(base.gameObject, iTween.Hash("y", moveY, "time", 0.3f, "delay", 0f, "easetype", "easeInExpo", "islocal", true));
				yield return new WaitForSeconds(0.3f);
			}
		}
		else
		{
			if (window != null)
			{
				window.gameObject.SetActive(value: false);
			}
			bg.CrossFadeAlpha(0f, 0.3f, ignoreTimeScale: false);
			yield return new WaitForSeconds(0.3f);
		}
		if (destroyOnClose)
		{
			UnityEngine.Object.Destroy(base.gameObject);
		}
		else
		{
			base.gameObject.SetActive(value: false);
		}
	}
}
