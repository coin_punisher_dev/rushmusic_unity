using UnityEngine;
using UnityEngine.UI;

public class AchievementPopUp : PopupUI
{
	public Text diamondText;

	public Text achievementName;

	private AchievementObj obj;

	private void Awake()
	{
		Configuration.instance.SetCurrentLocation(LOCATION_NAME.ACHIEVEMENT_POPUP, isPopup: true);
	}

	public void SetObj(AchievementObj obj)
	{
		this.obj = obj;
		diamondText.text = obj.diamonds.ToString();
		achievementName.text = LocalizationManager.instance.GetLocalizedValue(obj.type.ToString());
		Physics.gravity = new Vector3(0f, -9.8f, 0f);
	}

	public void Claim()
	{
		SoundManager.instance.PlayCoins();
		obj.SetStatus(AchievementStatus.RECEIVED);
		TopBar.instance.UpdateDiamond(obj.diamonds, Income_Type.ACHIEVEMENT);
		Close();
	}
}
