using System;
using UnityEngine;
#if ENABLE_IAP
using UnityEngine.Purchasing;
#endif

public class InAppPurchaser : MonoBehaviour
#if ENABLE_IAP
    , IStoreListener
#endif
{
	public static InAppPurchaser instance;

#if ENABLE_IAP
	private static IStoreController m_StoreController;

	private static IExtensionProvider m_StoreExtensionProvider;
#endif

	public static string kProductIDP1 = "removeads";

	public static string kProductIDP2 = "diamond1";

	public static string kProductIDP3 = "diamond2";

	public static string kProductIDP4 = "diamond3";

	public static string kProductIDP5 = "diamond4";

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
			UnityEngine.Object.DontDestroyOnLoad(base.gameObject);
		}
		else if (instance != null)
		{
			UnityEngine.Object.Destroy(base.gameObject);
		}
	}

	private void Start()
	{
#if ENABLE_IAP
		if (m_StoreController == null)
		{
			InitializePurchasing();
		}
#endif
	}

	public void InitializePurchasing()
	{
		if (!IsInitialized())
		{
#if ENABLE_IAP
			ConfigurationBuilder configurationBuilder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
			configurationBuilder.AddProduct(kProductIDP1, ProductType.NonConsumable);
			configurationBuilder.AddProduct(kProductIDP2, ProductType.Consumable);
			configurationBuilder.AddProduct(kProductIDP3, ProductType.Consumable);
			configurationBuilder.AddProduct(kProductIDP4, ProductType.Consumable);
			configurationBuilder.AddProduct(kProductIDP5, ProductType.Consumable);
			UnityPurchasing.Initialize(this, configurationBuilder);
#endif
		}
	}

	private bool IsInitialized()
	{
#if ENABLE_IAP
		return m_StoreController != null && m_StoreExtensionProvider != null;
#endif
        return false;
	}

	public void BuyProduct(int package)
	{
#if ENABLE_IAP
		Product product = GetProduct(package);
		if (product != null)
		{
			m_StoreController.InitiatePurchase(product);
		}
#endif
	}

	public string GetProductID(int package)
	{
		string text = null;
		switch (package)
		{
		case 1:
			return kProductIDP1;
		case 2:
			return kProductIDP2;
		case 3:
			return kProductIDP3;
		case 4:
			return kProductIDP4;
		default:
			return kProductIDP5;
		}
	}

#if ENABLE_IAP
	public Product GetProduct(int package)
	{
		string productID = GetProductID(package);
		if (IsInitialized())
		{
			Product product = m_StoreController.products.WithID(productID);
			if (product != null && product.availableToPurchase)
			{
				return product;
			}
		}
		return null;
	}
#endif

	public void RestorePurchases()
	{
		if (IsInitialized() && (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.OSXPlayer))
		{
#if ENABLE_IAP
			IAppleExtensions extension = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
			extension.RestoreTransactions(delegate
			{
			});
#endif
		}
	}

#if ENABLE_IAP
	public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
	{
		m_StoreController = controller;
		m_StoreExtensionProvider = extensions;
	}
#endif

#if ENABLE_IAP
	public void OnInitializeFailed(InitializationFailureReason error)
	{
	}
#endif

#if ENABLE_IAP
	public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
	{
		if (string.Equals(args.purchasedProduct.definition.id, kProductIDP1, StringComparison.Ordinal))
		{
			Shop.instance.CompletedPurchase(1);
		}
		else if (string.Equals(args.purchasedProduct.definition.id, kProductIDP2, StringComparison.Ordinal))
		{
			Shop.instance.CompletedPurchase(2);
		}
		else if (string.Equals(args.purchasedProduct.definition.id, kProductIDP3, StringComparison.Ordinal))
		{
			Shop.instance.CompletedPurchase(3);
		}
		else if (string.Equals(args.purchasedProduct.definition.id, kProductIDP4, StringComparison.Ordinal))
		{
			Shop.instance.CompletedPurchase(4);
		}
		else if (string.Equals(args.purchasedProduct.definition.id, kProductIDP5, StringComparison.Ordinal))
		{
			Shop.instance.CompletedPurchase(5);
		}
		return PurchaseProcessingResult.Complete;
	}

	public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
	{
	}
#endif
}
