//using com.F4A.MobileThird;
using System.Collections;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public string gameButton = "click";

	public string gameCompleted = "gameCompleted";

	public string star = "star";

	public string die = "die";

	public string select = "select";

	public string coins = "coins";

	public string impactExtend = "impactExtend";

	public string buy = "buy";

	public string checkPoint = "checkPoint";

	public string diamond = "diamond";

	public float soundVolume = 0.2f;

	private int channel;

	public static SoundManager instance;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
			Object.DontDestroyOnLoad(base.gameObject);
		}
		else if (instance != null)
		{
			UnityEngine.Object.Destroy(base.gameObject);
		}
	}

	private void Start()
	{
		StartCoroutine(DelayLoad());
	}

	private IEnumerator DelayLoad()
	{
		yield return new WaitForSeconds(2f);
        //@TODO SoundManager

        //AudioPlayer.Load(gameButton);
        //AudioPlayer.Load(gameCompleted);
        //AudioPlayer.Load(star);
        //AudioPlayer.Load(die);
        //AudioPlayer.Load(select);
        //AudioPlayer.Load(coins);
        //AudioPlayer.Load(impactExtend);
        //AudioPlayer.Load(buy);
        //AudioPlayer.Load(checkPoint);
        //AudioPlayer.Load(diamond);
    }

    public void PlaySingleSound(string type)
	{
		if (Configuration.instance.SoundIsOn())
		{
            //@TODO SoundManager

            //if (type == coins)
            //{
            //	soundVolume = 0.5f;
            //}
            //else
            //{
            //	soundVolume = 0.2f;
            //}
            //AudioPlayer.Play(channel, type, soundVolume);
            //channel++;
            //if (channel == 4)
            //{
            //	channel = 0;
            //}

            //AudioManager.Instance.PlaySfx(type);
		}
	}

	public void PlayGameButton()
	{
		PlaySingleSound(gameButton);
		PlayerPrefs.SetString("klikStatus","0");
	}

	public void PlaySelect()
	{
		PlaySingleSound(select);
	}

	public void PlayGameCompleted()
	{
		PlaySingleSound(gameCompleted);
	}

	public void PlayDie()
	{
		PlaySingleSound(die);
	}

	public void PlayStar()
	{
		PlaySingleSound(star);
	}

	public void PlayCheckPoint()
	{
		PlaySingleSound(checkPoint);
	}

	public void PlayCollect()
	{
		PlaySingleSound(coins);
	}

	public void PlayCoins()
	{
		PlaySingleSound(coins);
	}

	public void PlayImpactExtend()
	{
		PlaySingleSound(impactExtend);
	}

	public void PlayBuy()
	{
		PlaySingleSound(buy);
	}

	public void PlayDiamond()
	{
		PlaySingleSound(diamond);
	}
}
