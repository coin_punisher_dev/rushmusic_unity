using System.Runtime.InteropServices;

[StructLayout(LayoutKind.Sequential, Size = 1)]
public struct CONFIG_STRING
{
	public const string Sound = "sound";

	public const string Music = "music";

	public const string NoAds = "noads";

	public const string Intro = "intro";

	public const string DataVersion = "data_version";

	public const string LanguageVersion = "language_version";

	public const string LanguageID = "language_id";

	public const string HourlyGiftTime = "hourlygift_time";

	public const string DailyGiftDate = "dailygift_date";

	public const string DailyGiftNumber = "dailygift_number";

	public const string WrongPlay = "WrongPlay";

	public const string Bonus_Login = "bonus_fb_login";

	public const string Bonus_Sharing = "bonus_sharing";

	public const string CurrentSong = "current_song";

	public const string SongVersion = "v";

	public const string BestScore = "bestscore";

	public const string BestStars = "beststars";

	public const string Diamonds = "diamonds";

	public const string RateLater = "RateLater";

	public const string PlayCount = "PlayCount";

	public const string Balls = "balls";

	public const string SelectedBall = "selected_ball";

	public const string Themes = "themes";

	public const string SelectedTheme = "selected_theme";

	public const string OpenedSongTotal = "opened_song_total";

	public const string Retry = "retry";

	public const string Revive = "revive";

	public const string IsTop1 = "istop1";

	public const string PerfectCount = "perfect_count";

	public const string ABTest = "ABTest_op";

	public const string FreeAddSong = "FreeAddSong";

	public const string DBKey_LeaderBoards = "LeaderBoards";

	public const string DBKey_Total = "Total";

	public const string DBKey_Users = "Users";

	public const string DBKey_Links = "Links";

	public const string CurrentFolder = "CurrentFolder";

	public const string ThemeArray = "ThemeArray";

	public const string EndLessMode = "ENDLESS MODE";

	public const string NewHighScore = "NEW HIGHSCORE";

	public const string SongUnlockCount = "SongUnlockCount";

	public const string TutorialAd = "TutorialAd";

	public const string Session_ID = "Session_ID";

	public const string Session_Duration = "Session_Duration";

	public const string Session_Play = "Session_Play";

	public const string first_open_time = "first_open_time";
}
