using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class FileSelector : PopupUI
{
	public GameObject itemFile;

	public GameObject itemFolder;

	public GameObject upButton;

	public GameObject confirmObj;

	private List<GameObject> buttonList;

	public static FileSelector instance;

	public string currentFolder;

	private string[] mainFolder;

	private float positionY;

	public RectTransform container;

	public Item song;

	private void Awake()
	{
		instance = this;
		Configuration.instance.SetCurrentLocation(LOCATION_NAME.SELECT_LOCALSONG, isPopup: true);
	}

	private void Start()
	{
		Util.UpdateBannerContent(base.transform);
		currentFolder = GetCurrentFolder();
		if (!string.IsNullOrEmpty(currentFolder) && !Directory.Exists(currentFolder))
		{
			SetCurrentFolder(null);
			BuildList();
			return;
		}
		buttonList = new List<GameObject>();
        var currentDirectory = Directory.GetCurrentDirectory();
		//string androidExternalStoragePath = Util.GetAndroidExternalStoragePath();
        string androidExternalStoragePath = currentDirectory + "sdcard" + ";" + currentDirectory + "storage";
        Debug.LogError("TilesHop FileSelector.Start androidExternalStoragePath:" + androidExternalStoragePath);
        //AndroidNativeFunctions.ShowToast("TilesHop FileSelector.Start androidExternalStoragePath: " + androidExternalStoragePath);
        if (!string.IsNullOrEmpty(androidExternalStoragePath))
		{
			mainFolder = androidExternalStoragePath.Split(';');
			BuildList();
		}
	}

	public void BuildList()
	{
		if (container == null)
		{
			return;
		}
		container.anchoredPosition = Vector2.zero;
		positionY = 0f;
		if (!string.IsNullOrEmpty(currentFolder))
		{
			upButton.SetActive(value: true);
		}
		else
		{
			upButton.SetActive(value: false);
			positionY -= 98f;
		}
		foreach (GameObject button in buttonList)
		{
			UnityEngine.Object.Destroy(button);
		}
		buttonList = new List<GameObject>();
		if (string.IsNullOrEmpty(currentFolder) && mainFolder.Length == 1)
		{
			SetCurrentFolder(mainFolder[0]);
		}
		if (string.IsNullOrEmpty(currentFolder))
		{
			for (int i = 0; i < mainFolder.Length; i++)
			{
				if (Util.CanReadDirectory(mainFolder[i]))
				{
					AddButton("Storage " + i.ToString(), mainFolder[i], FileSelectorItem.ITEM_TYPE.FOLDER);
				}
			}
		}
		else if (!Directory.Exists(currentFolder))
		{
			Util.ShowMessage(LocalizationManager.instance.GetLocalizedValue("FAILED_LOAD_FOLDER") + currentFolder);
		}
		else
		{
			try
			{
				DirectoryInfo directoryInfo = new DirectoryInfo(currentFolder);
				DirectoryInfo[] directories = directoryInfo.GetDirectories();
				for (int j = 0; j < directories.Length; j++)
				{
					if (Util.CanReadDirectory(directories[j].FullName))
					{
                        string nameDirectory = directories[j].Name.ToLower();
                        //AddButton(directories[j].Name, directories[j].FullName, FileSelectorItem.ITEM_TYPE.FOLDER);
                        if (currentFolder.Equals("/") || currentFolder.Equals(@"\"))
                        {
                            if (nameDirectory.Equals("sdcard") || nameDirectory.Equals("storage"))
                            {
                                Debug.LogError("TilesHop folder:" + directories[j].FullName);
                                AddButton(directories[j].Name, directories[j].FullName, FileSelectorItem.ITEM_TYPE.FOLDER);
                            }
                        }
                        else
                        {
                            AddButton(directories[j].Name, directories[j].FullName, FileSelectorItem.ITEM_TYPE.FOLDER);
                        }
                    }
                }
				FileInfo[] files = directoryInfo.GetFiles();
				for (int k = 0; k < files.Length; k++)
				{
					if (files[k].Extension.Equals(".mp3") && !RemoteFilesData.instance.localSongList.ContainsKey(files[k].FullName))
					{
						AddButton(files[k].Name, files[k].FullName, FileSelectorItem.ITEM_TYPE.FILE);
                    }
                }
				container.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, positionY + 300f);
			}
			catch (Exception ex)
			{
				Util.ShowMessage(ex.ToString());
			}
		}
	}

	private void AddButton(string nameItem, string patItem, FileSelectorItem.ITEM_TYPE type)
	{
		nameItem = Util.FileNameToTitle(nameItem);
		GameObject item = null;
		item = ((type != 0) ? UnityEngine.Object.Instantiate(itemFolder) : UnityEngine.Object.Instantiate(itemFile));
		item.SetActive(value: true);
		item.GetComponent<FileSelectorItem>().SetValues(nameItem, patItem, type);
		item.transform.SetParent(itemFile.transform.parent, worldPositionStays: false);
		StartCoroutine(DelayUpdate(item, Vector3.down * positionY));
		buttonList.Add(item);
		positionY += 100f;
	}

	private IEnumerator DelayUpdate(GameObject item, Vector3 localPosition)
	{
		yield return null;
		if (item != null)
		{
			item.transform.localPosition += localPosition;
		}
	}

	public void Up_Click()
	{
		SoundManager.instance.PlayGameButton();
		if (string.IsNullOrEmpty(currentFolder))
		{
			return;
		}
		for (int i = 0; i < mainFolder.Length; i++)
		{
			if (currentFolder == mainFolder[i])
			{
				SetCurrentFolder(null);
				BuildList();
				return;
			}
		}
		string parentFolder = Util.GetParentFolder(currentFolder);
		if (parentFolder != null && Util.CanReadDirectory(parentFolder))
		{
			SetCurrentFolder(parentFolder);
			for (int j = 0; j < mainFolder.Length; j++)
			{
				if (currentFolder == mainFolder[j])
				{
					SetCurrentFolder(null);
					break;
				}
			}
			BuildList();
		}
		else
		{
			SetCurrentFolder(null);
			BuildList();
		}
	}

	public void SetCurrentFolder(string folder)
	{
		PlayerPrefs.SetString("CurrentFolder", folder);
		currentFolder = folder;
	}

	public string GetCurrentFolder()
	{
		return PlayerPrefs.GetString("CurrentFolder", null);
	}

	public void ImportConfirm(Item songImport)
	{
		SoundManager.instance.PlayGameButton();
		this.song = songImport;
		confirmObj.SetActive(value: true);
		confirmObj.transform.Find("Text").GetComponent<Text>().text = LocalizationManager.instance.GetLocalizedValue("ASK_IMPORT") + "\n<color='#00E031'>" + songImport.name + "</color>";
		confirmObj.transform.Find("Buttons").gameObject.SetActive(value: true);
	}

	public void Confirm_Yes()
	{
		SoundManager.instance.PlayGameButton();
		if (SongList.instance.buttonType == 1)
		{
			Configuration.SetFreeAddSongOff();
		}
		else if (SongList.instance.buttonType == 3)
		{
			TopBar.instance.UpdateDiamond(-SongList.localItemPrice);
		}
		AnalyticHelper.LocalSongAdd(SongList.instance.buttonType);
		confirmObj.transform.Find("Buttons").gameObject.SetActive(value: false);
		confirmObj.transform.Find("Text").GetComponent<Text>().text = LocalizationManager.instance.GetLocalizedValue("IMPORTING");
		StartCoroutine(Importing(song));
	}

	public void Confirm_No()
	{
		SoundManager.instance.PlayGameButton();
		confirmObj.SetActive(value: false);
	}

	private IEnumerator Importing(Item songItem)
	{
		yield return null;
		float[] bpm = SuperpoweredSDK.instance.GetBPM(songItem.path);
		yield return null;
		songItem.UpdateBPM(bpm[0], bpm[1]);
		RemoteFilesData.instance.AddSongLocal(songItem);
		SongList.instance.BuidListNew();
		yield return null;
		confirmObj.transform.Find("Text").GetComponent<Text>().text = LocalizationManager.instance.GetLocalizedValue("SUCCESSFULLY");
		yield return new WaitForSeconds(0.5f);
		Close();
	}

	private string GetAndroidContextExternalFilesDir()
	{
		string result = string.Empty;
		if (Application.platform == RuntimePlatform.Android)
		{
			try
			{
				using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
				{
					using (AndroidJavaObject androidJavaObject = androidJavaClass.GetStatic<AndroidJavaObject>("currentActivity"))
					{
						result = androidJavaObject.Get<AndroidJavaObject>("getExternalFilesDirs").Call<string>("getAbsolutePath", new object[0]);
						return result;
					}
				}
			}
			catch (Exception ex)
			{
				UnityEngine.Debug.LogWarning("Error fetching native Android external storage dir: " + ex.Message);
				return result;
			}
		}
		return result;
	}

	private string GetAndroidContextInternalFilesDir()
	{
		string result = string.Empty;
		if (Application.platform == RuntimePlatform.Android)
		{
			try
			{
				using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
				{
					using (AndroidJavaObject androidJavaObject = androidJavaClass.GetStatic<AndroidJavaObject>("currentActivity"))
					{
						result = androidJavaObject.Call<AndroidJavaObject>("getFilesDir", new object[0]).Call<string>("getAbsolutePath", new object[0]);
						return result;
					}
				}
			}
			catch (Exception ex)
			{
				UnityEngine.Debug.LogWarning("Error fetching native Android internal storage dir: " + ex.Message);
				return result;
			}
		}
		return result;
	}
}
