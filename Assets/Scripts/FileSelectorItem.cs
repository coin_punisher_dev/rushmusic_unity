using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class FileSelectorItem : MonoBehaviour
{
	public enum ITEM_TYPE
	{
		FILE,
		FOLDER
	}

	public Text title;

	private string path;

	private ITEM_TYPE type;

	public void SetValues(string name, string path, ITEM_TYPE type)
	{
		title.text = name;
		this.type = type;
		this.path = path;
		if (Configuration.instance.isDebug && type == ITEM_TYPE.FOLDER)
		{
			base.transform.Find("Add").gameObject.SetActive(value: true);
		}
	}

	public void Item_Click()
	{
		SoundManager.instance.PlayGameButton();
		if (type == ITEM_TYPE.FILE)
		{
			FileSelector.instance.ImportConfirm(new Item(path, title.text, 100f, 1f, 0, SONGTYPE.OPEN.ToString(), 0));
			return;
		}
		FileSelector.instance.SetCurrentFolder(path);
		FileSelector.instance.BuildList();
	}

	public void Folder_Add()
	{
		SongList.instance.ClearAllItems();
		DirectoryInfo directoryInfo = new DirectoryInfo(path);
		FileInfo[] files = directoryInfo.GetFiles();
		for (int i = 0; i < files.Length; i++)
		{
			string[] array = files[i].Name.Split('.');
			string text = array[array.Length - 1];
			if (text.ToLower() == "mp3")
			{
				Item song = new Item(files[i].FullName, files[i].Name, 100f, 0f, 0, SONGTYPE.OPEN.ToString(), 0);
				RemoteFilesData.instance.AddSongLocal(song);
			}
		}
		DirectoryInfo[] directories = directoryInfo.GetDirectories();
		for (int j = 0; j < directories.Length; j++)
		{
			DirectoryInfo directoryInfo2 = new DirectoryInfo(directories[j].FullName);
			FileInfo[] files2 = directoryInfo2.GetFiles();
			for (int k = 0; k < files2.Length; k++)
			{
				string[] array2 = files2[k].Name.Split('.');
				string text2 = array2[array2.Length - 1];
				if (text2.ToLower() == "mp3")
				{
					Item song2 = new Item(files2[k].FullName, files2[k].Name, 100f, 0f, 0, SONGTYPE.OPEN.ToString(), 0);
					RemoteFilesData.instance.AddSongLocal(song2);
				}
			}
		}
		SongList.instance.BuidListNew();
		FileSelector.instance.Close();
	}
}
