using System.IO;
using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.Advertisements;

public class SongItem : MonoBehaviour
{
	public string songID = string.Empty;

	public Text nameText;

	public Text bestScoreText;

	public Text priceText;

	public GameObject rankPanel;

	public GameObject star1;

	public GameObject star2;

	public GameObject star3;

	public SONGTYPE type = SONGTYPE.LOCK;

	private int price;

	public Animation diskAnim;

	public static Animation diskAnimPlaying;

	public static GameObject loadingImg;

	private CoreUser.FriendLoadedHandler handler;

	private void Awake()
	{
		if (rankPanel != null && CoreUser.instance != null)
		{
			Subscribe(CoreUser.instance);
			//FriendLoaded();
		}
		PlayerPrefs.SetString("klikStatus","0");
	}

	private void FriendLoaded()
	{
		if (type == SONGTYPE.OPEN)
		{
			long num = 0L;
			if (CoreUser.instance.loadedFriend && CoreData.GetBestScore(songID) > 0)
			{
				num = CoreUser.instance.user.GetMyRank(Util.SongToBoardId(songID));
			}
			rankPanel.transform.Find("Text").GetComponent<Text>().text = ((num <= 0) ? "--" : num.ToString());
		}
	}

	private void OnDestroy()
	{
		if (rankPanel != null && CoreUser.instance != null)
		{
			CoreUser.instance.onFriendLoaded -= handler;
		}
	}

	public void Subscribe(CoreUser coreUser)
	{
		handler = FriendLoaded;
		coreUser.onFriendLoaded += handler;
	}

	public void OnEnable()
	{
		if (diskAnimPlaying != null)
		{
			diskAnimPlaying.Play();
		}
		else if (songID == CoreData.instance.GetCurrentSong().path)
		{
			diskAnim.Play();
			diskAnimPlaying = diskAnim;
			if (diskAnim != null && diskAnim.transform.parent.Find("Pause") != null)
			{
				diskAnim.transform.parent.Find("Pause").gameObject.SetActive(value: true);
				diskAnim.transform.parent.Find("Play").gameObject.SetActive(value: false);
			}
		}
		if (type == SONGTYPE.OPEN)
		{
			int bestScore = CoreData.GetBestScore(songID);
			bestScoreText.text = LocalizationManager.instance.GetLocalizedValue("BEST_X").Replace("{0}", (bestScore <= 0) ? "--" : bestScore.ToString());
		}
	}
	
	public void ItemClick()
	{
		PlayerPrefs.SetString("statusVAds", "0");
		switch (type)
		{
		case SONGTYPE.VIDEO:
			SoundManager.instance.PlayGameButton();
			AdsManager.instance.ShowRewardAds(VIDEOREWARD.UNLOCKSONG, base.transform);
			break;
		case SONGTYPE.OPEN:
			if (RemoteFilesData.instance.IsLocalSong(songID) && !File.Exists(RemoteFilesData.instance.localSongList[songID].path))
			{
				Util.ShowMessage(LocalizationManager.instance.GetLocalizedValue("MISSING_SONG_FILE") + "\n<color='#00E031'>" + RemoteFilesData.instance.localSongList[songID].path + "</color>");
				break;
			}
			Util.GoToGamePlay(songID);
			HomeManager.instance.groundMusic.StopMusic();
			break;
		case SONGTYPE.PUSH:
			SoundManager.instance.PlayGameButton();
			if (CoreData.GetBestScore(songID) == 0)
			{
				AnalyticHelper.FireString("PushNewSong_Try");
				Util.GoToGamePlay(songID);
				UnlockPushSong.songid = songID;
			}
			else
			{
				AdsManager.instance.ShowRewardAds(VIDEOREWARD.UNLOCKSONG, base.transform);
			}
			break;
		default:
			if (CoreData.instance.GetDiamonds() >= price)
			{
				TopBar.instance.UpdateDiamond(-price);
				SongList.instance.OpenSong(base.transform);
			}
			else
			{
				Util.ShowNeedMore(price - CoreData.instance.GetDiamonds());
			}
			break;
		}
	}

	public void ItemPlaySong()
	{
		if (loadingImg != null)
		{
			loadingImg.SetActive(value: false);
		}
		if (diskAnim.transform.parent.Find("Play").gameObject.activeSelf)
		{
			/*
			loadingImg = diskAnim.transform.parent.Find("Disk/Loading").gameObject;
			HomeManager.instance.groundMusic.PlayMusicOnline(songID);
			diskAnim.Play();
			diskAnim.transform.parent.Find("Pause").gameObject.SetActive(value: true);
			diskAnim.transform.parent.Find("Play").gameObject.SetActive(value: false);
			if (diskAnimPlaying != null)
			{
				diskAnimPlaying.Stop();
				if (diskAnimPlaying.transform.parent.Find("Pause") != null)
				{
					diskAnimPlaying.transform.parent.Find("Pause").gameObject.SetActive(value: false);
					diskAnimPlaying.transform.parent.Find("Play").gameObject.SetActive(value: true);
				}
			}
			diskAnimPlaying = diskAnim;*/
		}
		else
		{
			/*
			HomeManager.instance.groundMusic.StopMusic();
			diskAnim.Stop();
			diskAnimPlaying = null;
			diskAnim.transform.parent.Find("Pause").gameObject.SetActive(value: false);
			diskAnim.transform.parent.Find("Play").gameObject.SetActive(value: true);
			*/
		}
	}

	public void SetSong(SONGTYPE songType, Item item)
	{
		type = songType;
		songID = item.path;
		nameText.text = item.name;
		switch (songType)
		{
		case SONGTYPE.LOCK:
			price = item.diamonds;
			priceText.text = price.ToString();
			break;
		case SONGTYPE.OPEN:
		{
			int bestStars = CoreData.GetBestStars(songID);
			star1.SetActive(value: false);
			star2.SetActive(value: false);
			star3.SetActive(value: false);
			if (bestStars >= 1)
			{
				star1.SetActive(value: true);
			}
			if (bestStars >= 2)
			{
				star2.SetActive(value: true);
			}
			if (bestStars >= 3)
			{
				star3.SetActive(value: true);
			}
			break;
		}
		case SONGTYPE.PUSH:
		{
			int bestScore = CoreData.GetBestScore(songID);
			if (bestScore > 0)
			{
				base.transform.Find("VideoButton").gameObject.SetActive(value: true);
			}
			else
			{
				base.transform.Find("TryButton").gameObject.SetActive(value: true);
			}
			break;
		}
		}
		OnEnable();
	}

	public void ShowBoard()
	{
		SoundManager.instance.PlayGameButton();
		LeaderBoardUI.instance.ShowBoard(Util.SongToBoardId(songID), CoreData.GetBestScore(songID));
	}

	public void ShowDetails_Click()
	{
		SoundManager.instance.PlayGameButton();
		GameObject gameObject = Util.ShowPopUp("SongDetails");
		gameObject.GetComponent<SongDetails>().SetSong(RemoteFilesData.instance.localSongList[songID]);
	}
}
