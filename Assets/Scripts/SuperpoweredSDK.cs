//#define ENABLE_SUPERPOWERED_SDK
//#define DISABLE_MUSIC

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class SuperpoweredSDK : MonoBehaviour
{
	public static SuperpoweredSDK instance;

#if ENABLE_SUPERPOWERED_SDK
	private AndroidJavaClass unityPlayer;

	private AndroidJavaObject activity;

	private AndroidJavaObject context;

	private AndroidJavaClass musicPlay;
#else
    [SerializeField]
    private AudioSource _musicSource;
    private bool _isLoop = false;
    private bool _isPlayAfterLoad = false;
    private string _songName = "";
    private string _path = "";
    private Dictionary<string, AudioClip> _dicAudioClipMusic = new Dictionary<string, AudioClip>();
#endif

    private const float VOLUME = 1f;

	private float currVolume = 1f;

	//[HideInInspector]
	public float musicLength;

	private bool isReady;

	private Coroutine coroutine;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
			Object.DontDestroyOnLoad(base.gameObject);
			InitInstance();
		}
		else if (instance != null)
		{
			UnityEngine.Object.Destroy(base.gameObject);
		}
	}

	private void InitInstance()
	{
#if ENABLE_SUPERPOWERED_SDK
		unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		activity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
        musicPlay = new AndroidJavaClass("com.inwave.superpowered.MusicPlay");
		musicPlay.CallStatic<string>("SetContext", new object[1]
		{
			activity
		});
#else
#if !DISABLE_MUSIC
        if (_musicSource) _musicSource = GetComponentInChildren<AudioSource>();
#endif
#endif
    }

    public void LoadMusic(string path, bool isPlayAfterLoad = false, bool isLoop = false)
	{
		isReady = false;
#if ENABLE_SUPERPOWERED_SDK
		musicLength = 0f;
		musicPlay.CallStatic<string>("onSuperpoweredLoadMusic", new object[3]
		{
			path,
			isPlayAfterLoad,
			isLoop
		});
#else
        _path = path;
        musicLength = Random.Range(10f, 15f);
        _isLoop = isLoop;
        _isPlayAfterLoad = isPlayAfterLoad;

#if !DISABLE_MUSIC
        if (_dicAudioClipMusic.ContainsKey(path) && _dicAudioClipMusic[path])
        {
            var clip = _dicAudioClipMusic[path];
            isReady = true;
            musicLength = clip.length;
            if (_isPlayAfterLoad) PlayMusic();
        }
        else
        {
            StartCoroutine(AsyncLoadMusic(path));
        }
#else
        isReady = true;
#endif
#endif
    }

    IEnumerator AsyncLoadMusic(string path)
    {
        /*UnityWebRequest webRequest = UnityWebRequestMultimedia.GetAudioClip("file://" + path, AudioType.MPEG);
        yield return webRequest.SendWebRequest();
        if (string.IsNullOrEmpty(webRequest.error))
        {
            AudioClip clip = DownloadHandlerAudioClip.GetContent(webRequest);
            if (clip)
            {
                _dicAudioClipMusic[path] = clip;
                musicLength = clip.length;
                isReady = true;
                if (_isPlayAfterLoad) PlayMusic();
            }
        }*/
		Debug.Log("play music on "+ path);
		var pathToArray = path.Split('/');
		var getName = pathToArray[pathToArray.Length-1].ToString().Split('.');
		Debug.Log(getName[0]);
		AudioClip clip = Resources.Load<AudioClip>("Sound/"+getName[0]);
		if (clip)
		{
			_dicAudioClipMusic[path] = clip;
			musicLength = clip.length;
			isReady = true;
			if (_isPlayAfterLoad) PlayMusic();
		}
		yield return "";
    }

    public void RemoveMusic(string path)
	{
	}

	private void SetReady(string ready)
	{
		if (ready == "1")
		{
			isReady = true;
		}
		else
		{
			isReady = false;
		}
	}

	public float[] GetBPM(string path)
	{
		string text = string.Empty;
#if ENABLE_SUPERPOWERED_SDK
		text = musicPlay.CallStatic<string>("onSuperpoweredGetBPM", new object[1]
		{
			path
		});
#endif
        return ParseBPM(text);
	}

    public float[] ParseBPM(string bpmStr)
    {
        float bpm = 100f;
        float startTime = 0f;
        if (!string.IsNullOrEmpty(bpmStr))
        {
            try
            {
                string[] array = bpmStr.Split('|');
                bpm = float.Parse(array[0]);
                float num3 = 60f / bpm;
                startTime = float.Parse(array[1]) / 1000f + 0.835f * num3;
                if (startTime > num3)
                {
                    startTime %= num3;
                }
            }
            catch (System.Exception ex)
            {
                Debug.Log("TilesHop SuperpoweredSDK.ParseBPM ex:" + ex);
            }
        }
        return new float[2]
        {
            bpm,
            startTime
        };
    }

    public string AndroidSdCardPath()
	{
#if ENABLE_SUPERPOWERED_SDK
		return musicPlay.CallStatic<string>("onSuperpoweredSDCardDir", new object[0]);
#endif
        return "";
	}

	public string GetDeviceCountry()
	{
        //@TODO GetDeviceCountry
#if ENABLE_SUPERPOWERED_SDK
        {
#if !UNITY_EDITOR && UNITY_ANDROID
		return musicPlay.CallStatic<string>("GetDeviceCountry", new object[0]);
#else
        return SystemInfo.deviceModel;
#endif
        }
#else
        return SystemInfo.deviceModel;
#endif
    }

	public void PlayMusic()
	{
#if ENABLE_SUPERPOWERED_SDK
		musicPlay.CallStatic<string>("onSuperpoweredPlay", new object[0]);
#else
#if !DISABLE_MUSIC
        _musicSource.Stop();
        if (_dicAudioClipMusic.ContainsKey(_path))
        {
            var clip = _dicAudioClipMusic[_path];
            if (clip)
            {
                _musicSource.clip = clip;
                _musicSource.loop = _isLoop;
                _musicSource.Play();
            }
        }
#endif
#endif
    }

    public void PauseMusic()
	{
#if ENABLE_SUPERPOWERED_SDK
#else
#if !DISABLE_MUSIC
        _musicSource.Pause();
#endif
#endif
    }

    public void SetSpeed(float speed)
	{
#if ENABLE_SUPERPOWERED_SDK
		musicPlay.CallStatic<string>("onSuperpoweredSetSpeed", new object[1]
		{
			speed
		});
#else
#if !DISABLE_MUSIC
        //AudioManager.Instance.SetPitchMusic(speed);
#endif
#endif
    }

    public void UpdateMusicLength()
	{
#if ENABLE_SUPERPOWERED_SDK
		musicLength = (float)musicPlay.CallStatic<int>("onSuperpoweredGetCurrentLength", new object[0]) / 1000f;
#else
#endif
    }

	public void SetPosition(float position, bool andStop = false)
	{
#if ENABLE_SUPERPOWERED_SDK
		musicPlay.CallStatic<string>("onSuperpoweredSetPosition", new object[3]
		{
			position * 1000f,
			andStop,
			false
		});
#else
#if !DISABLE_MUSIC
        _musicSource.time = position;
#else

#endif
#endif
    }

    public float GetPosition()
	{
#if ENABLE_SUPERPOWERED_SDK
        return musicPlay.CallStatic<float>("onSuperpoweredGetPosition", new object[0]) / 1000f;
#else
#if !DISABLE_MUSIC
        return _musicSource.time;
#else
        return Random.Range(1f, 5f);
#endif
#endif
    }

    public float GetVolume()
	{
#if ENABLE_SUPERPOWERED_SDK
        return currVolume;
#else
#if !DISABLE_MUSIC
        return _musicSource.volume;
#else
        return 1;
#endif
#endif
    }

    public void SetVolume(float volume)
	{
#if ENABLE_SUPERPOWERED_SDK
		musicPlay.CallStatic<string>("onSuperpoweredSetVolume", new object[1]
		{
			volume
		});
		currVolume = volume;
#else
#if !DISABLE_MUSIC
        _musicSource.volume = volume;
#endif
#endif
    }

    public bool IsPlaying()
	{
#if ENABLE_SUPERPOWERED_SDK
		return musicPlay.CallStatic<bool>("onSuperpoweredIsPlaying", new object[0]);
#else
#if !DISABLE_MUSIC
        return _musicSource.isPlaying;
#else
        return true;
#endif
#endif
    }

    public bool IsReady()
	{
#if ENABLE_SUPERPOWERED_SDK
		int num = musicPlay.CallStatic<int>("onSuperpoweredLoadMusicResult", new object[0]);
		return num == 0 || num == 2;
#else
        return isReady;
#endif
    }

    public bool IsFinished()
	{
#if ENABLE_SUPERPOWERED_SDK
		return musicPlay.CallStatic<int>("onSuperpoweredLoadMusicResult", new object[0]) == 2;
#else
#if !DISABLE_MUSIC
        return false;
#else
        return false;
#endif
#endif
    }

    public void SmoothStop(float sec)
	{
		if (coroutine != null)
		{
			StopCoroutine(coroutine);
		}
		coroutine = StartCoroutine(_SmoothStop(sec));
	}

	public void SmoothPlay(float sec)
	{
		if (coroutine != null)
		{
			StopCoroutine(coroutine);
		}
		coroutine = StartCoroutine(_SmoothPlay(sec));
	}

	private void OnApplicationPause(bool pauseStatus)
	{
		if (Configuration.instance.gameloaded)
		{
#if ENABLE_SUPERPOWERED_SDK
			if (pauseStatus)
			{
				musicPlay.CallStatic<string>("onSuperpoweredPause", new object[0]);
				musicPlay.CallStatic<string>("onSuperpoweredBackground", new object[0]);
			}
			else
			{
				musicPlay.CallStatic<string>("onSuperpoweredPlayPause", new object[0]);
				musicPlay.CallStatic<string>("onSuperpoweredForeground", new object[0]);
			}
#else
#endif
        }
	}

	private void OnApplicationQuit()
	{
#if ENABLE_SUPERPOWERED_SDK
		musicPlay.CallStatic<string>("onSuperpoweredDestroy", new object[0]);
#endif
	}

	private IEnumerator _SmoothStop(float timer)
	{
		float orignial = GetVolume();
		float destination = 0f;
		if (timer > 0f)
		{
			float t = 0f;
			float delta = Time.unscaledDeltaTime / timer;
			while (t < 1f)
			{
				t += delta;
				SetVolume(Mathf.Lerp(orignial, destination, t));
				yield return null;
			}
		}
		PauseMusic();
		SetVolume(destination);
		instance.SetPosition(0f);
	}

	private IEnumerator _SmoothPlay(float timer)
	{
		PlayMusic();
		float orignial = 0f;
		float destination = GetVolume();
		if (timer > 0f && destination > 0f)
		{
			float t = 0f;
			float delta = Time.unscaledDeltaTime / timer;
			while (t < 1f)
			{
				t += delta;
				SetVolume(Mathf.Lerp(orignial, destination, t));
				yield return null;
			}
		}
		SetVolume(destination);
	}
}
