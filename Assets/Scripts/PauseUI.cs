using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class PauseUI : MonoBehaviour
{
	public Text countDownText;

	public GameObject pauseText;

	public Image overlay;

	private void OnEnable()
	{
		countDownText.gameObject.SetActive(value: false);
		pauseText.SetActive(value: true);
		overlay.canvasRenderer.SetAlpha(0f);
		overlay.CrossFadeAlpha(1f, 0.5f, ignoreTimeScale: true);
	}

	public void CountDown()
	{
		if (pauseText.activeSelf)
		{
			countDownText.gameObject.SetActive(value: true);
			pauseText.SetActive(value: false);
			StartCoroutine(CountDown(2f));
		}
	}

	public void Resume()
	{
		base.gameObject.SetActive(value: false);
		Time.timeScale = 1f;
		SuperpoweredSDK.instance.SetPosition(GameController.controller.pauseTime);
		SuperpoweredSDK.instance.SetVolume(1f);
		SuperpoweredSDK.instance.SmoothPlay(0.5f);
	}

	private IEnumerator CountDown(float time)
	{
		do
		{
			countDownText.text = time.ToString();
			time -= 1f;
			yield return new WaitForSecondsRealtime(1f);
			if (time == 0f)
			{
				countDownText.text = "0";
				yield return null;
				pauseText.SetActive(value: true);
				base.gameObject.SetActive(value: false);
				Time.timeScale = 1f;
				SuperpoweredSDK.instance.SetPosition(GameController.controller.pauseTime);
				SuperpoweredSDK.instance.SetVolume(1f);
				SuperpoweredSDK.instance.SmoothPlay(1f);
				overlay.CrossFadeAlpha(0f, 0.5f, ignoreTimeScale: true);
			}
		}
		while (time >= 0f);
	}
}
