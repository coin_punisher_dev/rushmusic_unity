//@TODO
#if ENABLE_SOOMLA
using Soomla.Traceback;
#endif
using UnityEngine;

public class SoomlaAdsTrackingManager : MonoBehaviour
{
	public string appKey = "68b2a514-7f75-42d4-bc90-e6ad2b05a375";

	public void Start()
	{
#if ENABLE_SOOMLA
		Application.RequestAdvertisingIdentifierAsync(delegate(string advertisingId, bool trackingEnabled, string error)
		{
			string text = advertisingId;
			if (string.IsNullOrEmpty(text))
			{
				text = SystemInfo.deviceUniqueIdentifier;
			}
			SoomlaTraceback.Initialize(appKey, text);
		});
#endif
	}
}
