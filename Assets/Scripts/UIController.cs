using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour, GameSubscriber
{
	public static UIController ui;

	public GameObject gameui;

	public GameObject gameover;

	public GameObject continueui;

	public PauseUI pauseui;

	public GameObject roadPercent;

	public Image roadPercentFill;

	public RectTransform roadPercentPin;

	public Transform[] roadPercentStars;

	private void Awake()
	{
		ui = this;
	}

	private void Start()
	{
		GameController.controller.addSubcriber(this);
	}

	public void gameOver()
	{
	}

	public void gameStart()
	{
		gameui.SetActive(value: false);
	}

	public void gameContinue()
	{
		gameui.SetActive(value: true);
	}

	public void offGame()
	{
	}

	public void onGame()
	{
	}

	public void HideRoadPercent()
	{
		roadPercent.SetActive(value: false);
	}

	public void ResetSetRoadPercent()
	{
		roadPercent.SetActive(value: true);
		roadPercentFill.fillAmount = 0f;
		Vector3 v = roadPercentPin.anchoredPosition;
		v.y = 0f;
		roadPercentPin.anchoredPosition = v;
		roadPercentStars[0].gameObject.SetActive(value: false);
		roadPercentStars[1].gameObject.SetActive(value: false);
		roadPercentStars[2].gameObject.SetActive(value: false);
	}

	public void SetRoadPercent()
	{
		float num = Spawner.Intance.fullTime / Spawner.Intance.jumpTime;
		float num2 = (float)GameController.controller.score / num;
		roadPercentFill.fillAmount = num2 * 240f / 290f;
		Vector3 v = roadPercentPin.anchoredPosition;
		v.y = num2 * 240f;
		if (v.y > 79f)
		{
			roadPercentStars[0].gameObject.SetActive(value: true);
		}
		if (v.y > 159f)
		{
			roadPercentStars[1].gameObject.SetActive(value: true);
		}
		if (v.y >= 239f)
		{
			roadPercentStars[2].gameObject.SetActive(value: true);
		}
		if (v.y <= 290f)
		{
			roadPercentPin.anchoredPosition = v;
		}
	}
}
