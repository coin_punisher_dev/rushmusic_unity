using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneFader : MonoBehaviour
{
	public static SceneFader instance;

	public Image img;

	public Text text;

	public AnimationCurve curve;

	public GameObject canvas;

	private AsyncOperation async;

	[HideInInspector]
	public bool isAnimated;

	private float tSpeed;

	private Color tColor = new Color(0f, 0f, 0f, 0f);

	public GameObject loadingBar;

	public GameObject overlay;

	public Image loadingProgress;

	public Text percent;

	private string messageError;

	private float loadSongTime = 5f;

	private float startLoadTime;

	public bool allowFadeIn = true;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
			DontDestroyOnLoad(base.gameObject);
		}
		else if (instance != null)
		{
            Destroy(base.gameObject);
		}
	}

	private void Start()
	{
		SceneManager.sceneLoaded += OnSceneLoaded;
		tSpeed = Time.deltaTime * 3f;
	}

	public void ShowOverlay()
	{
		canvas.SetActive(value: true);
		overlay.SetActive(value: true);
	}

	public void HideOverlay()
	{
		canvas.SetActive(value: false);
		overlay.SetActive(value: false);
	}

	public void SetError(string error)
	{
		messageError = error;
	}

	private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
	{
		isAnimated = false;
		if (allowFadeIn)
		{
			StartCoroutine(FadeIn());
		}
		else
		{
			allowFadeIn = true;
		}
	}

	public void FadeTo(string scene, string song = null)
	{
		AdsManager.instance.HideBanner();
		if (!isAnimated)
		{
			isAnimated = true;
			StartCoroutine(FadeOut(scene, song));
		}
	}

	private IEnumerator FadeIn()
	{
		img.canvasRenderer.SetAlpha(1f);
		text.transform.gameObject.SetActive(value: false);
		loadingBar.SetActive(value: false);
		float t = 1f;
		while (t > 0f)
		{
			t -= tSpeed;
			float a = curve.Evaluate(t);
			tColor.a = a;
			img.color = tColor;
			yield return null;
		}
		canvas.SetActive(value: false);
		if (messageError != null)
		{
			Util.ShowMessage(messageError, 1);
			messageError = null;
		}
		AdsManager.instance.ShowBanner();
	}

	private IEnumerator FadeOut(string scene, string song = null)
	{
		if (song != null)
		{
			CoreData.SetCurrentSong(song);
		}
		canvas.SetActive(value: true);
		loadingBar.SetActive(value: false);
		float t = 0f;
		bool newData = false;
		if (song != null && !RemoteFilesData.instance.IsLocalSong(song))
		{
			if (!RemoteFilesData.instance.songList.ContainsKey(song))
			{
				DownloadFailed(scene);
				yield break;
			}
			newData = RemoteFilesData.instance.IsNewData(song);
		}
        Debug.LogError("SceneFader.FadeOut song:" + song + " is nameData:" + newData);

        if (!newData)
		{
			img.canvasRenderer.SetAlpha(1f);
			while (t < 1f)
			{
				t += tSpeed;
				tColor.a = curve.Evaluate(t);
				img.color = tColor;
				yield return null;
			}
		}
		if (newData)
		{
			Configuration.instance.SetCurrentLocation(LOCATION_NAME.DOWNLOAD_SONG);
			if (HomeManager.instance != null)
			{
				HomeManager.instance.ShowTips(2);
			}
			else if (UIController.ui != null)
			{
				UIController.ui.gameover.SetActive(value: false);
				tColor.a = 1f;
				img.color = tColor;
				img.canvasRenderer.SetAlpha(0.7f);
			}
		}
		else
		{
			async = SceneManager.LoadSceneAsync(scene);
			async.allowSceneActivation = false;
			text.transform.gameObject.SetActive(value: true);
		}
		if (song != null)
		{
			if (newData)
			{
				RemoteFilesData.instance.PrepareRemoteData(song);
				percent.text = string.Empty;
				loadingProgress.fillAmount = 0f;
				loadingBar.SetActive(value: true);
				while (RemoteFilesData.instance.currentFileStatus != FILE_DATA_STATUS.READY)
				{
					if (RemoteFilesData.instance.GetError() != null)
					{
						if (RemoteFilesData.instance.currentFileStatus != FILE_DATA_STATUS.NEED_UPDATE)
						{
							DownloadFailed(scene);
							yield break;
						}
						RemoteFilesData.instance.currentFileStatus = FILE_DATA_STATUS.READY;
						break;
					}
					UpdateProgress();
					yield return null;
				}
			}
			else
			{
				startLoadTime = Time.time;
				while (RemoteFilesData.instance.currentFileStatus != FILE_DATA_STATUS.READY && !RemoteFilesData.instance.IsLocalSong(song))
				{
					if (Time.time - startLoadTime > loadSongTime)
					{
						DownloadFailed(scene);
						yield break;
					}
					yield return null;
				}
			}
			if (!RemoteFilesData.instance.IsLocalSong(song))
			{
				Debug.Log("Remote Song Load"+song);
				SuperpoweredSDK.instance.LoadMusic(RemoteFilesData.LocalPath(song));
			}
			else
			{
				Debug.Log("Local Song Load");
				SuperpoweredSDK.instance.LoadMusic(song+song);
			}
			startLoadTime = Time.time;
			while (!SuperpoweredSDK.instance.IsReady())
			{
				if (Time.time - startLoadTime > loadSongTime)
				{
					if (!RemoteFilesData.instance.IsLocalSong(song))
					{
						RemoteFilesData.DeleteLocalFile(song);
					}
					DownloadFailed(scene);
					yield break;
				}
				yield return null;
			}
			SuperpoweredSDK.instance.UpdateMusicLength();
			if (newData)
			{
				async = SceneManager.LoadSceneAsync(scene);
			}
		}
		async.allowSceneActivation = true;
	}

	private void DownloadFailed(string scene)
	{
		canvas.SetActive(value: false);
		if (!RemoteFilesData.instance.IsLocalSong(CoreData.instance.GetCurrentSong().path))
		{
			Util.ShowMessage(LocalizationManager.instance.GetLocalizedValue("DOWNLOAD_FAILED"), 1);
		}
		else
		{
			Util.ShowMessage(LocalizationManager.instance.GetLocalizedValue("CANT_LOAD_SONG") + ": " + CoreData.instance.GetCurrentSong().path, 1);
		}
		isAnimated = false;
	}

	private void UpdateProgress()
	{
		float progress = RemoteFilesData.instance.GetProgress();
		if (progress > loadingProgress.fillAmount)
		{
			loadingProgress.fillAmount = RemoteFilesData.instance.GetProgress();
			percent.text = Math.Round(100f * loadingProgress.fillAmount) + "%";
		}
	}
}
