using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class BoardItem : MonoBehaviour
{
	public Image bg;

	public Image avatarImage;

	public Text nameText;

	public Text scoreText;

	public Image rankBg;

	public Text rankText;

	public string photoUrl;

	public void Setup(UserEntry entry, string boardID)
	{
		nameText.text = entry.name;
		if (entry.scores != null && entry.scores.ContainsKey(boardID))
		{
			scoreText.text = LocalizationManager.instance.GetLocalizedValue("SCORE_X").Replace("{0}", entry.scores[boardID].ToString());
		}
		else
		{
			scoreText.text = LocalizationManager.instance.GetLocalizedValue("SCORE_X").Replace("{0}", "0");
		}
		rankText.text = string.Empty;
		if (entry.ranks[boardID] == 1)
		{
			rankBg.sprite = ((!(boardID == "Total")) ? BoardManager.instance.song_rank1 : BoardManager.instance.total_rank1);
		}
		else if (entry.ranks[boardID] == 2)
		{
			rankBg.sprite = ((!(boardID == "Total")) ? BoardManager.instance.song_rank2 : BoardManager.instance.total_rank2);
		}
		else if (entry.ranks[boardID] == 3)
		{
			rankBg.sprite = ((!(boardID == "Total")) ? BoardManager.instance.song_rank3 : BoardManager.instance.total_rank3);
		}
		else
		{
			rankBg.sprite = BoardManager.instance.rankBgDefault;
			rankText.text = entry.ranks[boardID].ToString();
		}
		if (entry.photoUrl != null)
		{
			StartCoroutine(UpdateProfilePhoto(entry.photoUrl));
		}
		else
		{
			avatarImage.sprite = BoardManager.instance.avatarDefault;
		}
		if (CoreUser.instance.user != null && entry.auId == CoreUser.instance.user.auId)
		{
			bg.sprite = BoardManager.instance.bgMe;
			nameText.color = Color.white;
			scoreText.color = Color.white;
		}
		else
		{
			bg.sprite = BoardManager.instance.bgDefault;
			nameText.color = rankText.color;
			Color color = rankText.color;
			color.a = 0.7f;
			scoreText.color = color;
		}
	}

	public void SetupMe(long rank, int score, string url)
	{
		scoreText.text = LocalizationManager.instance.GetLocalizedValue("SCORE_X").Replace("{0}", (score <= 0) ? "--" : score.ToString());
		rankText.text = ((score <= 0) ? "--" : rank.ToString());
		if (url != null && base.gameObject.activeSelf)
		{
			StartCoroutine(UpdateProfilePhoto(url));
		}
		else
		{
			avatarImage.sprite = BoardManager.instance.avatarDefault;
		}
	}

	private IEnumerator UpdateProfilePhoto(string url)
	{
		if (photoUrl != url)
		{
			photoUrl = url;
			avatarImage.sprite = BoardManager.instance.avatarDefault;
			WWW www = new WWW(url);
			try
			{
				yield return www;
				if (www.error == null)
				{
					try
					{
						Texture2D texture2D = Util.RoundTexture(www.texture);
						avatarImage.sprite = Sprite.Create(texture2D, new Rect(0f, 0f, texture2D.width, texture2D.height), new Vector2(0f, 0f));
						www.Dispose();
					}
					catch
					{
					}
				}
			}
			finally
			{
                //@TODO
				//base._003C_003E__Finally0();
			}
		}
	}
}
