using System;
using UnityEngine;

[Serializable]
public class AchievementObj
{
	public static string PRE_STATUS = "achie_";

	public AchievementType type;

	public Sprite icon;

	public int diamonds;

	[HideInInspector]
	public AchievementStatus status;

	public int value;

	[HideInInspector]
	public int current;

	public static AchievementObj GetByType(AchievementType stype)
	{
		for (int i = 0; i < CoreData.instance.achievementList.Count; i++)
		{
			if (CoreData.instance.achievementList[i].type == stype)
			{
				return CoreData.instance.achievementList[i];
			}
		}
		return null;
	}

	public static void CheckTypes(AchievementType[] types)
	{
		for (int num = types.Length - 1; num >= 0; num--)
		{
			AchievementObj byType = GetByType(types[num]);
			if (byType.status == AchievementStatus.LOCK)
			{
				byType.UpdateStatus(forceChange: true);
				if (byType.status == AchievementStatus.UNLOCK)
				{
					GameObject gameObject = Util.ShowPopUp("AchievementPopUp");
					gameObject.GetComponent<AchievementPopUp>().SetObj(byType);
				}
				else if (byType.isNewChallenge() && byType.status == AchievementStatus.RECEIVED)
				{
					Util.ShowPopUp("Challenge");
				}
			}
		}
	}

	public bool isNewChallenge()
	{
		return (type == AchievementType.UNLOCK_SONG_5 || type == AchievementType.UNLOCK_SONG_10) && value == RemoteVariables.Unlock_NewChallenge;
	}

	public void UpdateStatus(bool forceChange = false)
	{
		AchievementStatus achievementStatus = (AchievementStatus)PlayerPrefs.GetInt(PRE_STATUS + type.ToString(), 0);
		if ((achievementStatus == AchievementStatus.NONE || forceChange) && achievementStatus != AchievementStatus.RECEIVED)
		{
			switch (type)
			{
			case AchievementType.LOGIN_1ST:
				if (PlayerPrefs.GetInt("bonus_fb_login", 0) == 1)
				{
					achievementStatus = AchievementStatus.RECEIVED;
					current = 1;
				}
				else
				{
					achievementStatus = AchievementStatus.LOCK;
				}
				break;
			case AchievementType.SCORE_100:
			case AchievementType.SCORE_250:
			case AchievementType.SCORE_500:
			case AchievementType.SCORE_1000:
				if (IsReachScore(value, out current))
				{
					current = value;
					achievementStatus = AchievementStatus.UNLOCK;
				}
				else
				{
					achievementStatus = AchievementStatus.LOCK;
				}
				break;
			case AchievementType.STARS_1:
			case AchievementType.STARS_10:
			case AchievementType.STARS_ALL:
				if (value == 0)
				{
					value = RemoteFilesData.instance.songList.Count;
				}
				if (IsReachStars(value, out current))
				{
					current = value;
					achievementStatus = AchievementStatus.UNLOCK;
				}
				else
				{
					achievementStatus = AchievementStatus.LOCK;
				}
				break;
			case AchievementType.UNLOCK_SONG_5:
			case AchievementType.UNLOCK_SONG_10:
			case AchievementType.UNLOCK_SONG_ALL:
				if (value == 0)
				{
					value = RemoteFilesData.instance.songList.Count;
				}
				if (IsReachUnlocks(value, out current))
				{
					current = value;
					achievementStatus = AchievementStatus.UNLOCK;
				}
				else
				{
					achievementStatus = AchievementStatus.LOCK;
				}
				break;
			case AchievementType.UNLOCK_BALL_5:
			case AchievementType.UNLOCK_BALL_10:
			case AchievementType.UNLOCK_BALL_ALL:
				if (value == 0)
				{
					value = CoreData.instance.ballGradient.Length;
				}
				if (IsReachBalls(value, out current))
				{
					current = value;
					achievementStatus = AchievementStatus.UNLOCK;
				}
				else
				{
					achievementStatus = AchievementStatus.LOCK;
				}
				break;
			case AchievementType.UNLOCK_THEME_2:
			case AchievementType.UNLOCK_THEME_ALL:
				if (value == 0)
				{
					value = 5;
				}
				if (IsReachThemes(value, out current))
				{
					current = value;
					achievementStatus = AchievementStatus.UNLOCK;
				}
				else
				{
					achievementStatus = AchievementStatus.LOCK;
				}
				break;
			case AchievementType.RETRY_50:
			case AchievementType.RETRY_100:
				current = PlayerPrefs.GetInt("retry", 0);
				if (current >= value)
				{
					current = value;
					achievementStatus = AchievementStatus.UNLOCK;
				}
				else
				{
					achievementStatus = AchievementStatus.LOCK;
				}
				break;
			case AchievementType.REVIVE_20:
			case AchievementType.REVIVE_50:
				current = PlayerPrefs.GetInt("revive", 0);
				if (current >= value)
				{
					current = value;
					achievementStatus = AchievementStatus.UNLOCK;
				}
				else
				{
					achievementStatus = AchievementStatus.LOCK;
				}
				break;
			case AchievementType.HIGHEST_SCORE:
				current = PlayerPrefs.GetInt("istop1", 0);
				if (current >= value)
				{
					current = value;
					achievementStatus = AchievementStatus.UNLOCK;
				}
				else
				{
					achievementStatus = AchievementStatus.LOCK;
				}
				break;
			case AchievementType.PERFECT_10:
				current = PlayerPrefs.GetInt("perfect_count", 0);
				if (current >= value)
				{
					current = value;
					achievementStatus = AchievementStatus.UNLOCK;
				}
				else
				{
					achievementStatus = AchievementStatus.LOCK;
				}
				break;
			default:
				achievementStatus = AchievementStatus.LOCK;
				break;
			}
		}
		if (isNewChallenge() && achievementStatus == AchievementStatus.UNLOCK)
		{
			achievementStatus = AchievementStatus.RECEIVED;
		}
		SetStatus(achievementStatus);
	}

	public void SetStatus(AchievementStatus status)
	{
		this.status = status;
		PlayerPrefs.SetInt(PRE_STATUS + type.ToString(), (int)status);
	}

	public void SetType(AchievementType type)
	{
		this.type = type;
	}

	private bool IsReachScore(int score, out int current)
	{
		current = 0;
		foreach (Item value2 in RemoteFilesData.instance.songList.Values)
		{
			int bestScore = CoreData.GetBestScore(value2.path);
			if (bestScore >= score)
			{
				return true;
			}
			if (bestScore > current)
			{
				current = bestScore;
			}
		}
		return false;
	}

	private bool IsReachStars(int count, out int current)
	{
		current = 0;
		foreach (Item value2 in RemoteFilesData.instance.songList.Values)
		{
			if (CoreData.GetBestStars(value2.path) >= 3)
			{
				current++;
				if (count == current)
				{
					return true;
				}
			}
		}
		return false;
	}

	private bool IsReachUnlocks(int count, out int current)
	{
		current = 0;
		foreach (Item value2 in RemoteFilesData.instance.songList.Values)
		{
			if (CoreData.GetSongType(value2.path, value2.type) == SONGTYPE.OPEN)
			{
				current++;
				if (count == current)
				{
					return true;
				}
			}
		}
		return false;
	}

	private bool IsReachBalls(int count, out int current)
	{
		current = 0;
		for (int i = 0; i < CoreData.instance.ballGradient.Length; i++)
		{
			if (CoreData.IsOpenBall(i))
			{
				current++;
				if (count == current)
				{
					return true;
				}
			}
		}
		return false;
	}

	private bool IsReachThemes(int count, out int current)
	{
		current = 0;
		for (int i = 0; i < 5; i++)
		{
			if (CoreData.IsOpenTheme(i))
			{
				current++;
				if (count == current)
				{
					return true;
				}
			}
		}
		return false;
	}
}
