using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Achievements : MonoBehaviour
{
	public GameObject item;

	public RectTransform container;

	private List<GameObject> items = new List<GameObject>();

	public Sprite lockIcon;

	public Sprite unlockIcon;

	public static Achievements instance;

	private void Awake()
	{
		instance = this;
		Util.UpdateBannerContent(base.transform);
		for (int i = 0; i < CoreData.instance.achievementList.Count; i++)
		{
			GameObject gameObject = UnityEngine.Object.Instantiate(item);
			items.Add(gameObject);
		}
	}

	private void OnEnable()
	{
		float num = 0f;
		//CoreData.instance.achievementList.Sort(delegate(AchievementObj x, AchievementObj y)
		//{
		//	int num2 = x.status.CompareTo(y.status);
		//	if (num2 == 0)
		//	{
		//		num2 = x.type.CompareTo(y.type);
		//	}
		//	return num2;
		//});
		for (int i = 0; i < CoreData.instance.achievementList.Count; i++)
		{
			items[i].GetComponent<AchievementItem>().SetValues(CoreData.instance.achievementList[i]);
			items[i].SetActive(value: true);
			items[i].transform.SetParent(item.transform.parent, worldPositionStays: false);
			StartCoroutine(DelayUpdate(items[i], item.transform.localPosition + Vector3.down * num));
			num += 103f;
		}
		container.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, num + 500f);
	}

	private IEnumerator DelayUpdate(GameObject item, Vector3 localPosition)
	{
		yield return null;
		item.transform.localPosition = localPosition;
	}

	public void Close()
	{
		base.gameObject.SetActive(value: false);
		SoundManager.instance.PlayGameButton();
		HomeManager.instance.ShowHome();
	}
}
