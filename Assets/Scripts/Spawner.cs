#if ENABLE_AMANOTES
using Amanotes.Data;
#endif
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Spawner : MonoBehaviour, GameSubscriber
{
	public GameObject block;

	public GameObject trees;

	public GameObject diamond;

	public GameObject followCamera;

	public ParticleSystem brustEffect;

	public ParticleSystem burnedEffect;

	public Color burnedColor;

	public static Spawner Intance;

	[HideInInspector]
	public float bmp;

	[HideInInspector]
	public float jumpTime;

	//[HideInInspector]
	public int spawnCount;

	private float delayTime;

	private int cloneTotal;

	public List<Block> blockList;

	private List<Block> cloneList;

	private float prevPosX;

	[HideInInspector]
	public float fullTime;

	private Vector3 blockPosition;

	private Vector3 blockScale;

	private GameObject cloneTrees;

	[HideInInspector]
	public float savedMusicTime;

	public Image[] bgsIm;

	public Color[] colors;

	public List<Color[]> colorMap;

	private bool onTransition;

	[HideInInspector]
	public int themeId;

	private int currentBg;

	private float burnPosition;

	public Item song;

	private Vector3 scaleTextSize = new Vector3(16f, 0.6f, 4f);

	[HideInInspector]
	public bool showedEndlessModeText;

	private int bestScore;

	public Color[] colorSub;

	public Image coloryImage;

	private List<SpriteRenderer> colorySprites = new List<SpriteRenderer>();

	private Coroutine bgCoroutine;

	private bool isNewChallenge;

	private List<int> parts = new List<int>();

	private GAME_PLAY gamePlay;

	private bool endlessMode;

	private int blockTotal;

	public static float groundPathLength = 40f;

	public List<GameObject> groundParts;

#if ENABLE_AMANOTES
	public List<NoteData> noteDatas;
#endif

	[HideInInspector]
	public int currentJumpNote;

	private Vector3 textPosition = new Vector3(0f, 2f, 0.5f);

	private Color fireColor = Color.black;

	private float firstJumpTime;

    private List<Block> _blockCollects = new List<Block>();

	private void Awake()
	{
		Intance = this;
		song = CoreData.instance.GetCurrentSong();
#if ENABLE_AMANOTES
		noteDatas = new List<NoteData>();
		string text = (!RemoteFilesData.instance.IsLocalSong(song.path)) ? RemoteFilesData.LocalPath(song.path) : song.path;
		text = text.Replace(".mp3", ".bin");
		byte[] array = RemoteFilesData.LoadFileBinary(text);
		if (array != null)
		{
			NoteGeneration.LoadFileContent(RemoteFilesData.LoadFileBinary(text), Difficulty.Easy, delegate(List<NoteData> resSucess)
			{
				noteDatas = resSucess;
			}, delegate(string errorLog)
			{
                Debug.LogError("Spawner.Awake error" + errorLog);

            });
		}
#endif
		themeId = CoreData.GetSelectedTheme(song.path);
		blockTotal = RemoteVariables.Block_Total;
		if (themeId == 6 || themeId == 7)
		{
			blockTotal++;
		}
		cloneTotal = blockTotal * 2 + 2;
		bmp = song.bmp;
		delayTime = song.startTime;
		isNewChallenge = CoreData.CheckNewChallenge();
		if (!isNewChallenge)
		{
			cloneTotal = 0;
		}
		if (!SuperpoweredSDK.instance.IsReady())
		{
			return;
		}
		if (song.type == SONGTYPE.OPEN.ToString() && !RemoteFilesData.instance.IsLocalSong(song.path))
		{
			isNewChallenge = false;
		}
		jumpTime = 60f / bmp;

		{
			delayTime -= jumpTime;
		}
		fullTime = SuperpoweredSDK.instance.musicLength - delayTime;
		blockScale = this.block.transform.Find("Square").localScale;
		blockList = new List<Block>();
		cloneList = new List<Block>();
		InsertBlock(this.block);
		for (int j = 1; j < blockTotal; j++)
		{
			GameObject obj = UnityEngine.Object.Instantiate(this.block);
			InsertBlock(obj);
		}
		for (int k = 0; k < cloneTotal; k++)
		{
			GameObject blockInGame = UnityEngine.Object.Instantiate(this.block);
			blockInGame.SetActive(value: false);
			InsertBlock(blockInGame, isClone: true);
		}
		blockPosition = this.block.transform.position;
		colorMap = new List<Color[]>();
		for (int l = 0; l < colors.Length / 2; l++)
		{
			Color[] item =
			{
				colors[l * 2],
				colors[l * 2 + 1]
			};
			colorMap.Add(item);
		}
		ref Vector3 reference = ref scaleTextSize;
		float num3 = RemoteVariables.Block_MaxDistance * 2f;
		Block block = blockList[0];
		Vector3 localScale = block.sq.transform.localScale;
		reference.x = num3 + localScale.x;
		float num4 = jumpTime * (float)(blockTotal - 2);
		parts.Add((int)(SuperpoweredSDK.instance.musicLength / 3f - num4));
		parts.Add((int)(SuperpoweredSDK.instance.musicLength / 3f));
		parts.Add((int)(SuperpoweredSDK.instance.musicLength * 2f / 3f - num4));
		parts.Add((int)(SuperpoweredSDK.instance.musicLength * 2f / 3f));
		parts.Add((int)(SuperpoweredSDK.instance.musicLength - num4));
		parts.Add((int)SuperpoweredSDK.instance.musicLength);
	}

	public float GetCurrentJumpTime(bool count = true)
	{
		float num = 0f;
		{
			num = jumpTime;
		}
		return num;
	}

	public void DecreaseJumpNote()
	{
		if (currentJumpNote > 0)
		{
			currentJumpNote--;
		}
	}

    private float[] GetMapPositions()
    {
        int num = spawnCount / RemoteVariables.Distance_Interval;
        float num2 = 0f;
        float num3 = 0f;
        if (num >= 3 || Ball.b.ph.endlessMode > 0)
        {
            num2 = RemoteVariables.Distance_MinMax4[1];
            num3 = RemoteVariables.Distance_MinMax4[0];
        }
        else
        {
            switch (num)
            {
                case 0:
                    num2 = RemoteVariables.Distance_MinMax1[1];
                    num3 = RemoteVariables.Distance_MinMax1[0];
                    break;
                case 1:
                    num2 = RemoteVariables.Distance_MinMax2[1];
                    num3 = RemoteVariables.Distance_MinMax2[0];
                    break;
                case 2:
                    num2 = RemoteVariables.Distance_MinMax3[1];
                    num3 = RemoteVariables.Distance_MinMax3[0];
                    break;
            }
        }
        if (gamePlay != 0)
        {
            if (num2 <= RemoteVariables.Block_MaxSize)
            {
                num2 = RemoteVariables.Block_MaxSize + 0.1f;
            }
            return new float[3]
            {
                0f - num2,
                0f,
                num2
            };
        }
        return new float[14]
        {
            0f,
            num3,
            num3 + 0.2f * (num2 - num3),
            num3 + 0.4f * (num2 - num3),
            num3 + 0.6f * (num2 - num3),
            num3 + 0.8f * (num2 - num3),
            num2,
            num2,
            num2,
            num2,
            num2,
            num2,
            num2,
            num2
        };
    }

	private void Start()
	{
		GameController.controller.addSubcriber(this);
	}

	public void spawn(Block bl)
	{
        try
        {
            float[] mapPositions = GetMapPositions();
            int num = 0;
            Vector3 position = bl.obj.transform.position;
            spawnCount++;
            {
                position.z = RemoteVariables.Block_Span * (float)spawnCount;
            }
            if (spawnCount <= 2)
            {
                position.x = 0f;
            }
            else
            {
                num = ((spawnCount != 3) ? UnityEngine.Random.Range(0, mapPositions.Length) : UnityEngine.Random.Range(mapPositions.Length - 2, mapPositions.Length));
                if (prevPosX > 0f && mapPositions[num] > 0f && UnityEngine.Random.Range(0, 8) != 0)
                {
                    position.x = 0f - mapPositions[num];
                }
                else
                {
                    position.x = mapPositions[num];
                }
                prevPosX = position.x;
            }
            bool isMoving = false;
            bool forward = UnityEngine.Random.Range(0, 2) == 0;
            if (song.type != SONGTYPE.OPEN.ToString() && gamePlay == GAME_PLAY.NORMAL
                && (Ball.b.ph.endlessMode > 0 || (Ball.b.ph.endlessMode == 0 && RemoteVariables.Block_EndlessMovingOnly == 0)))
            {
                isMoving = (UnityEngine.Random.Range(0, 10) == 0 && spawnCount > RemoteVariables.UpSpeedScore);
            }
            bool flag = false;
            if (spawnCount > blockList.Count)
            {
                if (!showedEndlessModeText && endlessMode)
                {
                    bl.text.SetText("ENDLESS MODE");
                    flag = true;
                    showedEndlessModeText = true;
                }
                if (spawnCount == bestScore)
                {
                    if (!flag)
                    {
                        bl.text.SetText("NEW HIGHSCORE");
                        flag = true;
                    }
                    else
                    {
                        bestScore++;
                    }
                }
            }
            if (flag)
            {
                bl.text.gameObject.SetActive(value: true);
                position.x = 0f;
                isMoving = false;
                if (themeId != 6 && themeId != 7)
                {
                    bl.sq.transform.localScale = scaleTextSize;
                }
                else
                {
                    bl.text.transform.localPosition = textPosition;
                }
                bl.anim.gameObject.SetActive(value: false);
                bl.obj.transform.position = position;
            }
            else
            {
                ClearText(bl);
                bl.obj.transform.position = position;
            }
            if (spawnCount > blockList.Count)
            {
                if (!flag && !diamond.activeSelf && UnityEngine.Random.Range(0, (int)((float)RemoteVariables.DiamondsRate * Ball.b.ph.timeScale)) == 1)
                {
                    diamond.SetActive(value: true);
                    diamond.transform.SetParent(bl.obj.transform, worldPositionStays: false);
                }
            }
            else
            {
                diamond.SetActive(value: false);
            }
            bl.SetColor(colors[currentBg]);
            bl.pj.Moving(isMoving, forward, gamePlay != GAME_PLAY.NORMAL);
            if (gamePlay == GAME_PLAY.GRID3 && !flag)
            {
                for (int i = 0; i < mapPositions.Length; i++)
                {
                    if (Mathf.Approximately(position.x, mapPositions[i]))
                    {
                        continue;
                    }
                    for (int j = 0; j < cloneList.Count; j++)
                    {
                        Block blockAtHere = cloneList[j];
                        if (!blockAtHere.obj.activeSelf)
                        {
                            Vector3 position2 = position;
                            position2.x = mapPositions[i];
                            Block block2 = cloneList[j];
                            block2.obj.transform.position = position2;
                            Block block3 = cloneList[j];
                            block3.obj.SetActive(value: true);
                            Block block4 = cloneList[j];
                            block4.pj.Moving(isMoving, forward, gamePlay != GAME_PLAY.NORMAL);
                            Block block5 = cloneList[j];
                            block5.pj.FlyEffect();
                            break;
                        }
                    }
                }
            }
            else if (gamePlay == GAME_PLAY.GRID2 && !flag)
            {
                int num2 = 0;
                for (int k = 0; k < mapPositions.Length; k++)
                {
                    if (Mathf.Approximately(position.x, mapPositions[k]))
                    {
                        num2 = k;
                        break;
                    }
                }
                for (int l = 0; l < cloneList.Count; l++)
                {
                    Block block6 = cloneList[l];
                    if (!block6.obj.activeSelf)
                    {
                        Vector3 position3 = position;
                        if (num2 == 0 || num2 == 2)
                        {
                            position3.x = mapPositions[1];
                        }
                        else if (UnityEngine.Random.Range(0, 1) == 0)
                        {
                            position3.x = mapPositions[0];
                        }
                        else
                        {
                            position3.x = mapPositions[2];
                        }
                        Block block7 = cloneList[l];
                        block7.obj.transform.position = position3;
                        Block block8 = cloneList[l];
                        block8.obj.SetActive(value: true);
                        Block block9 = cloneList[l];
                        block9.pj.Moving(isMoving, forward, gamePlay != GAME_PLAY.NORMAL);
                        Block block10 = cloneList[l];
                        block10.pj.FlyEffect();
                        break;
                    }
                }
            }
            bl.pj.FlyEffect();
            if (bl.topFire != null)
            {
                bl.topFire.gameObject.SetActive(value: false);
            }
            bl.hitShadow.gameObject.SetActive(value: false);
        }
        catch(Exception ex)
        {
            Debug.LogError("TilesHop Spawner.spawn ex:" + ex.Message);
        }
	}

	private void ClearText(Block bl)
	{
		bl.text.gameObject.SetActive(value: false);
		Block blockAtHere = blockList[0];
		Vector3 localScale = blockAtHere.sq.transform.localScale;
		if (!Mathf.Approximately(localScale.x, scaleTextSize.x))
		{
			Transform square = bl.sq.transform;
			Block block2 = blockList[0];
			square.localScale = block2.sq.transform.localScale;
		}
		else
		{
			Block block3 = blockList[1];
			Vector3 localScale2 = block3.sq.transform.localScale;
			if (!Mathf.Approximately(localScale2.x, scaleTextSize.x))
			{
				Transform transform2 = bl.sq.transform;
				Block block4 = blockList[1];
				transform2.localScale = block4.sq.transform.localScale;
			}
			else
			{
				Transform transform3 = bl.sq.transform;
				Block block5 = blockList[2];
				transform3.localScale = block5.sq.transform.localScale;
			}
		}
		bl.anim.gameObject.SetActive(value: true);
	}

	private void InsertBlock(GameObject obj, bool isClone = false)
	{
        Block item = new Block();
		item.obj = obj;
		item.pj = obj.GetComponent<PlatformJoker>();
		item.sq = obj.transform.Find("Square").gameObject;
		item.ps = item.sq.transform.Find("PS").GetComponent<ParticleSystem>();
		if (themeId == 7)
		{
			item.ma = item.sq.transform.Find("Obj/Color").GetComponent<MeshRenderer>().materials;
			item.ma.Union(item.sq.transform.Find("Obj/Stroke").GetComponent<MeshRenderer>().materials).ToArray();
		}
		else
		{
			item.ma = item.sq.GetComponent<MeshRenderer>().materials;
		}
		item.anim = item.sq.transform.Find("Perfect").GetComponent<Animator>();
		item.text = item.obj.transform.Find("Text").GetComponent<LineBreak>();
		item.hitShadow = item.sq.transform.Find("HitShadow").GetComponent<HitShadowAnim>();
		if (item.sq.transform.Find("TOP-FIRE") != null)
		{
			item.topFire = item.sq.transform.Find("TOP-FIRE").GetComponent<SpriteRenderer>();
		}
		if (!isClone)
		{
			item.obj.name = blockList.Count.ToString();
			item.psMain = item.ps.main;
			item.psSize = item.ps.sizeOverLifetime;
			item.psEmi = item.ps.emission;
			blockList.Add(item);
			return;
		}
		item.sq.tag = GAMETAG.BlockClone;
		if (item.topFire != null)
		{
			item.topFire.gameObject.SetActive(value: true);
		}
		item.sq.GetComponent<BoxCollider>().size -= Vector3.forward * 0.4f;
		UnityEngine.Object.Destroy(item.text.gameObject);
		UnityEngine.Object.Destroy(item.ps.gameObject);
		UnityEngine.Object.Destroy(item.anim.gameObject);
		UnityEngine.Object.Destroy(item.hitShadow.gameObject);
		cloneList.Add(item);
		if (themeId == 7)
		{
			item.sq.transform.Find("Obj/Stroke").gameObject.SetActive(value: false);
		}
	}

	public void generate()
	{
		for (int i = 0; i < blockList.Count; i++)
		{
			Block bl = blockList[i];
			bl.sq.transform.localScale = blockScale;
			spawn(bl);
		}
		for (int j = 0; j < cloneList.Count; j++)
		{
			Block blockAtHere = cloneList[j];
			blockAtHere.sq.transform.localScale = blockScale;
		}
	}

	public void gameStart()
	{
        _blockCollects.Clear();

        if (GameController.controller.replayCount > 0)
		{
			PlayMusic(isContinue: true);
		}
		else
		{
			PlayMusic();
		}
	}

	public void PlayMusic(bool isContinue = false)
	{
		if (!Configuration.instance.MusicIsOn())
		{
			SuperpoweredSDK.instance.SetVolume(0f);
		}
		else
		{
			SuperpoweredSDK.instance.SetVolume(1f);
		}
		float num = 0f;
		num = ((!isContinue) ? (delayTime + RemoteVariables.FixLatency) : savedMusicTime);
		SuperpoweredSDK.instance.SetPosition(num);
		if (num < 0f)
		{
			StartCoroutine(PlayMusicDelay(num));
		}
		else
		{
			SuperpoweredSDK.instance.SmoothPlay(1f);
		}
	}

	public IEnumerator PlayMusicDelay(float delayTime)
	{
		yield return new WaitForSeconds(delayTime);
		SuperpoweredSDK.instance.SmoothPlay(1f);
	}

	public float GetMusicTime()
	{
		return SuperpoweredSDK.instance.GetPosition() - RemoteVariables.FixLatency;
	}

	public void gamePrepare()
	{
		gamePlay = GAME_PLAY.NORMAL;
		bestScore = CoreData.GetBestScore(song.path);
		showedEndlessModeText = false;
		endlessMode = false;
		currentJumpNote = 0;
		spawnCount = -1;
		ResetBlock();
		if (trees != null)
		{
			if (cloneTrees != null)
			{
				UnityEngine.Object.Destroy(cloneTrees);
			}
			cloneTrees = UnityEngine.Object.Instantiate(trees);
			if (themeId == 4 || themeId == 5)
			{
				int childCount = cloneTrees.transform.GetChild(0).childCount;
				colorySprites = new List<SpriteRenderer>();
				for (int i = 0; i < childCount; i++)
				{
					colorySprites.Add(cloneTrees.transform.GetChild(0).GetChild(i).GetComponent<SpriteRenderer>());
					colorySprites.Add(cloneTrees.transform.GetChild(1).GetChild(i).GetComponent<SpriteRenderer>());
				}
				foreach (SpriteRenderer colorySprite in colorySprites)
				{
					colorySprite.color = colorSub[currentBg];
				}
			}
		}
		if (coloryImage != null)
		{
			coloryImage.color = colorSub[currentBg];
		}
		ResetGame();
	}

	public void gameContinue()
	{
		{
			spawnCount -= RemoteVariables.Block_Total;
		}
		ResetGame();
		foreach (Block block2 in blockList)
		{
			Block current = block2;
			current.obj.SetActive(value: true);
			Vector3 position = current.obj.transform.position;
			float x = position.x;
			current.obj.transform.position = Util.SetPositionX(current.obj.transform.position, 0f);
			Vector3 localScale = current.sq.transform.localScale;
			if (localScale.x < 10f)
			{
				current.sq.transform.localScale = blockScale;
			}
			current.pj.Moving(isMoving: false);
			foreach (Block clone in cloneList)
			{
				Block current2 = clone;
				current2.obj.SetActive(value: false);
			}
		}
	}

	private void ResetGame()
	{
		generate();
		Ball.b.gameObject.SetActive(value: true);
		Block block = blockList[0];
		Vector3 position = block.obj.transform.position;
		position.x = 0f;
		position.y = GameController.controller.ballPosition.y;
		Ball.b.cTransform.position = position;
		Ball.b.ph.trailParticleSystem.transform.position = position;
		position = followCamera.transform.position;
		Vector3 position2 = Ball.b.cTransform.position;
		position.z = position2.z - 7f;
		position.x = 0f;
		followCamera.transform.position = position;
		diamond.SetActive(value: false);
		Vector3 position3 = followCamera.transform.position;
		float num = position3.z + 5f;
		foreach (GameObject groundPart in groundParts)
		{
			groundPart.transform.localPosition = Util.SetPositionZ(groundPart.transform.localPosition, num);
			num += groundPathLength;
		}
	}

	public void gameOver()
	{
		foreach (Block block2 in blockList)
		{
			Block current = block2;
			current.obj.SetActive(value: false);
		}
		foreach (Block clone in cloneList)
		{
			Block current2 = clone;
			current2.obj.SetActive(value: false);
		}
		Ball.b.ph.rb.velocity = Vector3.zero;
		Ball.b.gameObject.SetActive(value: false);
		Ball.b.ph.enabled = false;
		SuperpoweredSDK.instance.SmoothStop(0.1f);
	}

	public void onGame()
	{
		int num = spawnCount / RemoteVariables.Distance_Interval;
		int num2 = (int)SuperpoweredSDK.instance.GetPosition();
		if (num % 4 == 1)
		{
			gamePlay = (isNewChallenge ? GAME_PLAY.GRID2 : GAME_PLAY.NORMAL);
		}
		else if (num % 4 == 2)
		{
			gamePlay = GAME_PLAY.NORMAL;
		}
		else if (num % 4 == 3)
		{
			gamePlay = (isNewChallenge ? GAME_PLAY.GRID3 : GAME_PLAY.NORMAL);
		}
		else if (num % 4 == 0)
		{
			gamePlay = GAME_PLAY.NORMAL;
		}
		if (!onTransition && spawnCount > blockTotal && (spawnCount - blockTotal) % RemoteVariables.Distance_Interval == 0 && themeId != 6)
		{
			SwitchBackGround();
		}
		if (num2 == parts[4]
#if ENABLE_AMANOTES
            && noteDatas.Count == 0
#endif
            )
		{
			endlessMode = true;
		}
	}

	public void offGame()
	{
	}

	private void ResetBlock()
	{
		if (blockList.Count > 0)
		{
			foreach (Block block3 in blockList)
			{
				Block current = block3;
				current.obj.SetActive(value: true);
			}
			Block blockAtHere = blockList[0];
			blockAtHere.pj.Moving(isMoving: false);
			Block block2 = blockList[0];
			block2.obj.transform.position = blockPosition;
			ClearText(blockList[0]);
			if (themeId == 0 || themeId == 7)
			{
				currentBg = UnityEngine.Random.Range(0, bgsIm.Length);
			}
			else
			{
				currentBg = 0;
			}
			if (bgCoroutine != null)
			{
				StopCoroutine(bgCoroutine);
				onTransition = false;
			}
			for (int i = 0; i < bgsIm.Length; i++)
			{
				bgsIm[i].transform.gameObject.SetActive(value: false);
			}
			bgsIm[currentBg].transform.gameObject.SetActive(value: true);
			for (int j = 0; j < blockList.Count; j++)
			{
				blockList[j].SetColor(colors[currentBg]);
			}
		}
		if (cloneList.Count > 0)
		{
			Color color = Color.Lerp(colors[currentBg], Color.black, 0.3f);
			for (int k = 0; k < cloneList.Count; k++)
			{
				cloneList[k].SetColor(color);
			}
		}
	}

    public void HitByColector(Block blockCollect)
    {
        blockCollect.ps.Stop();
        blockCollect.ps.gameObject.SetActive(value: false);
        spawn(blockCollect);
    }


    public void HitByColector(string name)
	{
        //Debug.LogError("TilesHop HitByColector name:" + name);
		Block blockCollect = GetBlockByName(name);
        HitByColector(blockCollect);
    }

    public void Hit(Block bl, int perfectCount)
	{
		Vector3 localScale = bl.sq.transform.localScale;
		if (localScale.x > 10f)
		{
			SoundManager.instance.PlayCheckPoint();
		}
		UIController.ui.SetRoadPercent();
		if (bl.text.gameObject.activeSelf && (themeId == 6 || themeId == 7))
		{
			bl.text.StartHide();
		}
		foreach (Block clone in cloneList)
		{
			Block current = clone;
			Vector3 position = current.obj.transform.position;
			float z = position.z;
			Vector3 position2 = bl.obj.transform.position;
			if (Mathf.Approximately(z, position2.z))
			{
				StartCoroutine(HideClone(current));
			}
		}
		if (GameController.controller.score >= 0)
		{
			if (Ball.b.ph.endlessMode == 0)
			{
				burnedEffect.gameObject.SetActive(value: false);
				brustEffect.gameObject.SetActive(value: true);
				Vector3 position3 = brustEffect.transform.position;
				Vector3 position4 = Ball.b.cTransform.position;
				position3.x = position4.x;
				Vector3 position5 = Ball.b.cTransform.position;
				position3.z = position5.z;
				brustEffect.transform.position = position3;
				brustEffect.Play();
			}
			else
			{
				burnedEffect.gameObject.SetActive(value: true);
				brustEffect.gameObject.SetActive(value: false);
				Vector3 position6 = burnedEffect.transform.position;
				Vector3 position7 = Ball.b.cTransform.position;
				position6.x = position7.x;
				Vector3 position8 = Ball.b.cTransform.position;
				position6.z = position8.z;
				burnedEffect.transform.position = position6;
				burnedEffect.Play();
			}
			if (bl.topFire != null)
			{
				bl.topFire.gameObject.SetActive(value: true);
				fireColor.a = ((Ball.b.ph.endlessMode != 0) ? 0.7f : 0.3f);
				bl.topFire.color = fireColor;
			}
			if (Ball.b.ph.endlessMode == 0)
			{
				if (themeId == 7)
				{
					bl.SetColor(Color.Lerp(bl.GetColor(), Color.black, 0.45f));
				}
				else
				{
					bl.SetColor(Color.Lerp(bl.GetColor(), Color.black, 0.22f));
				}
			}
			else if (themeId == 7)
			{
				bl.SetColor(Color.Lerp(bl.GetColor(), Color.black, 0.9f));
			}
			else
			{
				bl.SetColor(Color.Lerp(bl.GetColor(), Color.black, 0.7f));
			}
			Vector3 position9 = bl.obj.transform.position;
			burnPosition = position9.z;
			bl.pj.SinkEffect();
			if (!bl.ps.isPlaying)
			{
				bl.hitShadow.ActiveAnim();
			}
		}
		bl.ps.gameObject.SetActive(value: false);
		if (perfectCount <= 0)
		{
			return;
		}
		int num = RemoteVariables.Block_PerfectTrigger - 3;
		if (perfectCount > num)
		{
			bl.ps.gameObject.SetActive(value: true);
			bl.psMain.maxParticles = perfectCount - num;
			ParticleSystem.MinMaxCurve size = bl.psSize.size;
			if (bl.psMain.maxParticles == 1)
			{
				size.curveMultiplier = 3f;
			}
			else if (bl.psMain.maxParticles == 2)
			{
				size.curveMultiplier = 3.5f;
			}
			else
			{
				size.curveMultiplier = 4f;
			}
			bl.psSize.size = size;
			bl.ps.Play();
		}
		foreach (Block block2 in blockList)
		{
			Block current2 = block2;
			if (current2.anim.gameObject.activeSelf)
			{
				current2.anim.Play("FadeInOut", -1, 0f);
			}
		}
	}

	private IEnumerator HideClone(Block bl)
	{
		float t = 0f;
		float time = 0.3f;
		while (t < time)
		{
			t += Time.deltaTime;
			if (themeId == 0)
			{
				bl.obj.transform.position += Vector3.down * Time.deltaTime * 40f;
			}
			else
			{
				bl.obj.transform.position += Vector3.back * Time.deltaTime * 30f;
			}
			yield return null;
		}
		bl.obj.gameObject.SetActive(value: false);
	}

	private IEnumerator BurnBlock(Block bl)
	{
		float t = 0f;
		float time = 0.2f;
		float spawnTime = Ball.b.ph.GetJumpTime() * 1.5f;
		if (time > spawnTime)
		{
			time = spawnTime;
		}
		Color from = bl.GetColor();
		Color to = (Ball.b.ph.endlessMode != 0) ? Color.Lerp(from, burnedColor, 0.9f) : Color.Lerp(from, Color.white, 0.4f);
		Vector3 position = bl.obj.transform.position;
		burnPosition = position.z;
		while (t < time)
		{
			t += Time.deltaTime;
			bl.SetColor(Color.Lerp(from, to, t / time));
			yield return null;
		}
	}

	public void ChangeBlockSize(int perfectCount)
	{
		StartCoroutine(UpdateBlockSize(perfectCount));
	}

	private IEnumerator UpdateBlockSize(int perfectCount)
	{
		float delta = RemoteVariables.Block_DeltaSize * Time.deltaTime / 0.2f;
		Block block = blockList[0];
		Vector3 localScale = block.sq.transform.localScale;
		float z = localScale.z;
		Block block2 = blockList[0];
		Vector3 localScale2 = block2.sq.transform.localScale;
		Vector3 increaseDelta = new Vector3(1f, 0f, z / localScale2.x) * delta;
		float increaseAmount = RemoteVariables.Block_DeltaSize;
		if (perfectCount < 0)
		{
			Block block3 = blockList[0];
			Vector3 localScale3 = block3.sq.transform.localScale;
			if (localScale3.x - RemoteVariables.Block_DeltaSize > RemoteVariables.Block_MinSize)
			{
				goto IL_02f7;
			}
		}
		if (perfectCount > 0)
		{
			Block block4 = blockList[0];
			Vector3 localScale4 = block4.sq.transform.localScale;
			if (localScale4.x + RemoteVariables.Block_DeltaSize < RemoteVariables.Block_MaxSize)
			{
				goto IL_02f7;
			}
			yield break;
		}
		yield break;
		IL_02f7:
		while (increaseAmount > 0f)
		{
			increaseAmount -= delta;
			foreach (Block block5 in blockList)
			{
				Block current = block5;
				Vector3 localScale5 = current.sq.transform.localScale;
				if (localScale5.x < scaleTextSize.x)
				{
					if (perfectCount > 0)
					{
						current.sq.transform.localScale += increaseDelta;
					}
					else
					{
						current.sq.transform.localScale -= increaseDelta;
					}
				}
			}
			foreach (Block clone in cloneList)
			{
				Block current2 = clone;
				if (perfectCount > 0)
				{
					current2.sq.transform.localScale += increaseDelta;
				}
				else
				{
					current2.sq.transform.localScale -= increaseDelta;
				}
			}
			yield return null;
		}
	}

	public void SwitchBackGround()
	{
		onTransition = true;
		int num = 0;
		while (true)
		{
			if (num < bgsIm.Length)
			{
				if (bgsIm[num].transform.gameObject.activeSelf)
				{
					break;
				}
				num++;
				continue;
			}
			return;
		}
		int num2 = num + 1;
		if (num2 == bgsIm.Length)
		{
			num2 = 0;
		}
		bgCoroutine = StartCoroutine(BackgroundTransition(num, num2));
	}

	private IEnumerator BackgroundTransition(int current, int next)
	{
		bgsIm[next].transform.gameObject.SetActive(value: true);
		float time = 4f;
		currentBg = next;
		bgsIm[current].CrossFadeAlpha(0f, time, ignoreTimeScale: false);
		bgsIm[next].canvasRenderer.SetAlpha(0f);
		bgsIm[next].CrossFadeAlpha(1f, time, ignoreTimeScale: false);
		float t = 0f;
		while (t < time)
		{
			t += Time.deltaTime;
			Color color = Color.Lerp(colors[current], colors[next], t);
			for (int i = 0; i < blockList.Count; i++)
			{
				Block blockAtHere = blockList[i];
				Vector3 position = blockAtHere.obj.transform.position;
				if (position.z > burnPosition)
				{
					blockList[i].SetColor(color);
				}
			}
			Color clonecolor = Color.Lerp(color, Color.black, (themeId != 7) ? 0.3f : 0f);
			for (int j = 0; j < cloneList.Count; j++)
			{
				cloneList[j].SetColor(clonecolor);
			}
			if (colorSub != null && colorSub.Length > 0)
			{
				color = Color.Lerp(colorSub[current], colorSub[next], t);
				if (coloryImage != null)
				{
					coloryImage.color = color;
				}
				if (colorySprites.Count > 0)
				{
					foreach (SpriteRenderer colorySprite in colorySprites)
					{
						colorySprite.color = color;
					}
				}
			}
			yield return null;
		}
		bgsIm[current].transform.gameObject.SetActive(value: false);
		onTransition = false;
	}

	public Block GetBlockByName(string name)
	{
		Block result = blockList[0];
		if (name != null)
		{
			for (int i = 0; i < blockList.Count; i++)
			{
				Block blockAtHere = blockList[i];
				if (name == blockAtHere.obj.name)
				{
					result = blockList[i];
					break;
				}
			}
		}
		return result;
	}
}
