using UnityEngine;

public class PlatformJoker : MonoBehaviour
{
	public Vector3 move = new Vector3(0.1f, 0f, 0f);

	private bool forward;

	private bool isMoving;

	private float sinkTime;

	public AnimationCurve curve;

	public AnimationCurve curve_moving;

	private float flyTime;

	private Vector3 flyStartPosition;

	private Vector3 flyEndPosition;

	[HideInInspector]
	public float center;

	[HideInInspector]
	public float distance;

	private float t;

	private float movingTime;

	public void Moving(bool isMoving, bool forward = false, bool isPingPong = false)
	{
		this.isMoving = isMoving;
		this.forward = forward;
		t = UnityEngine.Random.Range(0f, 100f) / 100f;
		movingTime = RemoteVariables.Block_MovingTime / Ball.b.ph.timeScale;
		center = 0f;
		distance = RemoteVariables.Block_MaxDistance;
		if (isMoving)
		{
			Vector3 position = base.transform.position;
			position.x = curve_moving.Evaluate(t) * distance + center;
			base.transform.position = position;
		}
	}

	private void Update()
	{
		if (!Mathf.Approximately(Time.timeScale, 1f))
		{
			return;
		}
		if (flyTime > 0f)
		{
			if (Spawner.Intance.themeId == 77)
			{
				flyTime -= Time.deltaTime * 3f;
				base.transform.position = flyEndPosition + Vector3.down * 10f * Configuration.instance.defaultCurve.Evaluate(flyTime);
			}
			else
			{
				flyTime -= Time.deltaTime * 5f;
				base.transform.position = flyEndPosition + Vector3.forward * 40f * Configuration.instance.defaultCurve.Evaluate(flyTime);
			}
			return;
		}
		if (flyTime < 0f)
		{
			base.transform.position = flyEndPosition;
			flyTime = 0f;
			return;
		}
		Vector3 position = base.transform.position;
		if (isMoving)
		{
			if (forward && t > 1f)
			{
				forward = false;
			}
			else if (!forward && t < 0f)
			{
				forward = true;
			}
			if (forward)
			{
				t += Time.deltaTime / movingTime;
				position.x = curve_moving.Evaluate(t) * distance + center;
			}
			else
			{
				t -= Time.deltaTime / movingTime;
				position.x = -1f * curve_moving.Evaluate(1f - t) * distance + center;
			}
		}
		if (sinkTime > 0f)
		{
			sinkTime -= Time.deltaTime * 7f;
			float num = position.y = curve.Evaluate(sinkTime) * RemoteVariables.Block_Sag;
		}
		base.transform.position = position;
	}

	public void SinkEffect()
	{
		sinkTime = 1f;
	}

	public void FlyEffect()
	{
		if (GameController.controller.game != 0)
		{
			flyTime = 1f;
			flyEndPosition = base.transform.position;
			if (Spawner.Intance.themeId == 77)
			{
				base.transform.position = base.transform.position + Vector3.down * 10f;
			}
			else
			{
				base.transform.position = base.transform.position + Vector3.forward * 50f;
			}
		}
	}

	private void OnDisable()
	{
		sinkTime = 0f;
		flyTime = 0f;
	}
}
