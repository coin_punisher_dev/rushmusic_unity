using System.Collections;
using UnityEngine;
#if ENABLE_IAP
using UnityEngine.Purchasing;
#endif
using UnityEngine.UI;

public class Shop : PopupUI
{
	public static Shop instance;

	public Text title;

	private string[] originalPrice;

	public Text diamondBonusText;

	private void Start()
	{
		instance = this;
		Configuration.instance.SetCurrentLocation(LOCATION_NAME.SHOP, isPopup: true);
		diamondBonusText = base.transform.Find("Window/Package0/Text").GetComponent<Text>();
		diamondBonusText.text = "+" + Configuration.instance.diamondsVideoBonus.ToString();
		if (Configuration.instance.isNoAds)
		{
			UpdateNoAds();
		}
		AnalyticHelper.IAP_Impression();
		originalPrice = new string[5];
#if ENABLE_IAP
		for (int i = 1; i <= 5; i++)
		{
			Text component = base.transform.Find("Window/Package" + i.ToString() + "/Btn").GetChild(0).GetComponent<Text>();
			originalPrice[i - 1] = component.text;
			Product product = InAppPurchaser.instance.GetProduct(i);
			if (product != null)
			{
				component.text = product.metadata.localizedPriceString;
			}
		}
#endif
	}

	public void IncreaseDiamondsVideoBonus()
	{
		if (Configuration.instance.diamondsVideoBonus == 0)
		{
			Configuration.instance.diamondsVideoBonus = RemoteVariables.DiamondsVideoBonus;
		}
		int num = Configuration.instance.diamondsVideoBonus + 5;
		if (num > 40)
		{
			num = RemoteVariables.DiamondsVideoBonus;
		}
		StartCoroutine(UpdateText(Configuration.instance.diamondsVideoBonus, num));
		Configuration.instance.diamondsVideoBonus = num;
	}

	private IEnumerator UpdateText(int from, int to)
	{
		float t = 0f;
		float time = 1f;
		while (t < time)
		{
			t += Time.deltaTime;
			diamondBonusText.text = "+" + ((int)Mathf.Lerp(from, to, t / time)).ToString();
			yield return null;
		}
	}

	private void UpdateNoAds()
	{
		GameObject gameObject = base.transform.Find("Window/Package1").gameObject;
		GameObject gameObject2 = base.transform.Find("Window/Package2").gameObject;
		GameObject gameObject3 = base.transform.Find("Window/Package3").gameObject;
		GameObject gameObject4 = base.transform.Find("Window/Package4").gameObject;
		GameObject gameObject5 = base.transform.Find("Window/Package5").gameObject;
		gameObject.SetActive(value: false);
		gameObject2.transform.localPosition = gameObject.transform.localPosition;
		gameObject3.transform.localPosition = gameObject.transform.localPosition + Vector3.down * 100f;
		gameObject4.transform.localPosition = gameObject.transform.localPosition + Vector3.down * 200f;
		gameObject5.transform.localPosition = gameObject.transform.localPosition + Vector3.down * 300f;
	}

	public void SetNeedMore(int x)
	{
		title.text = LocalizationManager.instance.GetLocalizedValue("NEED_MORE_DIAMONDS").Replace("{0}", x.ToString());
	}

	public void ShowAds()
	{
		SoundManager.instance.PlayGameButton();
		AdsManager.instance.ShowRewardAds(VIDEOREWARD.GETDIAMOND);
	}

	public void PurchasePackage(int id)
	{
		SoundManager.instance.PlayGameButton();
		InAppPurchaser.instance.BuyProduct(id);
		string text = base.transform.Find("Window/Package" + id.ToString() + "/Text").GetComponent<Text>().text;
		AnalyticHelper.IAP_Click(InAppPurchaser.instance.GetProductID(id), text, originalPrice[id - 1]);
	}

	public void CompletedPurchase(int id)
	{
		if (id == 1)
		{
			Configuration.instance.AddNoAds();
			UpdateNoAds();
		}
		else
		{
			int num = int.Parse(base.transform.Find("Window/Package" + id.ToString() + "/Text").GetComponent<Text>().text.Replace("+", string.Empty));
			TopBar.instance.UpdateDiamond(4000, Income_Type.IAP);
		}
		Util.ShowMessage(LocalizationManager.instance.GetLocalizedValue("PURCHASE_SUCCESS"));
	}
}
