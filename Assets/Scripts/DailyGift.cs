using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DailyGift : PopupUI
{
	public static DailyGift instance;

	public Image[] days;

	public Sprite receivedSprite;

	public GameObject diamonEffect;

	public GameObject claimBtn;

	public GameObject ballPreview;

	private int activedDay = -1;

	public Text countDownText;

	private void Awake()
	{
		instance = this;
		Configuration.instance.SetCurrentLocation(LOCATION_NAME.DAILY_GIFT, isPopup: true);
	}

	private void Start()
	{
		Prepare();
	}

	private void Update()
	{
		if (activedDay == -1)
		{
			countDownText.text = $"{23 - DateTime.Now.Hour:00}" + " : " + $"{59 - DateTime.Now.Minute:00}" + " : " + $"{59 - DateTime.Now.Second:00}";
		}
	}

	private void Prepare()
	{
		activedDay = GetActiveDay();
		int num = Configuration.GetDailyGiftNumber();
		if (activedDay == 0)
		{
			num = -1;
		}
		if (activedDay == -1)
		{
			claimBtn.SetActive(value: false);
		}
		else
		{
			ActivatedDay(activedDay);
			claimBtn.SetActive(value: true);
		}
		for (int i = 0; i <= num; i++)
		{
			ReceivedDay(i);
		}
	}

	public static int GetActiveDay()
	{
		int result = -1;
		DateTime t = DateTime.Parse(Configuration.GetDailyGiftDate());
		int num = (int)(DateTime.Now - t.Date).TotalDays;
		if (t > DateTime.Now)
		{
			t = DateTime.Now;
			num = 0;
		}
		if (num <= 1)
		{
			if (num == 1)
			{
				int num2 = Configuration.GetDailyGiftNumber() + 1;
				if (num2 < 0)
				{
					num2 = 0;
				}
				if (num2 < 5)
				{
					result = num2;
				}
				else
				{
					Configuration.SetDailyGift(-2, DateTime.Now.AddDays(-1.0).ToString());
					result = 0;
				}
			}
		}
		else
		{
			Configuration.SetDailyGift(-2, DateTime.Now.AddDays(-1.0).ToString());
			result = 0;
		}
		return result;
	}

	public void Claim()
	{
		diamonEffect.SetActive(value: false);
		if (activedDay < 0)
		{
			return;
		}
		days[activedDay].sprite = receivedSprite;
		int num = 0;
		if (activedDay != 4)
		{
			GameObject diamondObj = days[activedDay].transform.Find("diamond").gameObject;
			num = int.Parse(diamondObj.GetComponent<Text>().text);
			TopBar.instance.UpdateDiamond(num, Income_Type.DAILY_GIFT);
		}
		else
		{
			List<int> list = new List<int>();
			for (int i = 1; i < CoreData.instance.ballGradient.Length; i++)
			{
				if (!CoreData.IsOpenBall(i))
				{
					list.Add(i);
				}
			}
			if (list.Count == 0)
			{
				num = 150;
				TopBar.instance.UpdateDiamond(num, Income_Type.DAILY_GIFT);
			}
			else
			{
				int num2 = list[UnityEngine.Random.Range(0, list.Count)];
				GameObject gameObject2 = HomeManager.instance.ballList.ballContainer.transform.GetChild(num2).transform.GetChild(0).gameObject;
				GameObject gameObject3 = UnityEngine.Object.Instantiate(ballPreview);
				gameObject3.transform.SetParent(days[4].transform, worldPositionStays: false);
				CoreData.SetOpenBall(num2);
				gameObject3.GetComponent<Image>().sprite = gameObject2.GetComponent<Image>().sprite;
				gameObject3.transform.GetChild(0).GetComponent<ParticleSystem>().Play();
				days[4].transform.Find("Image").gameObject.SetActive(value: false);
				diamonEffect.SetActive(value: true);
			}
		}
		ReceivedDay(activedDay, isClaim: true);
		Configuration.SetDailyGift(activedDay, DateTime.Now.ToString());
		if (activedDay != 4)
		{
			Close();
		}
		else
		{
			SoundManager.instance.PlayGameButton();
			claimBtn.SetActive(value: false);
			claimBtn.transform.parent.Find("Close").gameObject.SetActive(value: true);
		}
		activedDay = -1;
	}

	public void ActivatedDay(int day)
	{
		diamonEffect.transform.SetParent(days[day].transform, worldPositionStays: false);
		diamonEffect.SetActive(value: true);
	}

	public void ReceivedDay(int day, bool isClaim = false)
	{
		days[day].sprite = receivedSprite;
	}
}
