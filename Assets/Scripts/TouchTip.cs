using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class TouchTip : MonoBehaviour
{
	public Image overlay;

	public Text title;

	public Text text;

	private void OnEnable()
	{
		overlay.canvasRenderer.SetAlpha(0f);
		title.canvasRenderer.SetAlpha(0f);
		text.canvasRenderer.SetAlpha(0f);
		overlay.CrossFadeAlpha(1f, 2f, ignoreTimeScale: false);
		title.CrossFadeAlpha(1f, 2f, ignoreTimeScale: false);
		text.CrossFadeAlpha(1f, 2f, ignoreTimeScale: false);
		StartCoroutine(FadeCompleted());
	}

	private IEnumerator FadeCompleted()
	{
		yield return new WaitForSeconds(3f);
		UIController.ui.gameover.SetActive(value: true);
		UnityEngine.Object.Destroy(base.gameObject);
	}
}
