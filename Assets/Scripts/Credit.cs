using UnityEngine;
using UnityEngine.UI;

public class Credit : PopupUI
{
	public Text versionText;

	public static Credit instance;

	private void Start()
	{
		instance = this;
		versionText.text = "V" + Application.version;
		Configuration.instance.SetCurrentLocation(LOCATION_NAME.CREDIT, isPopup: true);
	}

	private void Update()
	{
		if (Input.GetMouseButtonDown(0))
		{
			Close();
		}
	}
}
