using UnityEngine;

public class RateUs : PopupUI
{
	private void Start()
	{
		Configuration.instance.SetCurrentLocation(LOCATION_NAME.RATE_US, isPopup: true);
	}

	public void DontAsk()
	{
		PlayerPrefs.SetInt("RateLater", 99);
		Close();
	}

	public void Rate()
	{
		Util.RateUs();
		Close();
	}

	public void RateLater()
	{
		PlayerPrefs.SetInt("RateLater", PlayerPrefs.GetInt("RateLater", 0) + 1);
		Close();
	}

	public static void CheckAndOpenRate()
	{
		bool flag = PlayerPrefs.GetInt("RateLater", 0) == 0;
		bool flag2 = PlayerPrefs.GetInt("RateLater", 0) == 1;
		if (PlayerPrefs.GetInt("PlayCount", 0) >= RemoteVariables.PlayCountToRate && (flag || flag2))
		{
			foreach (Item value in RemoteFilesData.instance.songList.Values)
			{
				if (CoreData.GetSongType(value.path, value.type) == SONGTYPE.OPEN)
				{
					int @int = PlayerPrefs.GetInt("bestscore" + value.path);
					if ((@int > RemoteVariables.ScoreToRate && flag) || (@int > RemoteVariables.ScoreToRate + 100 && flag2))
					{
						Util.ShowPopUp("RateUs");
						break;
					}
				}
			}
		}
	}
}
