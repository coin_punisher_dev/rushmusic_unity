using UnityEngine;
using UnityEngine.UI;

public class Challenge : PopupUI
{
	public Text desc;

	public static Challenge instance;

	private void Awake()
	{
		instance = this;
		Physics.gravity = new Vector3(0f, -9.8f, 0f);
	}

	private void Start()
	{
		desc.text = LocalizationManager.instance.GetLocalizedValue("NEW_CHALLENGE_DESC").Replace("{0}", RemoteVariables.Unlock_NewChallenge.ToString());
	}
}
