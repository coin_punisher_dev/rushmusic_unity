public class AdPokkt : AdNetwork
{
	public string androidPokktAppId;

	public string androidPokktSecurityKey;

	private bool _listenerAttached;

	public string screenName = "Default";

	private int loadFailedCount;

	public override void init()
	{
	}

	private void FailedVideo()
	{
		loadFailedCount++;
		if (loadFailedCount == 3)
		{
			AnalyticHelper.FireEvent(FIRE_EVENT.Change_Adn);
		}
	}
}
