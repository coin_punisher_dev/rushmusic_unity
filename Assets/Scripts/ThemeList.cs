using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ThemeList : MonoBehaviour
{
	public static ThemeList instance;

	public RectTransform panel;

	public ScrollRect scrollRect;

	public List<ThemeItem> bttn;

	public RectTransform center;

	public float[] distance;

	private bool dragging;

	private int bttnDistance;

	private int minButtonNum;

	private float clickSpeed = 0.4f;

	private float startDragAnchored;

	private float startDragTime;

	private float realDistance;

	private float previewAnchored = -1f;

	private bool allowScroll = true;

	private void Awake()
	{
		instance = this;
		bttn[RemoteVariables.DefaultTheme].transform.localPosition = Vector3.zero;
		bttn[RemoteVariables.DefaultTheme].transform.SetAsFirstSibling();
		int num = 0;
		for (int i = 0; i < bttn.Count; i++)
		{
			if (i != RemoteVariables.DefaultTheme)
			{
				num += 260;
				bttn[i].transform.localPosition = Vector3.right * num;
			}
		}
		bttn.Sort(delegate(ThemeItem x, ThemeItem y)
		{
			Vector3 localPosition = x.transform.localPosition;
			ref float x4 = ref localPosition.x;
			Vector3 localPosition2 = y.transform.localPosition;
			return x4.CompareTo(localPosition2.x);
		});
		int count = bttn.Count;
		distance = new float[count];
		Vector2 anchoredPosition = bttn[1].GetComponent<RectTransform>().anchoredPosition;
		float x2 = anchoredPosition.x;
		Vector2 anchoredPosition2 = bttn[0].GetComponent<RectTransform>().anchoredPosition;
		bttnDistance = (int)Mathf.Abs(x2 - anchoredPosition2.x);
		Vector3 position = bttn[1].transform.position;
		float x3 = position.x;
		Vector3 position2 = bttn[0].transform.position;
		realDistance = Mathf.Abs(x3 - position2.x);
	}

	private void Start()
	{
		int num = 0;
		while (true)
		{
			if (num < bttn.Count)
			{
				if (bttn[num].id == CoreData.GetSelectedTheme(null))
				{
					break;
				}
				num++;
				continue;
			}
			return;
		}
		SetSelectedItem(num);
	}

	private void Update()
	{
		if (!allowScroll)
		{
			return;
		}
		for (int i = 0; i < bttn.Count; i++)
		{
			float[] array = distance;
			int num = i;
			Vector3 position = center.transform.position;
			float x = position.x;
			Vector3 position2 = bttn[i].transform.position;
			array[num] = Mathf.Abs(x - position2.x);
		}
		float num2 = Mathf.Min(distance);
		for (int j = 0; j < bttn.Count; j++)
		{
			if (num2 == distance[j])
			{
				minButtonNum = j;
			}
		}
		if (!dragging)
		{
			LerpToBttn(minButtonNum * -bttnDistance);
		}
		float num3 = previewAnchored;
		Vector2 anchoredPosition = panel.anchoredPosition;
		if (num3 != anchoredPosition.x || previewAnchored == -1f)
		{
			SetAttributes(minButtonNum, distance[minButtonNum]);
			if (minButtonNum > 0)
			{
				SetAttributes(minButtonNum - 1, distance[minButtonNum - 1]);
			}
			if (minButtonNum < bttn.Count - 1)
			{
				SetAttributes(minButtonNum + 1, distance[minButtonNum + 1]);
			}
			Vector2 anchoredPosition2 = panel.anchoredPosition;
			previewAnchored = anchoredPosition2.x;
		}
	}

	private void SetAttributes(int i, float diff)
	{
		if (realDistance > 0f)
		{
			GameObject gameObject = bttn[i].gameObject;
			float num = diff / realDistance;
			float num2 = num / 3f;
			Vector3 localScale = new Vector3(1f, 1f, 1f);
			localScale.x = 1f - num2;
			localScale.y = localScale.x;
			gameObject.transform.localScale = localScale;
			bttn[i].SetColor(1f - num);
		}
		else
		{
			Vector3 position = bttn[1].transform.position;
			float x = position.x;
			Vector3 position2 = bttn[0].transform.position;
			realDistance = Mathf.Abs(x - position2.x);
		}
	}

	private void LerpToBttn(int position)
	{
		if (allowScroll)
		{
			Vector2 anchoredPosition = panel.anchoredPosition;
			float num = Mathf.Lerp(anchoredPosition.x, position, Time.deltaTime * 10f);
			float x = num;
			Vector2 anchoredPosition2 = panel.anchoredPosition;
			Vector2 anchoredPosition3 = new Vector2(x, anchoredPosition2.y);
			panel.anchoredPosition = anchoredPosition3;
		}
	}

	private void MoveToBttn(int position, float time = 0f)
	{
		if (allowScroll)
		{
			Vector2 anchoredPosition = panel.anchoredPosition;
			anchoredPosition.x = position;
			if (time == 0f)
			{
				panel.anchoredPosition = anchoredPosition;
			}
			else
			{
				StartCoroutine(Translation(panel.anchoredPosition, anchoredPosition, time));
			}
		}
	}

	public void StartDrag()
	{
		dragging = true;
		Vector2 anchoredPosition = panel.anchoredPosition;
		startDragAnchored = anchoredPosition.x;
		startDragTime = Time.time;
	}

	public void EndDrag()
	{
		Vector2 anchoredPosition = panel.anchoredPosition;
		float num = Mathf.Abs(anchoredPosition.x - startDragAnchored);
		if (Time.time - startDragTime < 0.3f && num > 1f && num < (float)(bttnDistance / 2))
		{
			Vector2 anchoredPosition2 = panel.anchoredPosition;
			if (anchoredPosition2.x - startDragAnchored > 0f)
			{
				SetSelectedItem(minButtonNum - 1, effect: true);
			}
			else
			{
				SetSelectedItem(minButtonNum + 1, effect: true);
			}
		}
		else
		{
			dragging = false;
		}
	}

	public void SetSelectedItem(int num, bool effect = false)
	{
		if (num >= 0 && num < bttn.Count)
		{
			minButtonNum = num;
			if (effect)
			{
				MoveToBttn(minButtonNum * -bttnDistance, 0.4f);
			}
			else
			{
				MoveToBttn(minButtonNum * -bttnDistance);
			}
		}
		else
		{
			dragging = false;
		}
	}

	public void MapClicked(int num)
	{
		if (allowScroll)
		{
			if (num != minButtonNum)
			{
				minButtonNum = num;
				MoveToBttn(num * -bttnDistance, clickSpeed);
			}
			else if (bttn[minButtonNum].status == THEME_STATUS.OPENED || bttn[minButtonNum].status == THEME_STATUS.CURRENT)
			{
				allowScroll = false;
				scrollRect.enabled = false;
			}
		}
	}

	private IEnumerator Translation(Vector3 startPos, Vector3 endPos, float value)
	{
		float rate = 1f / value;
		float t = 0f;
		while ((double)t < 1.0)
		{
			t += Time.deltaTime * rate;
			panel.anchoredPosition = Vector3.Lerp(startPos, endPos, Mathf.SmoothStep(0f, 1f, t));
			yield return null;
		}
	}

	public void NextBtn()
	{
		if (bttn.Count > minButtonNum + 1)
		{
			minButtonNum++;
			MoveToBttn(minButtonNum * -bttnDistance, clickSpeed);
		}
		else
		{
			MoveToBttn(minButtonNum * -bttnDistance - bttnDistance / 4, clickSpeed);
		}
	}

	public void PrevBtn()
	{
		if (minButtonNum - 1 >= 0)
		{
			minButtonNum--;
			MoveToBttn(minButtonNum * -bttnDistance, clickSpeed);
		}
		else
		{
			MoveToBttn(minButtonNum * -bttnDistance + bttnDistance / 4, clickSpeed);
		}
	}

	public void Close()
	{
		base.gameObject.SetActive(value: false);
		SoundManager.instance.PlayGameButton();
		HomeManager.instance.ShowHome();
	}

	public void UnlockTheme()
	{
		bttn[minButtonNum].UnlockTheme();
		bttn[minButtonNum].SelectTheme();
	}

	public void UnSelectTheme()
	{
		int num = 0;
		while (true)
		{
			if (num < bttn.Count)
			{
				if (bttn[num].id == CoreData.GetSelectedTheme(null))
				{
					break;
				}
				num++;
				continue;
			}
			return;
		}
		bttn[num].UnSelect();
	}
}
