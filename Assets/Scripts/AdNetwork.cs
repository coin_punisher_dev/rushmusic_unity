using UnityEngine;

public abstract class AdNetwork : MonoBehaviour
{
	public enum BANNER_STATUS
	{
		LOADING,
		LOADED,
		SHOWED,
		HIDE,
		FAILED
	}

	public BANNER_STATUS bannerStatus;

	public virtual void init()
	{
	}

	public virtual bool isAdVideoAvailable()
	{
		return true;
	}

	public virtual void cacheAdVideo()
	{
	}

	public virtual void showAdVideo()
	{
	}

	public virtual bool isInterstitialVideoAvailable()
	{
		return true;
	}

	public virtual void cacheInterstitialVideo()
	{
	}

	public virtual void showInterstitialVideo()
	{
	}

	public virtual void loadBanner()
	{
	}

	public virtual void showBanner()
	{
	}

	public virtual void hideBanner()
	{
	}
}
