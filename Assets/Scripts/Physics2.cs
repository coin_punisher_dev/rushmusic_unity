using UnityEngine;

public class Physics2 : MonoBehaviour
{
	public float timeScale = 1f;

	public float timeScaleRoot = 1f;

	private Vector3 playerPosition;

	public float vspeed;

	public float trustTime;

	private float speedUpTime;

	public Rigidbody rb;

	public ParticleSystem trailParticleSystem;

	private ParticleSystem.ColorOverLifetimeModule colorModule;

	private ParticleSystem.EmissionModule emiModule;

	private ParticleSystem.ShapeModule shapeModule;

	private ParticleSystem smokeParticleSystem;

	private ParticleSystem.MainModule smokeMainModule;

	private float beginTime;

	public int endlessMode;

	private Gradient trailGradient;

	private ParticleSystem.MinMaxGradient mmGradient;

	public GradientColorKey[] colorKeys;

	public GradientAlphaKey[] alphaKeys;

	public Light roundLight;

	private float maxRange = 2.2f;

	private float minRange = 1.6f;

	public float forwardSpeed;

	private void Awake()
	{
		if (trailParticleSystem != null)
		{
			smokeParticleSystem = trailParticleSystem.transform.GetChild(0).GetComponent<ParticleSystem>();
			shapeModule = smokeParticleSystem.shape;
			smokeMainModule = smokeParticleSystem.main;
			trailGradient = CoreData.instance.ballGradient[CoreData.GetSelectedBall()];
			colorModule = trailParticleSystem.colorOverLifetime;
			emiModule = trailParticleSystem.emission;
			mmGradient = colorModule.color;
			colorKeys = trailGradient.colorKeys;
			alphaKeys = trailGradient.alphaKeys;
		}
	}

	private void OnEnable()
	{
		if (GameController.controller.replayCount == 0)
		{
			UpdateTimeScale(1f);
			speedUpTime = 0f;
			timeScaleRoot = 1f;
			endlessMode = 0;
			trailParticleSystem.transform.position = base.transform.position;
			UpTrailerEffect(0f);
			emiModule.rateOverDistance = new ParticleSystem.MinMaxCurve(0f);
		}
		trustTime = 0f;
		Ball.b.Reset();
		beginTime = Time.time;
		trailParticleSystem.Clear();
		trailParticleSystem.Play();
	}

	public void SetEndlessMode()
	{
        Debug.LogError("TilesHop Physics2.SetEndlessMode endlessMode:" + endlessMode);
		Spawner.Intance.PlayMusic();
		trustTime = 0f;
		speedUpTime = 0f;
		beginTime = Time.time;
		timeScaleRoot = timeScale;
		endlessMode++;
		UpTrailerEffect(1f);
	}

	private void OnDisable()
	{
		if (trailParticleSystem != null)
		{
			trailParticleSystem.Stop();
			roundLight.range = 0f;
		}
	}

	public void UpTrailerEffect(float t)
	{
        //Debug.LogError("TilesHop Physics2.UpTrailerEffect t:" + t);
		emiModule.rateOverTime = new ParticleSystem.MinMaxCurve(Mathf.Lerp(30f, 50f, t));
		for (int i = 0; i < colorKeys.Length; i++)
		{
			colorKeys[i].color = Color.Lerp(Color.white, trailGradient.colorKeys[i].color, t);
		}
		for (int j = 0; j < alphaKeys.Length; j++)
		{
			alphaKeys[j].alpha = Mathf.Lerp(trailGradient.alphaKeys[j].alpha / 15f, trailGradient.alphaKeys[j].alpha, t);
		}
		mmGradient.gradient.colorKeys = colorKeys;
		mmGradient.gradient.alphaKeys = alphaKeys;
		colorModule.color = mmGradient;
		shapeModule.radius = Mathf.Lerp(0.01f, 0.3f, t);
		smokeMainModule.startLifetime = new ParticleSystem.MinMaxCurve(Mathf.Lerp(0.15f, 0.3f, t));
		roundLight.range = Mathf.Lerp(minRange, maxRange, t);
		roundLight.color = colorKeys[0].color;
		Ball.b.mr.material.SetColor("_EmissionColor", Color.Lerp(Color.black, trailGradient.colorKeys[colorKeys.Length - 1].color, t));
	}

	private void Update()
	{
		if (GameController.controller.game != 0 && Mathf.Approximately(Time.timeScale, 1f))
		{
			playerPosition = Ball.b.cTransform.position;
			playerPosition.z += forwardSpeed * Time.deltaTime;
			Ball.b.cTransform.position = playerPosition;
			if (trailParticleSystem != null)
			{
				trailParticleSystem.transform.position = Ball.b.cTransform.position;
			}
		}
	}

	public bool Jump()
	{
		if (
            !SuperpoweredSDK.instance.IsPlaying() && GameController.controller.score > RemoteVariables.Block_Total)
		{
			SetEndlessMode();
		}
		if (endlessMode == 0 && trailParticleSystem != null)
		{
			UpTrailerEffect(SuperpoweredSDK.instance.GetPosition() / SuperpoweredSDK.instance.musicLength);
		}
		else
		{
			Ball.b.mr.material.SetColor("_EmissionColor", trailGradient.colorKeys[colorKeys.Length - 1].color);
		}
		if (speedUpTime > 0f || timeScale > 1f)
		{
			UpdateTimeScale(GetTimeScale());
		}
		Vector3 velocity = rb.velocity;
		float jumpTimeNow = Spawner.Intance.GetCurrentJumpTime() / timeScale;
		trustTime += jumpTimeNow;
		float num2 = trustTime - (Time.time - beginTime);
        if (num2 < jumpTimeNow * 0.8f)
        {
            Debug.LogError("TilesHop Physical2.Jump (num2 < jumpTimeNow * 0.8f) num2:" + num2 + "/jumpTimeNow * 0.8f:" + (jumpTimeNow * 0.8f));

            trustTime -= jumpTimeNow;
            Spawner.Intance.DecreaseJumpNote();
            return false;
        }
        if (num2 > jumpTimeNow * 1.2f)
        {
            Debug.LogError("TilesHop Physical2.Jump (num2 > jumpTimeNow * 1.2f) num2:" + num2 + "/jumpTimeNow * 1.2f:" + (jumpTimeNow * 1.2f));
            trustTime -= jumpTimeNow;
            num2 /= 2f;
            Spawner.Intance.DecreaseJumpNote();
            return false;
        }
        Vector3 gravity = Physics.gravity;
		{
			gravity.y = (0f - 8f * RemoteVariables.JumpHeight) / (num2 * num2);
		}
		Physics.gravity = gravity;
		float num4 = num2 / 2f;
		Vector3 gravity2 = Physics.gravity;
		velocity.y = Mathf.Abs(num4 * gravity2.y);
		rb.velocity = velocity;
		Spawner.Intance.savedMusicTime = SuperpoweredSDK.instance.GetPosition();
		return true;
	}

	public float GetJumpTime()
	{
		return Spawner.Intance.jumpTime / timeScale;
	}

	private float GetGravity(float height, float t)
	{
		return -8f * height / (t * t);
	}

	private float GetHeight(float g, float t)
	{
		return g * t * t / 2f;
	}

	public void UpdateTimeScale(float scale)
	{
        //Debug.LogError("TilesHop Physical2.UpdateTimeScale timeScale:" + scale);
        timeScale = scale;
		{
			forwardSpeed = RemoteVariables.Block_Span / GetJumpTime();
		}
		SuperpoweredSDK.instance.SetSpeed(timeScale);
	}

	public void UpSpeed()
	{
		speedUpTime = Spawner.Intance.GetMusicTime();
	}

	private float GetTimeScale()
	{
        float num = Spawner.Intance.GetMusicTime() - speedUpTime;
		if (num >= 0f)
		{
			return timeScaleRoot + num / SuperpoweredSDK.instance.musicLength * (RemoteVariables.UpSpeedMax - 1f);
		}
        var time = timeScaleRoot + (RemoteVariables.UpSpeedMax - 1f);
        return time;
	}
}
