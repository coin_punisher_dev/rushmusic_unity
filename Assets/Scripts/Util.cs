using System;
using System.IO;
using System.Text;
using UnityEngine;

public class Util : MonoBehaviour
{
	public static void Move(GameObject obj, Vector3 movement)
	{
		obj.transform.position += movement;
	}

	public static void GoToGamePlay(string songID)
	{
		SceneFader.instance.FadeTo("Theme" + CoreData.GetSelectedTheme(songID).ToString(), songID);
	}

	public static int Rand(int max)
	{
		return (int)UnityEngine.Random.Range(0f, (float)max + 0.5f);
	}

	public static void ShowMessage(string msg, int setFunc = 0)
	{
		GameObject gameObject = ShowPopUp("MessagePopup");
		if (gameObject) gameObject.GetComponent<MessagePopup>().Set(msg, setFunc);
	}

	public static Material GetBallMaterial()
	{
		return Resources.Load<Material>("BallMaterials/" + (CoreData.GetSelectedBall() + 1));
	}

	public static Sprite GetThemeDisk(string songID)
	{
		return Resources.Load<Sprite>("ThemeDisk/" + CoreData.GetSelectedTheme(songID));
	}

	public static int RandomExclude(int start, int end, int exclude)
	{
		int[] array = new int[end - start - 1];
		int num = 0;
		for (int i = start; i < end; i++)
		{
			if (i != exclude)
			{
				array[num] = i;
				num++;
			}
		}
		return array[UnityEngine.Random.Range(0, array.Length)];
	}

	public static string SongToBoardId(string songId)
	{
		return songId.Replace(".mp3", string.Empty);
	}

	public static string BoardToSongId(string boardId)
	{
		if (boardId != "Total")
		{
			return boardId + ".mp3";
		}
		return boardId;
	}

	public static string GetAppUrl()
	{
		string empty = string.Empty;
		string text = "http://play.google.com/store/apps/details?id={0}";
		return text.Replace("{0}", Application.identifier);
	}

	public static Texture2D RoundTexture(Texture2D sourceTex)
	{
		int height = sourceTex.height;
		int width = sourceTex.width;
		float num = -1 + sourceTex.height / 2;
		float num2 = -1 + sourceTex.height / 2;
		float num3 = -1 + sourceTex.width / 2;
		Color[] pixels = sourceTex.GetPixels(0, 0, sourceTex.width, sourceTex.height);
		Texture2D texture2D = new Texture2D(height, width);
		for (int i = 0; i < height * width; i++)
		{
			int num4 = Mathf.FloorToInt((float)i / (float)width);
			int num5 = Mathf.FloorToInt((float)i - (float)(num4 * width));
			if (num * num >= ((float)num5 - num2) * ((float)num5 - num2) + ((float)num4 - num3) * ((float)num4 - num3))
			{
				texture2D.SetPixel(num5, num4, pixels[i]);
			}
			else
			{
				texture2D.SetPixel(num5, num4, Color.clear);
			}
		}
		texture2D.Apply();
		return texture2D;
	}

	public static GameObject ShowPopUp(string name)
	{
		Transform transform = null;
		if (HomeManager.instance != null)
		{
			transform = HomeManager.instance.transform;
		}
		else if (UIController.ui != null)
		{
			transform = UIController.ui.transform;
		}
		if (transform != null)
		{
			GameObject original = Resources.Load<GameObject>("Prefabs/" + name);
			return UnityEngine.Object.Instantiate(original, transform);
		}
		return null;
	}

	public static void RateUs()
	{
		SoundManager.instance.PlayGameButton();
		string appUrl = GetAppUrl();
		if (!string.IsNullOrEmpty(appUrl))
		{
			Application.OpenURL(appUrl);
		}
		PlayerPrefs.SetInt("RateLater", 99);
		AnalyticHelper.ButtonClick(FIRE_EVENT.Button_Rate_Us);
	}

	//public static string GetAndroidExternalStoragePath()
	//{
 //       //return SuperpoweredSDK.instance.AndroidSdCardPath();
 //       return AndroidNativeFunctions.GetAndroidExternalFilesDir();
	//}

	public static string Base64Encode(string plainText)
	{
		byte[] bytes = Encoding.UTF8.GetBytes(plainText);
		return Convert.ToBase64String(bytes);
	}

	public static string Base64Decode(string base64EncodedData)
	{
		byte[] bytes = Convert.FromBase64String(base64EncodedData);
		return Encoding.UTF8.GetString(bytes);
	}

	public static string FileNameToTitle(string name)
	{
		string text = name
            //.Replace(".mp3", string.Empty).Replace(".MP3", string.Empty)
            .Replace("-", " ")
			.Replace("_", " ")
			.Replace(",", " ");
		if (text.Length > 22)
		{
			text = text.Remove(22) + "...";
		}
		return text;
	}

	public static string GetParentFolder(string folder)
	{
		DirectoryInfo directoryInfo = new DirectoryInfo(folder);
		if (directoryInfo != null && directoryInfo.Parent != null)
		{
			return directoryInfo.Parent.FullName;
		}
		return null;
	}

	public static bool CanReadDirectory(string path)
	{
		if (Directory.Exists(path))
		{
			try
			{
				DirectoryInfo directoryInfo = new DirectoryInfo(path);
				directoryInfo.GetDirectories();
				return true;
			}
			catch
			{
				return false;
			}
		}
		return false;
	}

	public static Vector3 SetPositionY(Vector3 position, float y)
	{
		position.y = y;
		return position;
	}

	public static Vector3 SetPositionX(Vector3 position, float x)
	{
		position.x = x;
		return position;
	}

	public static Vector3 SetPositionZ(Vector3 position, float z)
	{
		position.z = z;
		return position;
	}

	public static Vector3 SetPositionXY(Vector3 position, float x, float y)
	{
		position.x = x;
		position.y = y;
		return position;
	}

	public static void UpdateBannerContent(Transform transform)
	{
		RectTransform component = transform.Find("ScrollView").GetComponent<RectTransform>();
		if (AdsManager.instance.GetBannerStatus() != AdNetwork.BANNER_STATUS.FAILED)
		{
			RectTransform rectTransform = component;
			Vector2 offsetMin = component.offsetMin;
			rectTransform.offsetMin = new Vector2(offsetMin.x, 80f);
		}
		else
		{
			RectTransform rectTransform2 = component;
			Vector2 offsetMin2 = component.offsetMin;
			rectTransform2.offsetMin = new Vector2(offsetMin2.x, 0f);
		}
	}

	public static void ShowNeedMore(int x)
	{
		GameObject gameObject = ShowPopUp("Shop");
		gameObject.GetComponent<Shop>().SetNeedMore(x);
	}
}
