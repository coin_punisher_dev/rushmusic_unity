public interface GameSubscriber
{
	void gameStart();

	void gameContinue();

	void gameOver();

	void onGame();

	void offGame();
}
