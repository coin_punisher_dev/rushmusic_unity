using UnityEngine;

public class HitShadowAnim : MonoBehaviour
{
	private float t = 1f;

	private float time = 0.1f;

	private Vector3 start = new Vector3(0.3f, 0.3f, 1f);

	private Vector3 end = new Vector3(0.57f, 0.57f, 1f);

	public void ActiveAnim()
	{
		t = 0f;
		base.transform.localScale = start;
		base.gameObject.SetActive(value: true);
	}

	private void Update()
	{
		if (t < time)
		{
			t += Time.deltaTime;
			base.transform.localScale = Vector3.Lerp(start, end, t / time);
		}
	}
}
