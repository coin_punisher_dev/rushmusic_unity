using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
#if ENABLE_UTNOTIFICATION
using UTNotifications;
#endif

#if UNITY_ANDROID
using UnityEngine.Android;
#endif

public class SongList : MonoBehaviour
{
    public static SongList instance;

    public RectTransform container;

    public GameObject openedItem;

    public GameObject upButton;

    public GameObject lineObj;

    public GameObject lockedItem;

    public GameObject videoItem;

    public GameObject pushItem;

    public GameObject facebookButton;

    public Transform currentTransform;

    public Dictionary<string, GameObject> items = new Dictionary<string, GameObject>();

    public Dictionary<string, GameObject> localItems = new Dictionary<string, GameObject>();

    public GameObject localItem;

    private float positionY;

    public static int localItemPrice = 50;

    public static int freeAdding = 1;

    private Item importingSong;

    public int buttonType;

    //private const string STORAGE_PERMISSION = "android.permission.READ_EXTERNAL_STORAGE";

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        Util.UpdateBannerContent(base.transform);
        BuidListNew();
    }

    public void ClearAllItems()
    {
        foreach (GameObject value in localItems.Values)
        {
            UnityEngine.Object.Destroy(value);
        }
        foreach (GameObject value2 in items.Values)
        {
            UnityEngine.Object.Destroy(value2);
        }
        RemoteFilesData.instance.songList.Clear();
        RemoteFilesData.instance.localSongList.Clear();
    }

    public void BuidListNew()
    {
        container.anchoredPosition = Vector2.zero;
        positionY = 0f;
        Dictionary<string, Item> localSongList = RemoteFilesData.instance.localSongList;
        foreach (GameObject value in localItems.Values)
        {
            UnityEngine.Object.Destroy(value);
        }
        localItems.Clear();
        facebookButton.SetActive(value: false);
        //ShowUploadButton();
        foreach (Item value2 in localSongList.Values)
        {
            SONGTYPE songType = CoreData.GetSongType(value2.path, value2.type);
            GameObject gameObject = null;
            gameObject = UnityEngine.Object.Instantiate(localItem);
            gameObject.GetComponent<SongItem>().SetSong(songType, value2);
            gameObject.transform.SetParent(facebookButton.transform.parent, worldPositionStays: false);
            if (base.gameObject.activeInHierarchy)
            {
                StartCoroutine(DelayUpdate(gameObject, facebookButton.transform.localPosition + Vector3.down * positionY));
            }
            else
            {
                gameObject.SetActive(value: true);
                gameObject.transform.localPosition = facebookButton.transform.localPosition + Vector3.down * positionY;
            }
            localItems[value2.path] = gameObject;
            positionY += 105f;
        }
        if (RemoteFilesData.instance.localSongList.Count > 0)
        {
            if (base.gameObject.activeInHierarchy)
            {
                StartCoroutine(DelayUpdate(lineObj, facebookButton.transform.localPosition + Vector3.down * positionY));
            }
            else
            {
                lineObj.SetActive(value: true);
                lineObj.transform.localPosition = facebookButton.transform.localPosition + Vector3.down * positionY;
            }
            positionY += 25f;
            lineObj.SetActive(value: true);
        }
        else
        {
            lineObj.SetActive(value: false);
        }
        localSongList = RemoteFilesData.instance.songList;
        bool flag = false;
        string @string = PlayerPrefs.GetString("first_open_time");
        string productName = Application.productName;
        string localizedValue = LocalizationManager.instance.GetLocalizedValue("PUSH_5");
        bool flag2 = false;
        if (string.IsNullOrEmpty(@string))
        {
            PlayerPrefs.SetString("first_open_time", DateTime.Now.ToString());
#if ENABLE_UTNOTIFICATION
			Manager.Instance.ScheduleNotification((int)(86400f * RemoteVariables.PushNewSongDate), productName, localizedValue, 5);
#endif
        }
        else if (DateTime.Parse(@string) < DateTime.Now.AddHours(-24f * RemoteVariables.PushNewSongDate))
        {
            flag = true;
            flag2 = true;
        }
        foreach (Item value3 in localSongList.Values)
        {
            SONGTYPE sONGTYPE = CoreData.GetSongType(value3.path, value3.type);
            if (sONGTYPE == SONGTYPE.HIDE)
            {
                if (!flag)
                {
                    if (flag2)
                    {
#if ENABLE_UTNOTIFICATION
						Manager.Instance.CancelNotification(5);
						Manager.Instance.ScheduleNotification((int)(86400f * RemoteVariables.PushNewSongDate), productName, localizedValue, 5);
#endif
                        PlayerPrefs.SetString("first_open_time", DateTime.Now.ToString());
                        flag2 = false;
                    }
                    continue;
                }
                StartCoroutine(ScrollToAnchoredPosition(0.7f, Vector2.up * positionY));
                sONGTYPE = SONGTYPE.PUSH;
                PlayerPrefs.SetString(value3.path, sONGTYPE.ToString());
                PlayerPrefs.SetString("first_open_time", DateTime.Now.ToString());
                flag = false;
            }
            if (UnlockPushSong.songid == value3.path)
            {
                Util.ShowPopUp("UnlockPushSong");
                StartCoroutine(ScrollToAnchoredPosition(0f, Vector2.up * positionY));
                AnalyticHelper.FireString("PushNewSong");
            }
            GameObject gameObject2 = null;
            if (items.ContainsKey(value3.path) && items[value3.path] != null)
            {
                if (items[value3.path].GetComponent<SongItem>().type == sONGTYPE)
                {
                    gameObject2 = items[value3.path];
                }
                else
                {
                    UnityEngine.Object.Destroy(items[value3.path]);
                }
            }
            if (gameObject2 == null)
            {
                switch (sONGTYPE)
                {
                    case SONGTYPE.VIDEO:
                        gameObject2 = UnityEngine.Object.Instantiate(videoItem);
                        break;
                    case SONGTYPE.OPEN:
                        gameObject2 = UnityEngine.Object.Instantiate(openedItem);
                        break;
                    case SONGTYPE.PUSH:
                        gameObject2 = UnityEngine.Object.Instantiate(pushItem);
                        break;
                    default:
                        gameObject2 = UnityEngine.Object.Instantiate(lockedItem);
                        break;
                }
            }
            gameObject2.GetComponent<SongItem>().SetSong(sONGTYPE, value3);
            gameObject2.transform.SetParent(facebookButton.transform.parent, worldPositionStays: false);
            if (base.gameObject.activeInHierarchy)
            {
                StartCoroutine(DelayUpdate(gameObject2, facebookButton.transform.localPosition + Vector3.down * positionY));
            }
            else
            {
                gameObject2.SetActive(value: true);
                gameObject2.transform.localPosition = facebookButton.transform.localPosition + Vector3.down * positionY;
            }
            items[value3.path] = gameObject2;
            positionY += 105f;
        }
        container.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, positionY + 450f);
    }

    private IEnumerator DelayUpdate(GameObject item, Vector3 localPosition, bool active = true)
    {
        yield return null;
        if (item != null)
        {
            if (active)
            {
                item.SetActive(value: true);
            }
            item.transform.localPosition = localPosition;
        }
    }

    private void ShowUploadButton()
    {
        GameObject gameObject = localItem.transform.parent.Find("Button1").gameObject;
        GameObject gameObject2 = localItem.transform.parent.Find("Button2").gameObject;
        GameObject gameObject3 = localItem.transform.parent.Find("Button3").gameObject;
        if (facebookButton.activeSelf)
        {
            positionY += 102f;
        }
        if (base.gameObject.activeInHierarchy)
        {
            StartCoroutine(DelayUpdate(gameObject, facebookButton.transform.localPosition + Vector3.down * positionY, active: false));
        }
        else
        {
            gameObject.transform.localPosition = facebookButton.transform.localPosition + Vector3.down * positionY;
        }
        if (base.gameObject.activeInHierarchy)
        {
            StartCoroutine(DelayUpdate(gameObject2, facebookButton.transform.localPosition + Vector3.down * positionY, active: false));
        }
        else
        {
            gameObject2.transform.localPosition = facebookButton.transform.localPosition + Vector3.down * positionY;
        }
        if (RemoteFilesData.instance.localSongList.Count < freeAdding && Configuration.FreeAddSongIsOn())
        {
            gameObject.SetActive(value: true);
            gameObject2.SetActive(value: false);
            gameObject3.SetActive(value: false);
        }
        else if (RemoteFilesData.instance.localSongList.Count < freeAdding + RemoteVariables.Video_LocalSong || CoreData.instance.GetDiamonds() < localItemPrice)
        {
            gameObject.SetActive(value: false);
            gameObject2.SetActive(value: true);
            gameObject3.SetActive(value: false);
        }
        else
        {
            if (base.gameObject.activeInHierarchy)
            {
                StartCoroutine(DelayUpdate(gameObject3, facebookButton.transform.localPosition + Vector3.down * positionY));
            }
            else
            {
                gameObject3.transform.localPosition = facebookButton.transform.localPosition + Vector3.down * positionY;
            }
            gameObject.SetActive(value: false);
            gameObject2.SetActive(value: false);
            gameObject3.SetActive(value: true);
        }
        positionY += 110f;
    }

    public void ScrollChange()
    {
        Vector2 anchoredPosition = container.anchoredPosition;
        if (anchoredPosition.y > 350f)
        {
            if (!upButton.activeSelf)
            {
                Vector3 localPosition = upButton.transform.localPosition;
                localPosition.y = 100f;
                upButton.transform.localPosition = localPosition;
                iTween.MoveTo(upButton, iTween.Hash("y", 0, "time", 0.3f, "delay", 0f, "easetype", "easeOutExpo", "islocal", true));
                upButton.SetActive(value: true);
            }
        }
        else
        {
            upButton.SetActive(value: false);
        }
    }

    public void ScrollToTop()
    {
        StartCoroutine(ScrollToAnchoredPosition(0f, Vector2.zero));
    }

    private IEnumerator ScrollToAnchoredPosition(float delay, Vector2 des)
    {
        base.transform.Find("ScrollView").GetComponent<ScrollRect>().StopMovement();
        yield return new WaitForSeconds(delay);
        float t = 0f;
        float time = 0.3f;
        Vector2 from = container.anchoredPosition;
        while (t < time)
        {
            t += Time.deltaTime;
            container.anchoredPosition = Vector2.Lerp(from, des, t / time);
            yield return null;
        }
        container.anchoredPosition = des;
    }

    public void ReloadSongs()
    {
        BuidListNew();
    }

    public void DisableFacebookButton()
    {
        Configuration.instance.showFacebookLogin = false;
        BuidListNew();
    }

    public void OpenSong(Transform source)
    {
        string songID = source.GetComponent<SongItem>().songID;
        CoreData.instance.SetOpenSong(songID);
        GameObject openItemClone = UnityEngine.Object.Instantiate(openedItem);
        openItemClone.GetComponent<SongItem>().SetSong(SONGTYPE.OPEN, RemoteFilesData.instance.songList[songID]);
        openItemClone.SetActive(value: true);
        openItemClone.transform.SetParent(openedItem.transform.parent, worldPositionStays: false);
        openItemClone.transform.localPosition = source.localPosition;
        Destroy(source.gameObject);
    }

    public void OpenSongByID(string songId)
    {
        if (items.ContainsKey(songId) && items[songId] != null)
        {
            OpenSong(items[songId].transform);
        }
    }

    public void UploadButton_Click(int type)
    {
        buttonType = type;
        SoundManager.instance.PlayGameButton();
        switch (type)
        {
            case 1:
                PrepareShowFileSelector();
                return;
            case 2:
                AdsManager.instance.ShowRewardAds(VIDEOREWARD.UPLOADSONG);
                return;
        }
        if (CoreData.instance.GetDiamonds() >= localItemPrice)
        {
            PrepareShowFileSelector();
        }
        else
        {
            Util.ShowNeedMore(localItemPrice - CoreData.instance.GetDiamonds());
        }
    }

    Coroutine _coroutineSelectFile = null;
    public void PrepareShowFileSelector(float waitTime = 0f)
    {
        if (_coroutineSelectFile != null)
        {
            StopCoroutine(_coroutineSelectFile);
            _coroutineSelectFile = null;
        }

        _coroutineSelectFile = StartCoroutine(ShowFileSelector(waitTime));
    }

    FileSelector _fileSelectorPopup = null;
    private IEnumerator ShowFileSelector(float waitTime)
    {
        if (FileSelector.instance != null)
        {
            yield break;
            //_fileSelectorPopup = FileSelector.instance;
        }
        _fileSelectorPopup = Util.ShowPopUp("FileSelector").GetComponent<FileSelector>();
        FileSelector.instance = _fileSelectorPopup;
        yield return new WaitForSeconds(waitTime);
        if (Application.platform == RuntimePlatform.Android)
        {
            //if (!AndroidPermissionsManager.IsPermissionGranted("android.permission.READ_EXTERNAL_STORAGE"))
            //{
            //    AndroidPermissionsManager.RequestPermission(new string[1]
            //    {
            //        "android.permission.READ_EXTERNAL_STORAGE"
            //    }, new AndroidPermissionCallback(delegate
            //    {
            //        popup.SetActive(value: true);
            //    }, delegate
            //    {
            //        UnityEngine.Object.Destroy(popup);
            //    }));
            //}
            //else
            //{
            //    popup.SetActive(value: true);
            //}

            //    if (!Permission.HasUserAuthorizedPermission(Permission.ExternalStorageRead))
            //    {
            //        Permission.RequestUserPermission(Permission.ExternalStorageRead);
            //        yield return new WaitUntil(() => _fileSelectorPopup && Permission.HasUserAuthorizedPermission(Permission.ExternalStorageRead));

            //        if (_fileSelectorPopup) _fileSelectorPopup.gameObject.SetActive(true);
            //    }
            //    else
            //    {
            //        _fileSelectorPopup.gameObject.SetActive(value: true);
            //    }
            //}
            //else
            //{
            //    _fileSelectorPopup.gameObject.SetActive(value: true);
            //}
        }
    }
}