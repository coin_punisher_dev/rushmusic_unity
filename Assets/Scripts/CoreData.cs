using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CoreData : MonoBehaviour
{
	public static CoreData instance;

	public Gradient[] ballGradient;

	private int diamonds = 15;

	public List<AchievementObj> achievementList;

	private int[] themeArray;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
			UnityEngine.Object.DontDestroyOnLoad(base.gameObject);
		}
		else if (instance != null)
		{
			UnityEngine.Object.Destroy(base.gameObject);
		}
	}

	private void Start()
	{
		RemoteFilesData.instance.onSongListLoaded += UpdateAchievement;
		diamonds = PlayerPrefs.GetInt("diamonds", 15);
		if (Configuration.instance.isDebug)
		{
			instance.SetDiamonds(2000);
		}
	}

	public void UpdateAchievement()
	{
		for (int i = 0; i < instance.achievementList.Count; i++)
		{
			instance.achievementList[i].UpdateStatus(forceChange: true);
		}
	}

	public Item GetCurrentSong()
	{
		string @string = PlayerPrefs.GetString("current_song", null);
		if (RemoteFilesData.instance.localSongList.ContainsKey(@string))
		{
			return RemoteFilesData.instance.localSongList[@string];
		}
		if (!RemoteFilesData.instance.songList.ContainsKey(@string))
		{
			foreach (Item value in RemoteFilesData.instance.songList.Values)
			{
				if (value.type == SONGTYPE.OPEN.ToString())
				{
					return value;
				}
			}
			using (Dictionary<string, Item>.ValueCollection.Enumerator enumerator2 = RemoteFilesData.instance.localSongList.Values.GetEnumerator())
			{
				if (enumerator2.MoveNext())
				{
					return enumerator2.Current;
				}
			}
		}
		return RemoteFilesData.instance.songList[@string];
	}

	public static void SetCurrentSong(string id)
	{
		PlayerPrefs.SetString("current_song", id);
	}

	public static int GetBestScore(string id)
	{
		return PlayerPrefs.GetInt("bestscore" + id, 0);
	}

	public static int GetSongVersion(string id)
	{
		return PlayerPrefs.GetInt("v" + id, 0);
	}

	public static void SetSongVersion(string id, int version)
	{
		PlayerPrefs.SetInt("v" + id, version);
	}

	public static void SetBestStars(int stars, string id)
	{
		PlayerPrefs.SetInt("beststars" + id, stars);
	}

	public static int GetBestStars(string id)
	{
		return PlayerPrefs.GetInt("beststars" + id, 0);
	}

	public static void SetBestScore(int score, string songid)
	{
		PlayerPrefs.SetInt("bestscore" + songid, score);
	}

	public int GetDiamonds()
	{
		return diamonds;
	}

	public void IncreaseDiamonds(int change)
	{
		diamonds += change;
		PlayerPrefs.SetInt("diamonds", diamonds);
	}

	public void SetDiamonds(int amount)
	{
		diamonds = amount;
		PlayerPrefs.SetInt("diamonds", amount);
	}

	public static bool IsOpenBall(int id)
	{
		return RemoteVariables.UnlockAllSongs == 1 || id == 0 || PlayerPrefs.GetString("balls", string.Empty).Contains("," + id.ToString() + ",");
	}

	public static void SetOpenBall(int id, bool achievement = true)
	{
		if (!IsOpenBall(id))
		{
			AnalyticHelper.Item_Buy(Buy_Type.BALL, id.ToString(), RemoteVariables.BallPrice, id.ToString());
			PlayerPrefs.SetString("balls", PlayerPrefs.GetString("balls", string.Empty) + "," + id.ToString() + ",");
			if (achievement)
			{
				AchievementObj.CheckTypes(new AchievementType[3]
				{
					AchievementType.UNLOCK_BALL_5,
					AchievementType.UNLOCK_BALL_10,
					AchievementType.UNLOCK_BALL_ALL
				});
			}
		}
	}

	public static int GetSelectedBall()
	{
		return PlayerPrefs.GetInt("selected_ball", 0);
	}

	public static void SetSelectedBall(int id)
	{
		if (GetSelectedBall() != id)
		{
			AnalyticHelper.Item_Usage(Buy_Type.SONG, id.ToString());
		}
		PlayerPrefs.SetInt("selected_ball", id);
	}

	public static bool IsOpenTheme(int id)
	{
		return RemoteVariables.UnlockAllSongs == 1 || id == RemoteVariables.DefaultTheme || id == 0 || PlayerPrefs.GetString("themes", string.Empty).Contains("," + id.ToString() + ",");
	}

	public static void SetOpenTheme(int id, bool achievement = true)
	{
		if (!IsOpenTheme(id))
		{
			PlayerPrefs.SetString("themes", PlayerPrefs.GetString("themes", string.Empty) + "," + id.ToString() + ",");
			if (achievement)
			{
				AchievementObj.CheckTypes(new AchievementType[2]
				{
					AchievementType.UNLOCK_THEME_2,
					AchievementType.UNLOCK_THEME_ALL
				});
			}
		}
	}

	public static int GetSelectedTheme(string songID)
	{
		if (Configuration.instance.forceTheme >= 0)
		{
			return Configuration.instance.forceTheme;
		}
		if (!PlayerPrefs.HasKey(songID + "selected_theme"))
		{
			int num = UnityEngine.Random.Range(0, instance.GetThemeArray().Length);
			PlayerPrefs.SetInt(songID + "selected_theme", instance.GetThemeArray()[num]);
			return instance.GetThemeArray()[num];
		}
		int @int = PlayerPrefs.GetInt(songID + "selected_theme", PlayerPrefs.GetInt("selected_theme", RemoteVariables.DefaultTheme));
		if (instance.GetThemeArray().Contains(@int))
		{
			return @int;
		}
		return instance.GetThemeArray()[0];
	}

	public void SetSelectedThemeBySong(string songID, bool isLocal = false)
	{
		int num = 0;
		int num2 = SongUnlockCount();
		num = ((isLocal && num2 > GetThemeArray().Length * 2) ? UnityEngine.Random.Range(0, GetThemeArray().Length) : ((num2 - 1) % (GetThemeArray().Length * 2) / 2));
		PlayerPrefs.SetInt(songID + "selected_theme", GetThemeArray()[num]);
	}

	public void SetSelectedTheme(int id)
	{
		if (GetSelectedTheme(null) != id)
		{
			AnalyticHelper.ChangeTheme(id + 1);
		}
		PlayerPrefs.SetInt("selected_theme", id);
	}

	public void SetOpenSong(string id, bool achievement = true)
	{
		if (PlayerPrefs.GetString(id) != SONGTYPE.OPEN.ToString())
		{
			Item item = RemoteFilesData.instance.songList[id];
			if (item.type == SONGTYPE.LOCK.ToString())
			{
				AnalyticHelper.Item_Buy(Buy_Type.SONG, item.name, item.diamonds, id);
			}
			else
			{
				AnalyticHelper.Item_Buy(Buy_Type.SONG, item.name, 0f, id);
			}
			PlayerPrefs.SetString(id, SONGTYPE.OPEN.ToString());
			PlayerPrefs.SetInt("SongUnlockCount", SongUnlockCount() + 1);
			if (achievement)
			{
				AchievementObj.CheckTypes(new AchievementType[3]
				{
					AchievementType.UNLOCK_SONG_5,
					AchievementType.UNLOCK_SONG_10,
					AchievementType.UNLOCK_SONG_ALL
				});
			}
			AppsFlyerInit.TrackLvl();
			SetSelectedThemeBySong(id);
		}
	}

	public static bool CheckNewChallenge()
	{
		for (int i = 0; i < instance.achievementList.Count; i++)
		{
			if (instance.achievementList[i].isNewChallenge())
			{
				return instance.achievementList[i].status == AchievementStatus.RECEIVED || instance.achievementList[i].status == AchievementStatus.UNLOCK;
			}
		}
		return false;
	}

	public static SONGTYPE GetSongType(string id, string def)
	{
		if (RemoteVariables.UnlockAllSongs == 0)
		{
			string text = PlayerPrefs.GetString(id, def);
			if (text == string.Empty)
			{
				text = def;
			}
			return (SONGTYPE)Enum.Parse(typeof(SONGTYPE), text);
		}
		return SONGTYPE.OPEN;
	}

	public int[] GetThemeArray()
	{
		if (themeArray == null || themeArray.Length != Configuration.instance.totalThemes)
		{
			string @string = PlayerPrefs.GetString("ThemeArray");
			if (string.IsNullOrEmpty(@string))
			{
				themeArray = new int[Configuration.instance.totalThemes];
				for (int i = 0; i < Configuration.instance.totalThemes; i++)
				{
					themeArray[i] = i;
				}
				for (int j = 0; j < themeArray.Length; j++)
				{
					themeArray[j] = j;
					int num = themeArray[j];
					int num2 = UnityEngine.Random.Range(j, themeArray.Length);
					themeArray[j] = themeArray[num2];
					themeArray[num2] = num;
				}
				string text = string.Empty;
				for (int k = 0; k < themeArray.Length; k++)
				{
					text += themeArray[k].ToString();
					if (k != themeArray.Length - 1)
					{
						text += ",";
					}
				}
				PlayerPrefs.SetString("ThemeArray", text);
			}
			else
			{
				string[] array = @string.Split(',');
				themeArray = new int[array.Length];
				for (int l = 0; l < array.Length; l++)
				{
					themeArray[l] = int.Parse(array[l]);
				}
			}
		}
		return themeArray;
	}

	public static int SongUnlockCount()
	{
		int num = PlayerPrefs.GetInt("SongUnlockCount", 0);
		if (num == 0)
		{
			foreach (Item value in RemoteFilesData.instance.songList.Values)
			{
				if (GetSongType(value.path, value.type) == SONGTYPE.OPEN)
				{
					num++;
				}
			}
			PlayerPrefs.SetInt("SongUnlockCount", num);
		}
		return num;
	}
}
