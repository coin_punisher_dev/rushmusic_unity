using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BallList : MonoBehaviour
{
	public Sprite selected;

	public Sprite normal;

	public Sprite lockVideo;

	public Sprite lockNormal;

	public GameObject[] balls;

	public GameObject ballContainer;

	public GameObject videoBtn;

	public GameObject purchaseBtn;

	public GameObject selectedBtn;

	public Text priceText;

	public static BallList instance;

	private int selectedid;

	public static List<int> videoItems = new List<int>
	{
		/*6,
		7,
		10,
		15,
		20*/
	};

	private void Awake()
	{
		instance = this;
		balls = new GameObject[ballContainer.transform.childCount];
		for (int i = 0; i < ballContainer.transform.childCount; i++)
		{
			balls[i] = ballContainer.transform.GetChild(i).gameObject;
		}
	}

	private void Start()
	{
		priceText.text = RemoteVariables.BallPrice.ToString();
	}

	private void OnEnable()
	{
		balls[selectedid].GetComponent<Image>().sprite = normal;
		selectedid = CoreData.GetSelectedBall();
		for (int i = 0; i < balls.Length; i++)
		{
			if (CoreData.IsOpenBall(i))
			{
				balls[i].transform.Find("Lock").gameObject.SetActive(value: false);
				if (selectedid == i)
				{
					balls[i].GetComponent<Image>().sprite = selected;
				}
			}
			else if (videoItems.Contains(i))
			{
				balls[i].transform.Find("Lock").GetComponent<Image>().sprite = lockVideo;
			}
		}
	}

	public void Close()
	{
		SoundManager.instance.PlayGameButton();
		base.gameObject.SetActive(value: false);
		HomeManager.instance.ShowHome();
	}

	public void Click(int id)
	{
		SoundManager.instance.PlaySelect();
		balls[selectedid].GetComponent<Image>().sprite = normal;
		balls[id].GetComponent<Image>().sprite = selected;
		if (!CoreData.IsOpenBall(id))
		{
			selectedBtn.SetActive(value: false);
			if (videoItems.Contains(id))
			{
				videoBtn.SetActive(value: true);
				purchaseBtn.SetActive(value: false);
			}
			else
			{
				videoBtn.SetActive(value: false);
				purchaseBtn.SetActive(value: true);
			}
		}
		else if (selectedid != id)
		{
			videoBtn.SetActive(value: false);
			purchaseBtn.SetActive(value: false);
			CoreData.SetSelectedBall(id);
		}
		else
		{
			Close();
		}
		selectedid = id;
	}

	public void UnlockBall()
	{
		balls[selectedid].transform.Find("Lock").gameObject.SetActive(value: false);
		CoreData.SetOpenBall(selectedid);
		CoreData.SetSelectedBall(selectedid);
		videoBtn.SetActive(value: false);
		purchaseBtn.SetActive(value: false);
	}

	public void PayDiamonds()
	{
		if (CoreData.instance.GetDiamonds() >= RemoteVariables.BallPrice)
		{
			TopBar.instance.UpdateDiamond(-RemoteVariables.BallPrice);
			UnlockBall();
		}
		else
		{
			Util.ShowNeedMore(RemoteVariables.BallPrice - CoreData.instance.GetDiamonds());
		}
	}

	public void PlayVideo()
	{
		SoundManager.instance.PlayGameButton();
		AdsManager.instance.ShowRewardAds(VIDEOREWARD.UNLOCKBALL);
	}
}
