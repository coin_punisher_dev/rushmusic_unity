using UnityEngine;
using UnityEngine.UI;

public class LocalizedText : MonoBehaviour
{
	public string key;

	private Text text;

	private void Awake()
	{
		text = GetComponent<Text>();
	}

	private void LanguageHasChanged()
	{
        if (LocalizationManager.instance)
        {
            var str = LocalizationManager.instance.GetLocalizedValue(key);
            text.text = str;
        }
	}

    private void OnEnable()
    {
		LocalizationManager.OnLanguageChange += LanguageHasChanged;
        LanguageHasChanged();
    }

    private void OnDisable()
    {
	    LocalizationManager.OnLanguageChange -= LanguageHasChanged;
    }
}
