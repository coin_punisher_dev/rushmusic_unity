using UnityEngine;
using UnityEngine.UI;

public class MessagePopup : PopupUI
{
	public Text message;

	private int func;

	public static MessagePopup instance;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		else if (instance != null)
		{
			UnityEngine.Object.Destroy(base.gameObject);
		}
	}

	public void Set(string msg, int setFunc = 0)
	{
		message.text = msg;
		base.gameObject.SetActive(value: true);
		func = setFunc;
	}

	public new void Close()
	{
		SoundManager.instance.PlayGameButton();
		if (func == 1)
		{
			if (HomeManager.instance == null)
			{
				SceneFader.instance.FadeTo("Home");
			}
			else
			{
				HomeManager.instance.ShowHome();
			}
		}
		else if (func == 2)
		{
			UIController.ui.gameover.SetActive(value: true);
		}
		if (UIController.ui != null && UIController.ui.continueui.activeSelf)
		{
			UIController.ui.continueui.GetComponent<ContinueUI>().Cancel();
		}
		StartCoroutine(HidePopup());
	}
}
