using System;
using UnityEngine;

public class Ball : MonoBehaviour
{
	public Physics2 ph;

	public MeshRenderer mr;

	public static Ball b;

	private int perfectCount;

	public ParticleSystem brokenEffect;

	public Transform cTransform;

	private void Awake()
	{
		cTransform = base.transform;
		b = this;
		mr.material = Util.GetBallMaterial();
	}

	private void OnTriggerEnter(Collider col)
	{
        try
        {
            if (GameController.controller.game != GameStatus.LIVE)
            {
                return;
            }
            //Debug.LogError("TilesHop OnTriggerEnter tag:" + col.tag);
            if (col.CompareTag(GAMETAG.Block))
            {
                var isJump = ph.Jump();
                if (!isJump)
                {
                    return;
                }
                //Debug.LogError("TilesHop Ball.OnTriggerEnter name:" + col.transform.parent.gameObject.name);
                Transform xtransform = cTransform;
                Vector3 posBall = cTransform.position;
                float xBall = posBall.x;
                Vector3 posBlock = col.gameObject.transform.position;
                xtransform.position = new Vector3(xBall, 0.2f, posBlock.z);
                Block blockByName = Spawner.Intance.GetBlockByName(col.transform.parent.gameObject.name);
                bool flag = false;
                Vector3 posBall2 = cTransform.position;
                float x2 = posBall2.x;
                Vector3 posBlock2 = col.transform.position;
                if (Mathf.Abs(x2 - posBlock2.x) <= RemoteVariables.Block_PerfectSize)
                {
                    flag = true;
                }
                if (!flag)
                {
                    if (perfectCount <= 0)
                    {
                        perfectCount--;
                    }
                    else
                    {
                        perfectCount = -1;
                    }
                }
                else if (perfectCount >= 0)
                {
                    perfectCount++;
                }
                else
                {
                    perfectCount = 1;
                }
                GameController.controller.score++;
                TopBar.instance.UpdateScore(GameController.controller.score);
                if (Spawner.Intance.spawnCount == RemoteVariables.UpSpeedScore)
                {
                    ph.UpSpeed();
                }
                Spawner.Intance.Hit(blockByName, perfectCount);
                if (perfectCount >= RemoteVariables.Block_PerfectTrigger)
                {
                    Spawner.Intance.ChangeBlockSize(perfectCount);
                    SoundManager.instance.PlayImpactExtend();
                }
                if (GameController.controller.perfectCountMax < perfectCount)
                {
                    GameController.controller.perfectCountMax = perfectCount;
                }
                if (perfectCount * -1 == RemoteVariables.Block_BadTrigger)
                {
                    Spawner.Intance.ChangeBlockSize(perfectCount);
                    perfectCount = 0;
                }
            }
            else if (col.CompareTag(GAMETAG.BlockClone))
            {
                SoundManager.instance.PlayDiamond();
                brokenEffect.transform.position = cTransform.position;
                var main = brokenEffect.main;
                main.startColor = Color.Lerp(col.GetComponent<MeshRenderer>().material.color, Color.white, 0.6f);
                brokenEffect.gameObject.SetActive(value: true);
                brokenEffect.Play();
                col.transform.parent.gameObject.SetActive(value: false);
            }
            else if (col.CompareTag(GAMETAG.Road))
            {
                Vector3 velocity = ph.rb.velocity;
                if (!(velocity.y < 0f))
                {
                }
            }
        }
        catch(Exception ex)
        {
            Debug.LogError("TilesHop Ball.OnTriggerEnter ex:" + ex.Message);
        }
	}

	public void Reset()
	{
		perfectCount = 0;
	}
}
