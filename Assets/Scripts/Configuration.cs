using System;
using UnityEngine;
//@TODO ENABLE_UTNOTIFICATION
#if ENABLE_UTNOTIFICATION
using UTNotifications;
#endif

public class Configuration : MonoBehaviour
{
	public enum FreeGifts
	{
		WAIT,
		SHOW,
		CLOSE
	}

	public bool activeABTest;

	public string abTestName;

	public ABTestCase testCase;

	public int forceTheme = -1;

	public int totalThemes = 8;

	public bool cleanDataOnFirst;

	private bool musicIsOn = true;

	private bool soundIsOn = true;

	[HideInInspector]
	public bool isNoAds;

	[HideInInspector]
	public bool gameloaded;

	[HideInInspector]
	public bool showFacebookLogin = true;

	[HideInInspector]
	public FreeGifts showFreeGifts;

	[HideInInspector]
	public bool showInterstitialAd = true;

	[HideInInspector]
	public bool isTutorial;

	[HideInInspector]
	public int diamondsVideoBonus;

	public static Configuration instance;

	public AnimationCurve defaultCurve;

	private LOCATION_NAME current_location;

	private LOCATION_NAME current_popup;

	[HideInInspector]
	public float sessionDuration;

	[HideInInspector]
	public float playDuration;

	public DateTime startActiveTime;

	public DateTime lastActiveTime;

	public bool isDebug;

	public int sessionID
	{
		get;
		private set;
	}

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
			UnityEngine.Object.DontDestroyOnLoad(base.gameObject);
			Application.targetFrameRate = 60;
			if (cleanDataOnFirst)
			{
				PlayerPrefs.DeleteAll();
			}
			QualitySettings.vSyncCount = 0;
			isNoAds = (PlayerPrefs.GetInt("noads", 0) == 1 || isDebug);
			musicIsOn = (PlayerPrefs.GetInt("music", 1) == 1);
			soundIsOn = (PlayerPrefs.GetInt("sound", 1) == 1);
		}
		else if (instance != null)
		{
			UnityEngine.Object.Destroy(base.gameObject);
		}
	}

	private void Start()
	{
#if ENABLE_UTNOTIFICATION
		Manager.Instance.OnNotificationClicked += delegate(ReceivedNotification notification)
		{
			AnalyticHelper.FireNotifications(notification.id.ToString());
			if (notification.id == 5 && SongList.instance != null)
			{
				SongList.instance.BuidListNew();
			}
		};
		Manager.Instance.Initialize(willHandleReceivedNotifications: false);
#endif
		IncreaseSession();
	}

	private void IncreaseSession()
	{
		AnalyticHelper.End_Session();
		sessionDuration = 0f;
		playDuration = 0f;
		sessionID++;
		PlayerPrefs.SetInt("Session_ID", sessionID);
		AnalyticHelper.Start_Session();
	}

	public string GetCurrentLocation()
	{
		if (current_popup == LOCATION_NAME.POPUP_CLOSE)
		{
			return current_location.ToString();
		}
		return current_popup.ToString();
	}

	public void SetCurrentLocation(LOCATION_NAME location, bool isPopup = false)
	{
		if (isPopup)
		{
			current_popup = location;
		}
		else
		{
			current_location = location;
		}
		AnalyticHelper.Screen(location);
	}

	public static void ScheduleNotification()
	{
		string productName = Application.productName;
		if (PlayerPrefs.GetInt("SetupNotification", 0) == 0)
		{
			string localizedValue = LocalizationManager.instance.GetLocalizedValue("PUSH_1");
			string localizedValue2 = LocalizationManager.instance.GetLocalizedValue("PUSH_2");
			string localizedValue3 = LocalizationManager.instance.GetLocalizedValue("PUSH_3");
#if ENABLE_UTNOTIFICATION
			Manager.Instance.CancelAllNotifications();
			Manager.Instance.ScheduleNotification(TimeUtils.DaysToSeconds(1), productName, localizedValue, 1);
			Manager.Instance.ScheduleNotificationRepeating(TimeUtils.DaysToSeconds(3), TimeUtils.DaysToSeconds(9), productName, localizedValue2, 2);
			Manager.Instance.ScheduleNotificationRepeating(TimeUtils.DaysToSeconds(6), TimeUtils.DaysToSeconds(9), productName, localizedValue3, 3);
			Manager.Instance.ScheduleNotificationRepeating(TimeUtils.DaysToSeconds(9), TimeUtils.DaysToSeconds(9), productName, localizedValue, 4);
#endif
            PlayerPrefs.SetInt("SetupNotification", 1);
		}
	}

	private void Update()
	{
		if (!Input.GetKeyUp(KeyCode.Escape))
		{
			return;
		}
		Time.timeScale = 1f;
		if (SceneFader.instance.isAnimated)
		{
			return;
		}
		if (ExitPopUp.instance != null)
		{
			ExitPopUp.instance.Close();
		}
		else if (MessagePopup.instance != null)
		{
			MessagePopup.instance.Close();
		}
		else if (Shop.instance != null)
		{
			Shop.instance.Close();
		}
		else if (FreeVideo.instance != null)
		{
			FreeVideo.instance.Close();
		}
		else if (AdsPurchase.instance != null)
		{
			AdsPurchase.instance.Close();
		}
		else if (LanguageSelection.instance != null)
		{
			LanguageSelection.instance.Close();
		}
		else if (Tips.instance != null)
		{
			Tips.instance.Close();
		}
		else if (BoardManager.instance != null)
		{
			BoardManager.instance.Close();
		}
		else if (DailyGift.instance != null)
		{
			DailyGift.instance.Close();
		}
		else if (Challenge.instance != null)
		{
			Challenge.instance.Close();
		}
		else if (Credit.instance != null)
		{
			Credit.instance.Close();
		}
		else if (HomeManager.instance != null)
		{
			if (SettingList.instance != null && SettingList.instance.gameObject.activeSelf)
			{
				SettingList.instance.Close();
			}
			else if (BallList.instance != null && BallList.instance.gameObject.activeSelf)
			{
				BallList.instance.Close();
			}
			else if (ThemeList.instance != null && ThemeList.instance.gameObject.activeSelf)
			{
				ThemeList.instance.Close();
			}
			else if (Achievements.instance != null && Achievements.instance.gameObject.activeSelf)
			{
				Achievements.instance.Close();
			}
			else
			{
				Util.ShowPopUp("ExitPopUp");
			}
		}
		else if (GameController.controller != null)
		{
			if (GameController.controller.game != 0 || UIController.ui.gameui.activeSelf || UIController.ui.gameover.activeSelf)
			{
				SceneFader.instance.FadeTo("Home");
			}
			else if (UIController.ui.continueui.activeSelf)
			{
				UIController.ui.continueui.GetComponent<ContinueUI>().Cancel();
			}
		}
	}

	public static string GetLanguageID()
	{
		return PlayerPrefs.GetString("language_id", null);
	}

	public static void SetLanguageID(string id)
	{
		PlayerPrefs.SetString("language_id", id);
	}

	public void AddNoAds()
	{
		isNoAds = true;
		PlayerPrefs.SetInt("noads", 1);
	}

	public void SetMusic(bool ON)
	{
		if (ON)
		{
			SetMusicOn();
		}
		else
		{
			SetMusicOff();
		}
	}

	public void SetMusicOn()
	{
		PlayerPrefs.SetInt("music", 1);
		musicIsOn = true;
	}

	public void SetMusicOff()
	{
		PlayerPrefs.SetInt("music", 0);
		musicIsOn = false;
	}

	public bool MusicIsOn()
	{
		return musicIsOn;
	}

	public void SetSound(bool ON)
	{
		if (ON)
		{
			SetSoundOn();
		}
		else
		{
			SetSoundOff();
		}
	}

	public void SetSoundOn()
	{
		PlayerPrefs.SetInt("sound", 1);
		soundIsOn = true;
	}

	public void SetSoundOff()
	{
		PlayerPrefs.SetInt("sound", 0);
		soundIsOn = false;
	}

	public bool SoundIsOn()
	{
		return soundIsOn;
	}

	public static bool IntroIsOn()
	{
		return PlayerPrefs.GetInt("intro", 1) == 1;
	}

	public static void SetIntroOff()
	{
		PlayerPrefs.SetInt("intro", 0);
	}

	public static string GetDailyGiftDate()
	{
		string @string = PlayerPrefs.GetString("dailygift_date");
		if (string.IsNullOrEmpty(@string))
		{
			return DateTime.Now.AddDays(-1.0).ToString();
		}
		return @string;
	}

	public static string GetGiftTime(string key)
	{
		string @string = PlayerPrefs.GetString(key);
		if (string.IsNullOrEmpty(@string))
		{
			return DateTime.Now.AddDays(-1.0).ToString();
		}
		return @string;
	}

	public static void SetGiftTime(string key, string date)
	{
		PlayerPrefs.SetString(key, date);
	}

	public static int GetDailyGiftNumber()
	{
		return PlayerPrefs.GetInt("dailygift_number", -1);
	}

	public static void SetDailyGift(int number, string date)
	{
		PlayerPrefs.SetInt("dailygift_number", number);
		PlayerPrefs.SetString("dailygift_date", date);
	}

	public static void SetWrongPlay(string type)
	{
		PlayerPrefs.SetString("WrongPlay", type);
	}

	public static string GetWrongPlay()
	{
		return PlayerPrefs.GetString("WrongPlay", string.Empty);
	}

	public static string GetDataVersion()
	{
		return PlayerPrefs.GetString("data_version", RemoteVariables.DefaultDataVersion);
	}

	public static void SetDataVersion(string version)
	{
		PlayerPrefs.SetString("data_version", version);
	}

	public static string GetLanguageVersion()
	{
		return PlayerPrefs.GetString("language_version", RemoteVariables.DefaultLanguageVersion);
	}

	public static void SetLanguageVersion(string version)
	{
		PlayerPrefs.SetString("language_version", version);
	}

	public static bool FreeAddSongIsOn()
	{
		return PlayerPrefs.GetInt("FreeAddSong", 1) == 1;
	}

	public static void SetFreeAddSongOff()
	{
		PlayerPrefs.SetInt("FreeAddSong", 0);
	}

	public static bool IsABTestNew()
	{
		if (instance.activeABTest)
		{
			if (instance.testCase != 0)
			{
				return instance.testCase == ABTestCase.B;
			}
			return GetABTestID() == 2 && instance.activeABTest;
		}
		return false;
	}

	public static int GetABTestID(int caseNumber = 2, string eventName = null)
	{
		if (eventName == null)
		{
			eventName = instance.abTestName;
		}
		int num = 0;
		if (PlayerPrefs.HasKey("ABTest_op" + eventName))
		{
			num = PlayerPrefs.GetInt("ABTest_op" + eventName, 1);
		}
		else
		{
			num = UnityEngine.Random.Range(1, caseNumber + 1);
			PlayerPrefs.SetInt("ABTest_op" + eventName, num);
		}
		return num;
	}

	public static bool isAdAvailable()
	{
		return CoreData.SongUnlockCount() > 2 || isTutorialAdOn() || (GameController.controller != null && GameController.controller.GetStars() >= RemoteVariables.Tutorial_AdsStar);
	}

	public static void SetTutorialAdOn()
	{
		PlayerPrefs.SetInt("TutorialAd", 1);
	}

	public static bool isTutorialAdOn()
	{
		return PlayerPrefs.GetInt("TutorialAd", 0) == 1;
	}

	private void OnApplicationPause(bool pauseStatus)
	{
		if (pauseStatus)
		{
			lastActiveTime = DateTime.Now;
			sessionDuration += (float)(DateTime.Now - startActiveTime).TotalSeconds;
			PlayerPrefs.SetFloat("Session_Duration", sessionDuration);
			PlayerPrefs.SetFloat("Session_Play", playDuration);
			return;
		}
		int num = (int)(DateTime.Now - lastActiveTime).TotalMinutes;
		if (num > 30)
		{
			IncreaseSession();
			lastActiveTime = DateTime.Now;
		}
		startActiveTime = DateTime.Now;
	}
}
